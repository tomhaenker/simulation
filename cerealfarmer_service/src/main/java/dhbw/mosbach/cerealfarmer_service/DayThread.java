package dhbw.mosbach.cerealfarmer_service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dhbw.mosbach.cerealfarmer_service.dto.ConfirmOrderDTO;
import dhbw.mosbach.cerealfarmer_service.dto.OfferDTO;
import dhbw.mosbach.cerealfarmer_service.dto.OrderDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Component
public class DayThread extends Thread{

    private final CerealfarmerRepository cerealfarmerRepository;

    public DayThread(CerealfarmerRepository cerealfarmerRepository) {
        this.cerealfarmerRepository = cerealfarmerRepository;
    }

    public synchronized void start(Cerealfarmer cerealfarmer) {
        /*
        Täglich anfallende Kosten verbuchen
         */
        cerealfarmer.bookDailyCosts();
        cerealfarmerRepository.save(cerealfarmer);

        /*
        Verkauf:
        Beim Start des Tages wird im Controller ein zwischenstand gezogen mit dem die Nachfrage bedient wird.
        Änderungen an Bestand und Preisen können daher nun direkt am Objekt vorgenommen werden
         */

        /*
        Produktion
        Sämtlich Logik ist im Unternehmen vorhanden
         */
        cerealfarmer.produceProduct();
        cerealfarmerRepository.save(cerealfarmer);
        
        /*
        prüfen ob Unternehmen noch liquide ist
        Wenn nicht --> Aus dem Markt entfernen
         */
        if (cerealfarmer.getBudget().compareTo(new BigDecimal(0)) < 0){
            cerealfarmerRepository.delete(cerealfarmer);
        }

    }





}


