package dhbw.mosbach.cerealfarmer_service;

import dhbw.mosbach.cerealfarmer_service.dto.CerealfarmerGetDTO;
import dhbw.mosbach.cerealfarmer_service.dto.CerealfarmerPostDTO;
import dhbw.mosbach.cerealfarmer_service.dto.CerealfarmerPutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
public class CrudController {

    final
    CerealfarmerRepository cerealfarmerRepository;

    public CrudController(CerealfarmerRepository cerealfarmerRepository) {
        this.cerealfarmerRepository = cerealfarmerRepository;
    }

    @PostMapping("/api/company")
    public void createCerealfarmer(@RequestBody CerealfarmerPostDTO cerealfarmerPostDTO){
        cerealfarmerRepository.save(new Cerealfarmer(cerealfarmerPostDTO));
    }

    @GetMapping("/api/company/{id}")
    public CerealfarmerGetDTO getCerealfarmer(@PathVariable UUID id){
        Optional<Cerealfarmer> cerealfarmer = cerealfarmerRepository.findById(id);
        if (cerealfarmer.isEmpty()) throw new IllegalArgumentException("Wrong CompanyID");
        return new CerealfarmerGetDTO(cerealfarmer.get());
    }

    @PutMapping("/api/company/{id}")
    public CerealfarmerGetDTO updateCerealfarmer(@PathVariable UUID id, @RequestBody CerealfarmerPutDTO cerealfarmerPutDTO){
        Optional<Cerealfarmer> optionalCerealfarmer = cerealfarmerRepository.findById(id);
        if (optionalCerealfarmer.isEmpty()) throw new IllegalArgumentException("Wrong CompanyID");
        Cerealfarmer cerealfarmer = optionalCerealfarmer.get();
        cerealfarmer.updateTargetAmount(cerealfarmerPutDTO.getWarehousePercentage());
        cerealfarmer.setProfit_margin(cerealfarmerPutDTO.getProfitMargin());
        cerealfarmerRepository.save(cerealfarmer);
        return new CerealfarmerGetDTO(cerealfarmer);
    }

    @DeleteMapping("/api/company/{id}")
    public void deleteCompany(@PathVariable UUID id){
        if (cerealfarmerRepository.findById(id).isEmpty()) return;
        cerealfarmerRepository.deleteById(id);
    }

    @PostMapping("/api/randomCompany")
    public void createRandomCerealfarmer(){
        cerealfarmerRepository.save(Cerealfarmer.createRandom());
    }


}
