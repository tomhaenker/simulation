package dhbw.mosbach.cerealfarmer_service;

import dhbw.mosbach.cerealfarmer_service.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MainController {

    final CerealfarmerRepository cerealfarmerRepository;
    final ResourceValueRepository resourceValueRepository;
    final ApplicationContext applicationContext;
    public static List<OfferDTO> availableOffers = new ArrayList<>();

    public MainController(CerealfarmerRepository cerealfarmerRepository, ApplicationContext applicationContext, ResourceValueRepository resourceValueRepository) {
        this.cerealfarmerRepository = cerealfarmerRepository;
        this.applicationContext = applicationContext;
        this.resourceValueRepository = resourceValueRepository;
    }

    /**
     * Startet einen neuen Tag
     * @throws InterruptedException
     */
    @PostMapping("/api/startDay")
    public void startDay() throws InterruptedException {

        //Sollte der Service neugestartet werden, wird der in der DB hinterlegte Rohstoffwert herangezogen.
        //Ist die DB allerdings leer, existiert kein Rohstoffwert und "0.0" wird akzeptiert
        //Ist ein Wert vorhanden, findet ebenfalls keine neue Belegung statt.
        if((Cerealfarmer.getParagon().compareTo(BigDecimal.ZERO) <= 0) && (resourceValueRepository.count()>0))
            Cerealfarmer.setParagon(resourceValueRepository.findAll().get(0).getValue());


        synchronized (availableOffers) {
            availableOffers.clear(); // Angebote des Vortages werden gelöscht
        }
        List<Cerealfarmer> cerealfarmers = cerealfarmerRepository.findAll();
        List<DayThread> companyDayThreads = new ArrayList<>();
        synchronized (availableOffers) {
//        Angebote werden vor Produktion erstellt.
//        So wird sichergestellt, dass keine Waren angebote bzw. verkauft werden, die am selben Tag produziert wurden
            for (Cerealfarmer cerealfarmer : cerealfarmers) {
                availableOffers.add(new OfferDTO(cerealfarmer));
            }
        }
        for (Cerealfarmer cerealfarmer : cerealfarmers) {
//            für jedes Unternehmen wird ein separater Thread zur Simulation geöffnet
            DayThread dayThread = new DayThread(cerealfarmerRepository);
            applicationContext.getAutowireCapableBeanFactory().autowireBean(dayThread);
            dayThread.start(cerealfarmer);
            companyDayThreads.add(dayThread);
        }
        for (DayThread daythread : companyDayThreads) {
//            es wird nacheinander geprüft ob jedes Unternehmen fertig berechnet wurde
            daythread.join();
        }
//        Wenn alle Unternehmen simuliert wurden gilt die Methode als abgeschlossen
//        Erst jetzt wird durch den HTTP Status Code 200 signalisiert, dass der Tag fertig simuliert wurde
    }

    /**
     * Liefert alle Angebote einer Qualität zurück
     * @param qualityLevel Angefragte Qualität
     * @return Liste aller Unternehmen die Getreide dieser Qualität liefern können
     */

    /**
     * Liefert alle Angebote einer Qualität zurück
     * @param qualityLevel Angefragte Qualität
     * @return Liste aller Unternehmen die Bier dieser Qualität liefern können
     */
    @GetMapping("/api/request/{qualityLevel}")
    public List<OfferDTO> receiveRequest(
            @PathVariable QualityLevel qualityLevel) {
//        QualityLevel qualityLevel = QualityLevel.getQualityLevel(qualityLevelInt);
        List<OfferDTO> matchingOffers = new ArrayList<>();
        while (availableOffers.size() == 0 && cerealfarmerRepository.count() > 0){ //Wenn keine Brauereien angelegt sind kann es auch keine Angebote geben
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
        synchronized (availableOffers) {
//        throw new IllegalArgumentException("Start day before request data"); // Liste wird erstmals beim Starten von Tag 1 gefüllt
            for (OfferDTO offerDTO : availableOffers) {
                if (offerDTO.getQualityLevel(cerealfarmerRepository).equals(qualityLevel)) matchingOffers.add(offerDTO);
            }
        }
        return matchingOffers;
    }

    /**
     * Führt eine Bestellung aus
     * @param order Bestelldetails (VerkäuferID und Menge)
     * @return Bestellbestätigung
     */
    @PostMapping("/api/order")
    public ConfirmOrderDTO receiveOrder(
            @RequestBody OrderDTO order) {
        if (cerealfarmerRepository.findById(order.companyId).isEmpty())
            throw new IllegalArgumentException("Company does not exist");
        Cerealfarmer cerealfarmer = cerealfarmerRepository.findById(order.companyId).get();
        synchronized (availableOffers) {
//        Der Bestand vom Bier muss in den Offers geprüft werden. Stellt sicher, dass der Bestand zu Beginn des Tages geprüft wird
            for (OfferDTO offer : availableOffers) {
                if (offer.id.equals(order.companyId)) {
                    if (offer.quantity < order.quantity)
                        throw new IllegalArgumentException("Not enough goods available");
                    else {
                        offer.quantity = offer.quantity - order.quantity; // Update der heutigen Daten
                        cerealfarmer.sellProduct(order.quantity, BigDecimal.valueOf(offer.unitPrice)); // Update der Gesamtdaten
                        cerealfarmerRepository.save(cerealfarmer);
                        return new ConfirmOrderDTO(cerealfarmer.getId(), order.quantity, offer.unitPrice);
                    }
                }
            }
        }
        throw new IllegalArgumentException("Cerealfarmer - Receive Order - Something went wrong");
    }

    /**
     * Liefert detaillierte Daten der vorhandenen Unternehmen zurück
     * @return Brauereidaten
     */
    @GetMapping("/api/company")
    public DataDTO getAllCompanyData(){
        List<Cerealfarmer> cerealfarmers =  cerealfarmerRepository.findAll();
        List<CerealfarmerGetDTO> dtoList = new ArrayList<>();
        for (Cerealfarmer cerealfarmer: cerealfarmers) {
            dtoList.add(new CerealfarmerGetDTO(cerealfarmer));
        }
        return new DataDTO(dtoList);
    }

    /**
     * Liefert zusammengefasste Daten der vorhandenen Unternehmen zurück
     * @return Brauereidaten
     */
    @GetMapping("/api/dataOverview")
    public OverviewDTO getAllCompanyOverview(){
        List<Cerealfarmer> cerealfarmers =  cerealfarmerRepository.findAll();
        OverviewQualityDetailDTO qualityLow = new OverviewQualityDetailDTO();
        OverviewQualityDetailDTO qualityMiddle = new OverviewQualityDetailDTO();
        OverviewQualityDetailDTO qualityHigh = new OverviewQualityDetailDTO();
        for (Cerealfarmer cerealfarmer: cerealfarmers) {
            switch (cerealfarmer.getQualityLevel()){
                case LOW: qualityLow.addCompany(cerealfarmer); break;
                case MEDIUM: qualityMiddle.addCompany(cerealfarmer); break;
                case HIGH: qualityHigh.addCompany(cerealfarmer); break;
            }
        }

        return new OverviewDTO(qualityLow, qualityMiddle, qualityHigh);
    }

    /**
     * Erstellt Unternehmen mit zufälligen Parametern
     * Schnittstelle ist für das Testen gedacht
     * @param number Anzahl der Unternehmen die erstellt werden soll
     */
    @PostMapping("/api/createDummies/{number}")
    public void createDummies(
            @PathVariable int number){
        int counter = 0;
        while (counter < number){
            Cerealfarmer cerealfarmer = Cerealfarmer.createRandom();
//            Für die Tests sollen Unternehmen mit vollen Lagern erzeugt werden
            cerealfarmer.setAmount_product(cerealfarmer.getMax_amount_product());
            cerealfarmerRepository.save(cerealfarmer);
            counter++;
        }
    }

    /**
     * Wird aufgerufen um alle Daten zu löschen und bevor eine neue Simulation gestartet wird
     */
    @PostMapping("/api/endSimulation")
    public void deleteData(){

        if (cerealfarmerRepository.count() > 0) cerealfarmerRepository.deleteAllInBatch();
        Cerealfarmer.resetCounter();
        Cerealfarmer.setParagon(BigDecimal.ZERO);
        if (resourceValueRepository.count() > 0) resourceValueRepository.deleteAllInBatch();
    }


    /**
     * setzt den Wert des Rohstoffs Getreide
     * @param unitPrice
     */
    @PostMapping("api/unitPrice")
    public void setResourceValue(@RequestBody int unitPrice)
    {

        //Wandelt eingetragenen Rohstoffwert von Cent pro Tonne auf Euro pro Kilo (=resourceValue)
        double kiloValue = (double)unitPrice/100000;
        BigDecimal resourceValue = BigDecimal.valueOf(kiloValue);

        //Wert wird für später angelegte Unternehmen als Muster initialisiert
        Cerealfarmer.setParagon(resourceValue);

        //vorheriger Wert in DB wird gelöscht und neu gesetzt
        if (resourceValueRepository.count() > 0) resourceValueRepository.deleteAllInBatch();
        ResourceValue r = new ResourceValue(resourceValue);
        resourceValueRepository.save(r);

        //In der Datenbank vorhandene Unternehmen bekommen den Wert zugewiesen
        long amount = cerealfarmerRepository.count();
        List<Cerealfarmer> AllCompanies = cerealfarmerRepository.findAll();

        for (Cerealfarmer cerealfarmer: AllCompanies)
        {
            cerealfarmer.setValueResource(resourceValue);
            cerealfarmerRepository.save(cerealfarmer);
        }

        Cerealfarmer.setParagon(resourceValue);
    }

    /**
     * Gibt den Rohstoffwert in Cent pro Tonne zurück
     */
    @GetMapping("/api/unitPrice")
    public int getResourceValue(){
        return Cerealfarmer.getParagon().multiply(new BigDecimal("100000")).intValue();
    }
}
