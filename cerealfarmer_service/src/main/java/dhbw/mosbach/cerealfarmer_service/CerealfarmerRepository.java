package dhbw.mosbach.cerealfarmer_service;

import org.springframework.data.jpa.repository.JpaRepository;


        import java.util.List;
        import java.util.UUID;

public interface CerealfarmerRepository extends JpaRepository<Cerealfarmer, UUID> {

    public List<Cerealfarmer> findAllByQualityLevel(QualityLevel qualityLevel);
}