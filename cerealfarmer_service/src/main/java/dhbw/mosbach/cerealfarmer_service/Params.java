package dhbw.mosbach.cerealfarmer_service;

import java.math.BigDecimal;

/**
 * Klasse bündelt unternehmensübergreifende Daten
 * Daten könnten auch für andere Unternehmensdaten genutzt werden.
 * Ziel ist diese Daten über den Core bereitzustellen
 */
public class Params {
    public static int roundingScale = 10;
    public static BigDecimal storageCostPerKg = new BigDecimal("0.0025");
    public static final BigDecimal boost = BigDecimal.valueOf(2);


    /**
     * Lagergröße ist abhängig zur Unternehmensgröße
     * @param size Unternehmensgröße
     * @return Lagergröße
     */
    public static int getMaximumStorage(CompanySize size){
        switch (size){
            case SMALL: return 10_000;
            case MEDIUM: return 50_000;
            case BIG: return 100_000;
        }
        throw new IllegalArgumentException("Wrong size");
    }

    /**
     * Produktionsgeschwindigkeit ist abhängig zur produzierten Qualität
     * @param qualityLevel Produktqualität des Unternehmens
     * @return Bremsfaktor, der dazu dient die Produktionsmenge zu reduzieren
     */
    public static BigDecimal getBreakingFactor(QualityLevel qualityLevel) {
        switch (qualityLevel){
            case LOW: return new BigDecimal(1);
            case MEDIUM: return new BigDecimal("0.5");
            case HIGH: return new BigDecimal("0.25");
        }
        throw new IllegalArgumentException("Wrong Qualitylevel");
    }

    /**
     * Produktionskosten sind abhängig zur produzierten Qualität
     * @param qualityLevel Produktqualität des Unternehmens
     * @return Kostenfaktor der bei der Produktion verrechnet wird
     */
    public static BigDecimal getCostFactor(QualityLevel qualityLevel) {
        switch (qualityLevel){
            case LOW: return new BigDecimal("0.1");
            case MEDIUM: return new BigDecimal("0.2");
            case HIGH: return new BigDecimal("0.3");
        }
        throw new IllegalArgumentException("Wrong Qualitylevel");
    }

    /**
     * Aufrechnungsfaktor für Rohstoffe wird hier bestimmt. Dieser wird entsprechend auf die Eingaben verrechnet.
     * @param qualityLevel Produktqualität des Unternehmens
     * @return Wertfaktor gegenüber niedriger Qualität.
     */
    public static BigDecimal getValueFactor(QualityLevel qualityLevel) {
        switch (qualityLevel){
            case LOW: return new BigDecimal("1.0");
            case MEDIUM: return new BigDecimal("1.1");
            case HIGH: return new BigDecimal("1.2");
        }
        throw new IllegalArgumentException("Wrong Qualitylevel");
    }

    /**
     * Betriebskosten sind abhängig zur produzierten Qualität
     * @param qualityLevel Produktqualität des Unternehmens
     * @return Betriebskosten des Unternehmens
     */
    public static BigDecimal getDailyCost(QualityLevel qualityLevel){
        switch (qualityLevel){
            case LOW: return new BigDecimal(20);
            case MEDIUM: return new BigDecimal(100);
            case HIGH: return new BigDecimal(200);
        }
        throw new IllegalArgumentException("Wrong Qualitylevel");


    }


}
