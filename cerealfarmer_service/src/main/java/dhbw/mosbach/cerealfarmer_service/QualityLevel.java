package dhbw.mosbach.cerealfarmer_service;

public enum QualityLevel {
    LOW(1), MEDIUM(2), HIGH(3);

    private final int value;

    QualityLevel(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static QualityLevel getQualityLevel(int i){
        switch (i){
            case 1:
                return LOW;
            case 2:
                return MEDIUM;
            case 3:
                return HIGH;
        }
        return null;
    }

    public static QualityLevel getQualityLevel(String quality){
        switch (quality.toUpperCase()){
            case "LOW":
                return LOW;
            case "MEDIUM":
                return MEDIUM;
            case "HIGH":
                return HIGH;
        }
        return null;
    }


}
