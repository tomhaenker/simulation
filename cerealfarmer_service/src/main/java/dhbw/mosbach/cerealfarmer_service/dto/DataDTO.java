package dhbw.mosbach.cerealfarmer_service.dto;

import java.util.ArrayList;
import java.util.List;

public class DataDTO {
    public List<CerealfarmerGetDTO> companies = new ArrayList<>();

    public DataDTO(List<CerealfarmerGetDTO> companies) {
        this.companies = companies;
    }
}
