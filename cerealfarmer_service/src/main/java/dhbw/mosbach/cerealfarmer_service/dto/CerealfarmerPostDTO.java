package dhbw.mosbach.cerealfarmer_service.dto;

public class CerealfarmerPostDTO {
    public String companySize;
    public String qualityType;
    public int budget;
    public int warehousePercentage;
    public int profitMargin;
}
