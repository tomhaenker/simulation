package dhbw.mosbach.cerealfarmer_service.dto;

import dhbw.mosbach.cerealfarmer_service.Cerealfarmer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

public class CerealfarmerGetDTO {
    public UUID companyId;
    public int companyIdNumeric;
    public String companySize;
    public String qualityType;
    public int budget;
    public int balance; // steht für die Differenz zwischen Startbudget und aktuellem
    public int currentStock;
    public int warehousePercentage;
    public int profitMargin;

    public CerealfarmerGetDTO(Cerealfarmer cerealfarmer) {
        this.companyId = cerealfarmer.getId();
        this.qualityType = cerealfarmer.getQualityLevel().toString();
        this.budget = cerealfarmer.getBudget().movePointRight(2).intValue();
        this.balance = cerealfarmer.getBudget().subtract(cerealfarmer.getStartBudget()).movePointRight(2).intValue();
        this.warehousePercentage = new BigDecimal(cerealfarmer.getTarget_amount_product()).divide(new BigDecimal(cerealfarmer.getMax_amount_product()), 10, RoundingMode.DOWN).movePointRight(2).intValue(); //Wert ist immer ein Ganzzahl
        this.profitMargin = cerealfarmer.getProfit_margin().movePointRight(2).intValue();
        this.currentStock = cerealfarmer.getAmount_product();
        this.companySize = cerealfarmer.getSize().toString();
        this.companyIdNumeric = cerealfarmer.getNumericalId();
    }
}
