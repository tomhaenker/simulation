package dhbw.mosbach.cerealfarmer_service.dto;

import dhbw.mosbach.cerealfarmer_service.Cerealfarmer;

public class OverviewQualityDetailDTO {
    private int count;
    private int balance;
    private int amount;

    public void addCompany(Cerealfarmer cerealfarmer){
        count++;
        balance = balance + cerealfarmer.getBudget().subtract(cerealfarmer.getStartBudget()).movePointRight(2).intValue();
        amount = amount + cerealfarmer.getAmount_product();
    }

    public int getCount() {
        return count;
    }

    public int getBalance() {
        return balance;
    }

    public int getAmount() {
        return amount;
    }
}
