package dhbw.mosbach.cerealfarmer_service.dto;

import dhbw.mosbach.cerealfarmer_service.Cerealfarmer;
import dhbw.mosbach.cerealfarmer_service.CerealfarmerRepository;
import dhbw.mosbach.cerealfarmer_service.QualityLevel;

import java.util.UUID;

public class OfferDTO implements Comparable<OfferDTO>{
    public UUID id;
    public int quantity;
    public double unitPrice;

    public OfferDTO() { }

    public OfferDTO(Cerealfarmer cerealfarmer) {
        this(cerealfarmer.getId(), cerealfarmer.getAmount_product(), cerealfarmer.calcSellPrice().doubleValue());
    }

    public OfferDTO(UUID id, int quantity, double unitPrice) {
        this.id = id;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    public QualityLevel getQualityLevel(CerealfarmerRepository repository){
        if (repository.findById(id).isEmpty()) throw new IllegalArgumentException("Wrong Id");
        return repository.findById(id).get().getQualityLevel();
    }


    public double getUnitPrice() {
        return unitPrice;
    }

    @Override
    public int compareTo(OfferDTO o) {
        return this.id.compareTo(o.id);
    }
}
