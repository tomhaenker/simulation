package dhbw.mosbach.hopfarmer_service;

public enum CompanySize {
    SMALL(1), MEDIUM(2), BIG(3);

    private final int value;

    CompanySize(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static CompanySize getComanySize(int i){
        switch (i){
            case 1:
                return SMALL;
            case 2:
                return MEDIUM;
            case 3:
                return BIG;
        }
        return null;
    }

    public static CompanySize getCompanySize(String size){
        switch (size.toUpperCase()){
            case "SMALL":
                return SMALL;
            case "MEDIUM":
                return MEDIUM;
            case "BIG":
                return BIG;
        }
        return null;
    }
}

