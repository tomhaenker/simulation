package dhbw.mosbach.hopfarmer_service;

import dhbw.mosbach.hopfarmer_service.dto.HopfarmerPostDTO;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
public class Hopfarmer {
    public static final int default_productProduceRate = 1_000;
    public static final int default_hopConsumeRate = 1_000;

    private static final AtomicInteger count = new AtomicInteger(0);
    @Id @Type(type="uuid-char")
    private UUID id = UUID.randomUUID();
    private int numericalId;
    private BigDecimal budget; //in €
    private BigDecimal startBudget; //in €
    private int amount_product; //in kg
    private int max_amount_product;
    private int target_amount_product;
    private BigDecimal breaking_factor; // Bremserate in Abhängigkeit zur Qualität
    private BigDecimal cost_factor; // Teuerungsrate bei der Herstellung
    private BigDecimal profit_margin;
    private BigDecimal dailyFixCosts;
    private CompanySize size;
    private QualityLevel qualityLevel;
    private static BigDecimal paragon = BigDecimal.ZERO;

    @Column(columnDefinition="DECIMAL(19,10)")
    private BigDecimal value_hop_unit;

    @Column(columnDefinition="DECIMAL(19,10)")
    private BigDecimal value_product_unit = new BigDecimal(0); // beim Verkauf muss hier noch die Gewinnspanne draufgerechnet werden


    /**
     * @return Gibt ein Unternehmen mit zufälligen Parameter zurück
     */
    public static Hopfarmer createRandom(){
        Random random = new Random();
        CompanySize size = CompanySize.getComanySize(random.nextInt(3) + 1);
        QualityLevel quality = QualityLevel.getQualityLevel(random.nextInt(3) + 1);
        BigDecimal budget = BigDecimal.valueOf(random.nextInt(40_000)+10_000);
        BigDecimal profit_margin = BigDecimal.valueOf(random.nextInt(40)+ 10).movePointLeft(2);
        BigDecimal targetPercentage = BigDecimal.valueOf(random.nextInt(50)+ 50).movePointLeft(2);
        return new Hopfarmer(budget, size, quality, targetPercentage, profit_margin);
    }

    /**
     * Erzeugt einen neuen Hopfenbauer
     * @param budget in €
     * @param size Unternehmensgröße
     * @param qualityLevel Qualität
     * @param targetPercentage Zielfüllmenge des Lagers
     * @param profit_margin Angestrebte Gewinnspanne
     */
    public Hopfarmer(BigDecimal budget,
                     CompanySize size,
                     QualityLevel qualityLevel,
                     BigDecimal targetPercentage,
                     BigDecimal profit_margin) {
        this.budget = budget;
        this.startBudget = budget;
        int max_storage_size = Params.getMaximumStorage(size);
//        Bei der Erzeugng eines Unternehmens ist jedes Lager erstmal leer
        this.max_amount_product = max_storage_size;
        target_amount_product = targetPercentage.multiply(new BigDecimal(max_amount_product)).intValue();
        this.size = size;
        this.qualityLevel = qualityLevel;
        breaking_factor = Params.getBreakingFactor(qualityLevel);
        if (size == CompanySize.BIG) breaking_factor = breaking_factor.multiply(Params.boost);
        // große Unternehmen können doppelt so schnell produzieren
        cost_factor = Params.getCostFactor(qualityLevel);
        this.profit_margin = profit_margin;
        this.dailyFixCosts = Params.getDailyCost(qualityLevel);
        this.numericalId = count.incrementAndGet();
        //neu angelegte Unternehmen erhalten den Wert, der in globaler Variable abgespeichert ist.
        this.value_hop_unit = paragon;
    }

    /**
     * Erzeugt einen Hopfenbauer aus dem übergebenen DTO
     * @param hopfarmerPostDTO Von anderem Microservice übergebenes HopfarmerDTO
     */
    public Hopfarmer(HopfarmerPostDTO hopfarmerPostDTO){
        this(
                new BigDecimal(hopfarmerPostDTO.budget).movePointLeft(2),
                CompanySize.getCompanySize(hopfarmerPostDTO.companySize),
                QualityLevel.getQualityLevel(hopfarmerPostDTO.qualityType),
                new BigDecimal(hopfarmerPostDTO.warehousePercentage).movePointLeft(2),
                new BigDecimal(hopfarmerPostDTO.profitMargin).movePointLeft(2)
        );
    }

    /**
     * Zurücksetzen des Unternehmenszählers
     */
    public static void resetCounter()
    {
        count.set(0);
    }

    /**
     * Neusetzen des Rohstoffwerts
     */
    public void setValueResource (BigDecimal value){
        value_hop_unit = value.multiply(Params.getValueFactor(this.qualityLevel));
    }

    /**
     * Neusetzen des globalen Rohstoffwerts.
     */
    public static void setParagon(BigDecimal paragon) {
        Hopfarmer.paragon = paragon;
    }

    /**
     * Rückgabe des globalen Rohstoffwerts.
     */
    public static BigDecimal getParagon() {
        return paragon;
    }


    /**
     * Methode dient zum Starten eines Produktionsdurchgangs.
     * Einmal täglich aufrufen.
     * Sämltiche relevante Prüfungen sind hierin enthalten.
     */
    public void produceProduct(){
//        Bestandsänderungen werden berechnet
        BigDecimal consumed_hop = breaking_factor.multiply(new BigDecimal(default_hopConsumeRate));
        BigDecimal produced_product = breaking_factor.multiply(new BigDecimal(default_productProduceRate));

//        Bestände werden geprüft
        if (max_amount_product < (amount_product + produced_product.intValue()))return; //Es kann nicht mehr produziert werden als ins Lager passt
        if (target_amount_product < amount_product) return; //Es wird nicht mehr wenn die Zielmenge erreicht wurde

//        Berechnung von Zwischensummen zur Preisermittlung
//        Preis ändert sich somit bei jeder Produktion
        BigDecimal value_consumedGoods =
                consumed_hop.multiply(value_hop_unit);
        BigDecimal cost_production = value_consumedGoods.multiply(cost_factor);
        if(!enoughBudgetAvailable(cost_production)) return;
        BigDecimal[] recalculatedValues = recalcValues(amount_product, produced_product.intValue(), value_product_unit, cost_production.add(value_consumedGoods).divide(produced_product, Params.roundingScale, RoundingMode.DOWN));

//        Lagerbestände werden angepasst
        amount_product = recalculatedValues[0].intValue();
        value_product_unit = recalculatedValues[1];
        budget = budget.subtract(cost_production);
    }

    /**
     * Preisberechung nach Bestandszuwachs
     * @param oldAmount Menge vor Bestandsänderung
     * @param increaseAmount Menge des Bestandzuwachses
     * @param oldPrice bisheriger Preis pro Einheit
     * @param buyPrice Stückpreis des Zuwachses
     * @return [neue Gesamtmenge, neuer Stückpreis]
     */
    private BigDecimal[] recalcValues(int oldAmount, int increaseAmount, BigDecimal oldPrice, BigDecimal buyPrice){
        int total_amount = oldAmount + increaseAmount;
        BigDecimal oldValue = new BigDecimal(oldAmount).multiply(oldPrice);
        BigDecimal increasingValue = new BigDecimal(increaseAmount).multiply(buyPrice);
        BigDecimal total_value = oldValue.add(increasingValue);
        BigDecimal unit_value =  total_value.divide(new BigDecimal(total_amount), Params.roundingScale, RoundingMode.DOWN);  // Durchschnittlicher Wert einer Einheit im Lager
        BigDecimal[] returnValue = new BigDecimal[2];
        returnValue[0] = new BigDecimal(total_amount);
        returnValue[1] = unit_value;
        return returnValue;
    }

    /**
     * Verkaufspreisberechnung
     * Ist notwendig, da die Gewinnspanne beim Warenwert des Produktes berücksichtig wird
     * @return Verkaufspreis pro kg Hopfen
     */
    public BigDecimal calcSellPrice(){
        return value_product_unit.multiply(profit_margin.add(new BigDecimal(1)));
    }

    /**
     * Verbucht die Änderungen die durch einen Verkauf entstehen
     * @param sell_amount_product Menge des Hopfens, die verkauft wird
     * @param price_singleUnit Verkaufspreis von einem kg Hopfen (inkl. Profitmarge)
     */
    public void sellProduct(int sell_amount_product, BigDecimal price_singleUnit){
        if (sell_amount_product > amount_product) throw new IllegalArgumentException("Order Amount too high");
        amount_product = amount_product - sell_amount_product;
        budget = budget.add(price_singleUnit.multiply(new BigDecimal(sell_amount_product)));
    }

    /**
     * Setzt die Zielfüllmenge des Lagers anhand eines prozentualen Inputs fest
     * @param targetPercentage Prozentuale Zielfüllmenge des Lagers
     */
    public void updateTargetAmount(BigDecimal targetPercentage){
        if (targetPercentage.intValue() < 0) throw new IllegalArgumentException("Negative percentage");
        if (targetPercentage.intValue() > 100) throw new IllegalArgumentException("More then 100 percent");
        target_amount_product = targetPercentage.multiply(new BigDecimal(max_amount_product)).intValue();
    }

    public boolean enoughBudgetAvailable(BigDecimal cost){
        return cost.compareTo(this.budget) <= 0;
    }

    /**
     * Zieht die täglichen Kosten (Fix- plus Lagerkosten) vom Budget ab
     */
    public void bookDailyCosts(){
        BigDecimal totalCosts = dailyFixCosts.add(Params.storageCostPerKg.multiply(new BigDecimal(amount_product)));
        budget = budget.subtract(totalCosts);
//        es findet keine Prüfung statt. Es ist bewusst möglich hierbei ins Minus zu rutschen
    }









//    Standardmethoden

    /*
    Kennzeichnung als Deprecated hat den Zweck, dass die Methode nicht unwissentlich verwendet wird
    Wird lediglich für die Field based access von Hibernate benötigt
     */

    public Hopfarmer() {
    }

    public UUID getId() {
        return id;
    }

    @Deprecated
    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    @Deprecated
    public void setBudget(BigDecimal budget) {
        this.budget = budget;
    }

    public int getAmount_product() {
        return amount_product;
    }

    @Deprecated
    public void setAmount_product(int amount_product) {
        this.amount_product = amount_product;
    }

    public BigDecimal getValue_hop_unit() {
        return value_hop_unit;
    }

    @Deprecated
    public void setValue_hop_unit(BigDecimal value_hop_unit) {
        this.value_hop_unit = value_hop_unit;
    }

    @Deprecated
    public BigDecimal getValue_product_unit() {
        return value_product_unit;
    }

    @Deprecated
    public void setValue_product_unit(BigDecimal value_product_unit) {
        this.value_product_unit = value_product_unit;
    }


    public int getMax_amount_product() {
        return max_amount_product;
    }

    @Deprecated
    public void setMax_amount_product(int max_amount_product) {
        this.max_amount_product = max_amount_product;
    }

    public int getTarget_amount_product() {
        return target_amount_product;
    }

    @Deprecated
    public void setTarget_amount_product(int target_amount_product) {
        this.target_amount_product = target_amount_product;
    }

    public BigDecimal getBreaking_factor() {
        return breaking_factor;
    }

    @Deprecated
    public void setBreaking_factor(BigDecimal breaking_factor) {
        this.breaking_factor = breaking_factor;
    }

    public BigDecimal getCost_factor() {
        return cost_factor;
    }

    @Deprecated
    public void setCost_factor(BigDecimal cost_factor) {
        this.cost_factor = cost_factor;
    }

    public BigDecimal getProfit_margin() {
        return profit_margin;
    }

    public void setProfit_margin(BigDecimal profit_margin) {
        this.profit_margin = profit_margin;
    }

    public CompanySize getSize() {
        return size;
    }

    @Deprecated
    public void setSize(CompanySize size) {
        this.size = size;
    }

    public QualityLevel getQualityLevel() {
        return qualityLevel;
    }

    @Deprecated
    public void setQualityLevel(QualityLevel qualityLevel) {
        this.qualityLevel = qualityLevel;
    }

    public BigDecimal getStartBudget() {
        return startBudget;
    }

    @Deprecated
    public void setStartBudget(BigDecimal startBudget) {
        this.startBudget = startBudget;
    }

    public int getNumericalId() {
        return numericalId;
    }

    @Deprecated
    public void setNumericalId(int numericalId) {
        this.numericalId = numericalId;
    }

    public BigDecimal getDailyFixCosts() {
        return dailyFixCosts;
    }

    public void setDailyFixCosts(BigDecimal dailyFixCosts) {
        this.dailyFixCosts = dailyFixCosts;
    }


}
