package dhbw.mosbach.hopfarmer_service;

import dhbw.mosbach.hopfarmer_service.dto.HopfarmerGetDTO;
import dhbw.mosbach.hopfarmer_service.dto.HopfarmerPostDTO;
import dhbw.mosbach.hopfarmer_service.dto.HopfarmerPutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
public class CrudController {

    final
    HopfarmerRepository hopfarmerRepository;

    public CrudController(HopfarmerRepository hopfarmerRepository) {
        this.hopfarmerRepository = hopfarmerRepository;
    }

    @PostMapping("/api/company")
    public void createHopfarmer(@RequestBody HopfarmerPostDTO hopfarmerPostDTO){
        hopfarmerRepository.save(new Hopfarmer(hopfarmerPostDTO));
    }

    @GetMapping("/api/company/{id}")
    public HopfarmerGetDTO getHopfarmer(@PathVariable UUID id){
        Optional<Hopfarmer> hopfarmer = hopfarmerRepository.findById(id);
        if (hopfarmer.isEmpty()) throw new IllegalArgumentException("Wrong CompanyID");
        return new HopfarmerGetDTO(hopfarmer.get());
    }

    @PutMapping("/api/company/{id}")
    public HopfarmerGetDTO updateHopfarmer(@PathVariable UUID id, @RequestBody HopfarmerPutDTO hopfarmerPutDTO){
        Optional<Hopfarmer> optionalHopfarmer = hopfarmerRepository.findById(id);
        if (optionalHopfarmer.isEmpty()) throw new IllegalArgumentException("Wrong CompanyID");
        Hopfarmer hopfarmer = optionalHopfarmer.get();
        hopfarmer.updateTargetAmount(hopfarmerPutDTO.getWarehousePercentage());
        hopfarmer.setProfit_margin(hopfarmerPutDTO.getProfitMargin());
        hopfarmerRepository.save(hopfarmer);
        return new HopfarmerGetDTO(hopfarmer);
    }

    @DeleteMapping("/api/company/{id}")
    public void deleteCompany(@PathVariable UUID id){
        if (hopfarmerRepository.findById(id).isEmpty()) return;
        hopfarmerRepository.deleteById(id);
    }

    @PostMapping("/api/randomCompany")
    public void createRandomHopfarmer(){
        hopfarmerRepository.save(Hopfarmer.createRandom());
    }

}
