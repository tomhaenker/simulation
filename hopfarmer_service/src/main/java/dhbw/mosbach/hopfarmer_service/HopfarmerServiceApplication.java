package dhbw.mosbach.hopfarmer_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HopfarmerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(HopfarmerServiceApplication.class, args);
    }

}
