package dhbw.mosbach.hopfarmer_service;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.UUID;

//dient dem Backup der Rohstoffwerte bei Neustart des Services


@Entity
public class ResourceValue {

    @Id
    UUID id = UUID.randomUUID();
    @Column(columnDefinition="DECIMAL(19,10)")
    BigDecimal value = new BigDecimal("0.0");



public ResourceValue(BigDecimal value){
    this.value = value;
}

public ResourceValue(){};

public BigDecimal getValue(){
    return value;
}



}
