package dhbw.mosbach.hopfarmer_service.dto;

import dhbw.mosbach.hopfarmer_service.Hopfarmer;
import dhbw.mosbach.hopfarmer_service.HopfarmerRepository;
import dhbw.mosbach.hopfarmer_service.QualityLevel;

import java.util.UUID;

public class OfferDTO implements Comparable<OfferDTO>{
    public UUID id;
    public int quantity;
    public double unitPrice;

    public OfferDTO() { }

    public OfferDTO(Hopfarmer hopfarmer) {
        this(hopfarmer.getId(), hopfarmer.getAmount_product(), hopfarmer.calcSellPrice().doubleValue());
    }

    public OfferDTO(UUID id, int quantity, double unitPrice) {
        this.id = id;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    public QualityLevel getQualityLevel(HopfarmerRepository repository){
        if (repository.findById(id).isEmpty()) throw new IllegalArgumentException("Wrong Id");
        return repository.findById(id).get().getQualityLevel();
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    @Override
    public int compareTo(OfferDTO o) {
        return this.id.compareTo(o.id);
    }
}
