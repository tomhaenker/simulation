package dhbw.mosbach.hopfarmer_service.dto;

public class OverviewDTO {
    public OverviewQualityDetailDTO qualityLow;
    public OverviewQualityDetailDTO qualityMedium;
    public OverviewQualityDetailDTO qualityHigh;

    public OverviewDTO(OverviewQualityDetailDTO qualityLow, OverviewQualityDetailDTO qualityMedium, OverviewQualityDetailDTO qualityHigh) {
        this.qualityLow = qualityLow;
        this.qualityMedium = qualityMedium;
        this.qualityHigh = qualityHigh;
    }
}
