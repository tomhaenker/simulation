package dhbw.mosbach.hopfarmer_service.dto;

import dhbw.mosbach.hopfarmer_service.Hopfarmer;

public class OverviewQualityDetailDTO {
    private int count;
    private int balance;
    private int amount;

    public void addCompany(Hopfarmer hopfarmer){
        count++;
        balance = balance + hopfarmer.getBudget().subtract(hopfarmer.getStartBudget()).movePointRight(2).intValue();
        amount = amount + hopfarmer.getAmount_product();
    }

    public int getCount() {
        return count;
    }

    public int getBalance() {
        return balance;
    }

    public int getAmount() {
        return amount;
    }
}
