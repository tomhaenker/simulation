package dhbw.mosbach.hopfarmer_service.dto;

import java.math.BigDecimal;

public class HopfarmerPutDTO {
    private int warehousePercentage;
    private int profitMargin;

    public BigDecimal getWarehousePercentage() {
        return new BigDecimal(warehousePercentage).movePointLeft(2);
    }

    public BigDecimal getProfitMargin() {
        return new BigDecimal(profitMargin).movePointLeft(2);
    }
}


