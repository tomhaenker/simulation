package dhbw.mosbach.hopfarmer_service.dto;

public class HopfarmerPostDTO {
        public String companySize;
        public String qualityType;
        public int budget;
        public int warehousePercentage;
        public int profitMargin;
}
