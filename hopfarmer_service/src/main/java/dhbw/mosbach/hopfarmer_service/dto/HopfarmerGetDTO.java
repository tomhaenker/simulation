package dhbw.mosbach.hopfarmer_service.dto;

import dhbw.mosbach.hopfarmer_service.Hopfarmer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

public class HopfarmerGetDTO {
    public UUID companyId;
    public int companyIdNumeric;
    public String companySize;
    public String qualityType;
    public int budget;
    public int balance; // steht für die Differenz zwischen Startbudget und aktuellem
    public int currentStock;
    public int warehousePercentage;
    public int profitMargin;

    public HopfarmerGetDTO(Hopfarmer hopfarmer) {
        this.companyId = hopfarmer.getId();
        this.qualityType = hopfarmer.getQualityLevel().toString();
        this.budget = hopfarmer.getBudget().movePointRight(2).intValue();
        this.balance = hopfarmer.getBudget().subtract(hopfarmer.getStartBudget()).movePointRight(2).intValue();
        this.warehousePercentage = new BigDecimal(hopfarmer.getTarget_amount_product()).divide(new BigDecimal(hopfarmer.getMax_amount_product()), 10, RoundingMode.DOWN).movePointRight(2).intValue(); //Wert ist immer ein Ganzzahl
        this.profitMargin = hopfarmer.getProfit_margin().movePointRight(2).intValue();
        this.currentStock = hopfarmer.getAmount_product();
        this.companySize = hopfarmer.getSize().toString();
        this.companyIdNumeric = hopfarmer.getNumericalId();
    }
}
