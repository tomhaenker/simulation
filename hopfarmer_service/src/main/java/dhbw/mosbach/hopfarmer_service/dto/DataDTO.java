package dhbw.mosbach.hopfarmer_service.dto;

import java.util.ArrayList;
import java.util.List;

public class DataDTO {
    public List<HopfarmerGetDTO> companies = new ArrayList<>();

    public DataDTO(List<HopfarmerGetDTO> companies) {
        this.companies = companies;
    }
}
