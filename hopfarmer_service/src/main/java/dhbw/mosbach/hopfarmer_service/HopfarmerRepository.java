package dhbw.mosbach.hopfarmer_service;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface HopfarmerRepository extends JpaRepository<Hopfarmer, UUID> {

    public List<Hopfarmer> findAllByQualityLevel(QualityLevel qualityLevel);
}
