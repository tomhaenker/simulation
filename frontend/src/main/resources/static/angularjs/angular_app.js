//////////////////////////////
// Angular Route für ng-view
//////////////////////////////


var angular;

var app = angular.module('indexPage', ['ngRoute', 'nvd3']);

app.config(function ($routeProvider) {
	$routeProvider
		.when("/overview", {
			templateUrl : 'views/overview.html'
		})
		.when("/rawMaterials", {
			templateUrl : 'views/rawMaterials.html'
		})
		.when("/hopFarmer", {
			templateUrl : 'views/hopFarmer.html'
		})
		.when("/cerealFarmer", {
			templateUrl : 'views/cerealFarmer.html'
		})
		.when("/waterplant", {
			templateUrl : 'views/waterplant.html'
		})
		.when("/malter", {
			templateUrl : 'views/malter.html'
		})
		.when("/brewery", {
			templateUrl : 'views/brewery.html'
		})
		.when("/consumer", {
			templateUrl : 'views/consumer.html'
		})
		// Übersicht direkt beim Öffnen der Seite einblenden
		.otherwise({
			redirectTo: "/overview"
		})
	
});





//////////////////////////////
// App-Directive - StringToNumber für input type "number"
//////////////////////////////


app.directive('stringToNumber', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
			ngModel.$parsers.push(function(value) {
				return '' + value;
			});
			ngModel.$formatters.push(function(value) {
				return parseFloat(value);
			});
		}
	};
});





//////////////////////////////
// Deklarationen beim Start
//////////////////////////////


// Variable für URL -> localhost + Port für alle Http-requests
var url;


// Variable für alle Alerts zum Error-Handling
var alert;





//////////////////////////////
// Index (Startseite)
//////////////////////////////


app.controller('mainController', function($scope, $rootScope, $http, $log, $location, $window, $interval, $timeout) {
	

	// GET getCorePath - Port für alle Http-requests vom Core Microservice holen
	$http({
		method: "get",
		url: "http://localhost:8090/api/getCorePath",
	}).then(function(response) {
		$log.info(response);
		
		// Variable für URL updaten -> localhost + Port für alle Http-requests
		$scope.urlResponse = response.data;
		url = $scope.urlResponse.path + "/";
		
		// Aufruf Abfrage Simulationsstatus
		$scope.getSimulationStatus();
		
		// Aufruf get dataOverview beim Start
		$scope.getDataOverview();
		
		// Aufrufe für Diagramme
		$scope.getOrderHistory();
		$scope.getPriceHistory();

	}, function (response) {
		alert("Error URL-Abfrage" + response.status + " " + response.statusText + " " + response.data);
		
		/* Testing mit Mockoon API ohne Spring Server

		url = "http://localhost:8080/";
		// Aufruf Abfrage Simulationsstatus
		$scope.getSimulationStatus();
		// Aufruf get dataOverview beim Start
		$scope.getDataOverview();
		// Aufrufe für Diagramme
		$scope.getOrderHistory();
		$scope.getPriceHistory();
		
		*/
		
	});
	
	
	// GET simulationStatus - Abfrage Simulationsstatus
	$scope.getSimulationStatus = function() {
		$http({
			method: "get",
			url: url + "api/simulationStatus",
		}).then(function(response) {
			$log.info(response);
			
			// Informationen für den Simulationsstatus setzen
			$scope.simulationStatusResponse = response.data;	
			// Running "True" = Aktive Simulation -> Button "Beenden" einblenden
			$scope.running = $scope.simulationStatusResponse.running;
			// Paused "True = Simulation pausiert 
			$scope.paused = $scope.simulationStatusResponse.paused;

			// Aufruf setSimStatus -> Verarbeitung der Statusinformationen
			$scope.setSimStatus();
			
		}, function (response) {
			// Error Message hier nur in Console ausgeben wegen Aufruf alle 5 Sekunden
			$log.error("Error " + response.status + " " + response.statusText + " " + response.data);
		});
	}
	
	
	// Statusinformationen aus get simulationStatus verarbeiten -> Statusvariablen setzen
	$scope.setSimStatus = function() {
		
		// Statusinformationen -> Status Slider - Simulation pausiert oder Simulation läuft
		// Simulation noch nicht gestartet
		if (!$scope.running && $scope.paused) {
			$scope.simStatus = false;
			$scope.indexSlider = false;
		}
		// Simulation gestartet und pausiert
		else if ($scope.running && $scope.paused) {
			$scope.simStatus = false;
			$scope.indexSlider = false;
		}
		// Simulation gestartet und läuft
		else if ($scope.running && !$scope.paused) {
			$scope.simStatus = true;
			$scope.indexSlider = true;
		}
		
		// Status für pauseSimulation an Core -> true = Pause | false = Läuft
		// Simulation noch nicht gestartet
		if (!$scope.running && $scope.paused) {
			$scope.postSimStatus = true;
		}
		// Simulation gestartet und pausiert
		else if ($scope.running && $scope.paused) {
			$scope.postSimStatus = true;
		}
		// Simulation gestartet und läuft
		else if ($scope.running && !$scope.paused) {
			$scope.postSimStatus = false;
		}
		
		// Simulationsinformationen -> Start / Beenden - Simulation starten oder beenden
		// Simulation noch nicht gestartet
		if (!$scope.running && $scope.paused) {
			$scope.simButton = true;
			$scope.disableFunctions = true;
			$rootScope.disableSliderWithoutPrices = true;
		}
		// Simulation gestartet und pausiert
		else if ($scope.running && $scope.paused) {
			$scope.simButton = false;
			$scope.disableFunctions = false;
			$rootScope.disableSliderWithoutPrices = false;
		}
		// Simulation gestartet und läuft
		else if ($scope.running && !$scope.paused) {
			$scope.simButton = false;
			$scope.disableFunctions = false;
			$rootScope.disableSliderWithoutPrices = false;
		}

	}
	
	
	// Statusvariablen setzen für Eingabe Preisinformationen und Sidebar ausblenden beim öffnen
	$scope.initSimInformations = function() {
		$scope.showSidebarMenu = false;
		$scope.unitPricesForm = false;
	}
	
	
	// Status Slider - Status wechseln (pausiert oder läuft)
	$scope.changeStatus = function() {
		$scope.simStatus = !$scope.simStatus;
		$scope.postSimStatus = !$scope.postSimStatus;
		
		// Funktionsaufruf - Simulation laufen lassen oder pausieren
		$scope.postPauseSimulation();
	}
	
	
	// Status Slider und unitPricesForm beim Beenden der Simulation auf false setzen
	$scope.disableSlider = function() {
		$scope.simStatus = false;
		$scope.indexSlider = false;
		$scope.unitPricesForm = false;
		$rootScope.disableSliderWithoutPrices = true;
	}
	
	
	// Simulationsinformationen -> Start / Beenden wechseln
	$scope.changeSimulation = function() {
		$scope.simButton = !$scope.simButton;
		$scope.disableFunctions = !$scope.disableFunctions;
	}
	
	
	// View Grundstoffe beim Klick auf Start öffnen wenn die Preiseingabe der Grundstoffe noch nicht an den Core übergeben wurde
	$scope.checkPricesStart = function() {
		if ($rootScope.disableSliderWithoutPrices) {
			return "#/rawMaterials";
		}
		else {
			return "#/overview";	
		}
	}
	
	
	// Prüfen Simulation gestartet und Preiseingabe der Grundstoffe erfolgt -> dann Slider aktivieren
	$scope.checkSliderDisabled = function() {
		if ($scope.disableFunctions==false && $rootScope.disableSliderWithoutPrices==false) {
			return false;
		}
		else {
			return true;
		} 
	}
	
	
	// Prüfen ob Preiseingabe der Grundstoffe bereits erfolgt ist -> Sonst Tooltip Slider kann nicht freigegeben werden
	$scope.checkUnitPricesSubmit = function() {
		if ($rootScope.disableSliderWithoutPrices==true && $scope.disableFunctions==false) {
			return true
		}
		else {
			return false;	
		}
	}
	
	
	// Form für unitPrices beim Start der Simulation einblenden / ausblenden
	$scope.openPricesForm = function() {
		$scope.unitPricesForm = true;
	}
	$scope.closePricesForm = function() {
		$scope.unitPricesForm = false;
	}
	
	
	// Laufzeit letzte 2 Ziffern für Progress Bar width % verwenden
	$scope.initSimProgress = function() {
		$scope.numberLimit = -2;
	}
	
	
	// Einblenden Sidebar Icon Navigation im Burger Menü für medium size device-width 
	$scope.changeMenuIconStatus = function() {
		$scope.showSidebarMenu = !$scope.showSidebarMenu;
	}
	
	
	// Ausblenden Sidebar
	$scope.closeSidebarMenu = function() {
		$scope.showSidebarMenu = false;
	}
	
	
	// POST pauseSimulation - Simulation laufen lassen oder pausieren
	$scope.postPauseSimulation = function() {
        $http({
            method: "post",
            url: url + "api/pauseSimulation",
            data: {
				// true = Pause | false = Läuft
                pause: $scope.postSimStatus,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			$log.info(response);
	
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				pause: $scope.postSimStatus,
			}
			alert("Ausgabe JSON pauseSimulation: " + JSON.stringify(testData));
			*/
			
		});
	}
	
	
	// POST endSimulation - Simulation komplett beenden
	$scope.postEndSimulation = function() {
        $http({
            method: "post",
            url: url + "api/endSimulation",
            data: {
				// keine Daten übermitteln
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			$log.info(response);
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testHeaders = {
				'Content-Type': 'application/json'
			}
			alert("Ausgabe JSON headers endSimulation: " + JSON.stringify(testHeaders));
			*/
			
		});
	}
	
	
	// Abruf dataOverview alle 5 Sekunden
	$interval(function() { $scope.getDataOverview(); }, 5000);


	// GET dataOverview - Alle Übersichtsdaten + Simulationsdauer (Laufzeit) -> Aufruf alle 5 Sekunden
	$scope.getDataOverview = function() {
		$http({
			method: "get",
			url: url + "api/dataOverview",
		}).then(function(response) {
			$scope.overview = response.data;
			$log.info(response);
			
			// Laufzeit der Simulation - Zeiteinheit
			$scope.simDuration = response.data.day;
			
			/* 
			Testing JSON Input
			alert("Eingabe JSON dataOverview: " + JSON.stringify($scope.overview));
			*/
			
		}, function (response) {
			// Error Message hier nur in Console ausgeben wegen Aufruf alle 5 Sekunden
			$log.error("Error " + response.status + " " + response.statusText + " " + response.data);
		});
	}
	
	
	// Daten für Bierkonsum und Bierpreise alle 20 Sekunden holen
	$scope.time = 0;
	
	
	// Wiederholter Aufruf der Timeout Funktion
	var timer = function() {
		if($scope.time < 1000000) {
			$scope.time += 1;
			
			// Daten updaten
			$scope.getOrderHistory();
			$scope.getPriceHistory();
			
			$timeout(timer, 20000);
		}
	}
        
	
	// Timeout-Schleife laufen lassen
	$timeout(timer, 20000);   
	
	
	// Entwicklung des Bierkonsums -> Darstellung als Chart mit Angular-nvD3 charting library		
	// GET orderHistory - alle 20 Sekunden
	$scope.getOrderHistory = function() {
		$http({
			method: "get",
			url: url + "api/orderHistory/consumer",
		}).then(function(response) {
			$log.info(response);
			
			$scope.dataHistory = response.data;
			
			// Keys aus JSON umbenennen
			angular.forEach($scope.dataHistory, function(value){
				
				// Niedrige Qualität
				if (value.key.toLowerCase() == "low") {
					value.key = "Niedrige Qualität";
				}
				// Mittlere Qualität
				else if (value.key.toLowerCase() == "medium") {
					value.key = "Mittlere Qualität";
				}
				// Hohe Qualität
				else if (value.key.toLowerCase() == "high") {
					value.key = "Hohe Qualität";
				}

			});		
			
			$scope.data = $scope.dataHistory;
			
			// Testing JSON Input
			// alert("Eingabe JSON company: " + JSON.stringify($scope.data));
						
		}, function (response) {
			// Error Message hier nur in Console ausgeben wegen Aufruf alle 5 Sekunden
			$log.error("Error " + response.status + " " + response.statusText + " " + response.data);
		});
	}
	
	
	// Objekt für maxValue 
	$scope.maxLineRepeat = 0;
	$scope.maxLine = 0;
	
	
	// Entwicklung des Bierpreises -> Darstellung als Chart mit Angular-nvD3 charting library	
	// GET priceHistory - alle 20 Sekunden
	$scope.getPriceHistory = function() {
		$http({
			method: "get",
			url: url + "api/priceHistory/consumer",
		}).then(function(response) {
			$log.info(response);
			
			$scope.dataPrices = response.data;
			
			// Keys aus JSON umbenennen
			angular.forEach($scope.dataPrices, function(value){
				
				// Niedrige Qualität
				if (value.key.toLowerCase() == "low") {
					value.key = "Niedrige Qualität";
				}
				// Mittlere Qualität
				else if (value.key.toLowerCase() == "medium") {
					value.key = "Mittlere Qualität";
				}
				// Hohe Qualität
				else if (value.key.toLowerCase() == "high") {
					value.key = "Hohe Qualität";
				}

			});		
			
			// Umrechnung Cent in Euro
			angular.forEach($scope.dataPrices, function(data) {

				$scope.tempValues = data.values;
				
				// maxValue holen
				$scope.maxLineRepeat = Math.max.apply(Math,$scope.tempValues.map(function(item){return item.y;}));
			
				if (parseInt($scope.maxLine) < parseInt($scope.maxLineRepeat)) {
				$scope.maxLine = $scope.maxLineRepeat;
				}

				angular.forEach($scope.tempValues, function(data) {
					data.y = data.y / 100;	
				});
			
			});
			
			// maxValue für Diagramm in Euro umrechnen und + 1 addieren
			$scope.maxLine = $scope.maxLine / 100;
			$scope.maxLine = $scope.maxLine + 1;
			
			$scope.data2 = $scope.dataPrices;
			
			// Testing JSON Input
			// alert("Eingabe JSON company: " + JSON.stringify($scope.data2));
					
		}, function (response) {
			// Error Message hier nur in Console ausgeben wegen Aufruf alle 5 Sekunden
			$log.error("Error " + response.status + " " + response.statusText + " " + response.data);
		});
	}

	
	/*
	
	GEMOVED IN VIEW CONTROLLERS
	BERECHNUNG FÜR SOLL-IST-VERGLEICH NUR ANHAND DER DIREKTEN NACHFOLGER UND NICHT AUSGEHEND VON DER NACHFRAGEMENGE
	
	Berechnung der benötigten Produktionsmengen für die Erfüllung der aktuellen Nachfragemenge je Qualitätsstufe
	Variablen für Soll-Ist-Vergleich je Qualitätsstufe - Mengeneinheit KG / Liter
	Rückwärtsrechnung der Produktion und Mischverhältnisse im Herstellungsprozess
	
	Mischverhältnisse:
	4.0 Wasser + 1.0 Gerste = 0.8 Malz
	1.5 Wasser + 0.5 Malz + 0.2 Hopfen = 1.0 Bier

	$scope.targetPerformanceCalculation = function () {
		
		// Soll-Variablen - Nachfragemengen Endkunden
		$scope.targetConsumerLow = parseInt($scope.overview.consumerDemand.qualityLow);
		$scope.targetConsumerMedium = parseInt($scope.overview.consumerDemand.qualityMedium);
		$scope.targetConsumerHigh = parseInt($scope.overview.consumerDemand.qualityHigh);
		
		// Soll-Variablen - Erforderliche Produktionskapzität Brauereien
		$scope.targetBreweryLow = parseInt($scope.targetConsumerLow);
		$scope.targetBreweryMedium = parseInt($scope.targetConsumerMedium);
		$scope.targetBreweryHigh = parseInt($scope.targetConsumerHigh);
		
		// Soll-Variablen - Erforderliche Produktionskapzität Mälzereien
		$scope.targetMalterLow = parseInt($scope.targetBreweryLow) * 0.5;
		$scope.targetMalterMedium = parseInt($scope.targetBreweryMedium) * 0.5;
		$scope.targetMalterHigh = parseInt($scope.targetBreweryHigh) * 0.5;
		
		// Soll-Variablen - Erforderliche Produktionskapzität Wasserwerke
		$scope.targetWaterplantLow = (parseInt($scope.targetBreweryLow) * 1.5) + (parseInt($scope.targetMalterLow) * 5);
		$scope.targetWaterplantMedium = (parseInt($scope.targetBreweryMedium) * 1.5) + (parseInt($scope.targetMalterMedium) * 5);
		$scope.targetWaterplantHigh = (parseInt($scope.targetBreweryHigh) * 1.5) + (parseInt($scope.targetMalterHigh) * 5);
		
		// Soll-Variablen - Erforderliche Produktionskapzität Gerstenanbau
		$scope.targetCerealFarmerLow = parseInt($scope.targetMalterLow) * 1.25;
		$scope.targetCerealFarmerMedium = parseInt($scope.targetMalterMedium) * 1.25;
		$scope.targetCerealFarmerHigh = parseInt($scope.targetMalterHigh) * 1.25;
		
		// Soll-Variablen - Erforderliche Produktionskapzität Hopfenanbau
		$scope.targetHopFarmerLow = parseInt($scope.targetBreweryLow) * 0.2;
		$scope.targetHopFarmerMedium = parseInt($scope.targetBreweryMedium) * 0.2;
		$scope.targetHopFarmerHigh = parseInt($scope.targetBreweryHigh) * 0.2;	
		
		Testing Soll-Produktionskapazität
		alert("Targets Low: " + "Endkunden " + $scope.targetConsumerLow + " Brauereien " + $scope.targetBreweryLow + " Mälzereien " + $scope.targetMalterLow + " Wasserwerke " + $scope.targetWaterplantLow + " Gerstenanbau " + $scope.targetCerealFarmerLow + " Hopfenanbau " + $scope.targetHopFarmerLow);
		alert("Targets Medium: " + "Endkunden " + $scope.targetConsumerMedium + " Brauereien " + $scope.targetBreweryMedium + " Mälzereien " + $scope.targetMalterMedium + " Wasserwerke " + $scope.targetWaterplantMedium + " Gerstenanbau " + $scope.targetCerealFarmerMedium + " Hopfenanbau " + $scope.targetHopFarmerMedium);
		alert("Targets High: " + "Endkunden " + $scope.targetConsumerHigh + " Brauereien " + $scope.targetBreweryHigh+ " Mälzereien " + $scope.targetMalterHigh + " Wasserwerke " + $scope.targetWaterplantHigh + " Gerstenanbau " + $scope.targetCerealFarmerHigh + " Hopfenanbau " + $scope.targetHopFarmerHigh);

	}
	
	*/
	
});





//////////////////////////////
// Übersicht
//////////////////////////////


app.controller('overviewController', function() {
	
});





//////////////////////////////
// Grundstoffe
//////////////////////////////


app.controller('rawMaterials', function($scope, $rootScope, $http, $log) {
	
	
	// Status bearbeiten oder speichern setzten
	$scope.initUnitPrices = function() {
		$scope.unitPricesEditStatus = false;
	}
	
	
	// Grundstoffpreise bearbeiten und speichern durch Buttons
	$scope.changeUnitPrices = function() {
		$scope.unitPricesEditStatus = !$scope.unitPricesEditStatus;
	}
	
	
	// Felder zum Bearbeiten freigeben
	$scope.enableUnitPricesEdit = function() {
		$scope.unitPricesEdit = false;
	}
	
	
	// Felder zum Bearbeiten und Sperren
	$scope.disableUnitPricesEdit = function() {
		$scope.unitPricesEdit = true;
	}

	
	
	// Preiseingabe der Grundstoffe muss mit "Speichern" ausgeführt werden -> Status Slider wird freigegeben
	$scope.activateSliderAfterUnitPrices = function() {
		$rootScope.disableSliderWithoutPrices = false;
	}
	
	
	// GET unitPrices - Einkaufspreise für Grundstoffe Hopfen, Gerste und Wasser von den Microservices über Core holen
	$http({
		method: "get",
		url: url + "api/unitPrices",
	}).then(function(response) {
		$scope.unitPrices = response.data;
		$log.info(response);
		
		$scope.unitPriceHop = parseInt(response.data.unitPriceHop) / 100;
		$scope.unitPriceCereal = parseInt(response.data.unitPriceCereal) / 100;
		$scope.unitPriceWater = parseInt(response.data.unitPriceWater) / 100;
		
		/*
		// Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.unitPrices));
		*/
		
	}, function (response) {
		alert("Error " + response.status + " " + response.statusText + " " + response.data);
	});	
	
	
	// POST unitPrices - Einkaufspreise für Grundstoffe Hopfen, Gerste und Wasser
    $scope.postUnitPrices = function() {
        $http({
            method: "post",
            url: url + "api/unitPrices",
            data: {
				/* Übergabe Preise umgerechnet auf Cent pro Tonne / 1000 Liter */
                unitPriceHop: $scope.unitPriceHop * 100,
                unitPriceCereal: $scope.unitPriceCereal * 100,
                unitPriceWater: $scope.unitPriceWater * 100,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			$log.info(response);
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				unitPriceHop: $scope.unitPriceHop * 100,
                unitPriceCereal: $scope.unitPriceCereal * 100,
                unitPriceWater: $scope.unitPriceWater * 100,
			}
			alert("Ausgabe JSON unitPrices: " + JSON.stringify(testData));
			*/
			
		});
	}
	
});





//////////////////////////////
//Hopfenanbau
//////////////////////////////


app.controller('hopFarmerController', function($scope, $http, $log) {
	
	// Statusvariablen für die Neuanlage von Unternehmen setzen
	$scope.initCreateFormHopFarmer = function() {
		$scope.createHopFarmerForm = false;
		$scope.showAddHopFarmerButton = true;
	}

	
	// Statusvariablen für jeden HopFarmer in HopFarmers.companies setzten im ng-Repeat
	$scope.initHopFarmers = function(hopFarmer) {
		hopFarmer.hopFarmerEditStatus = false;
		hopFarmer.showHopFarmerDataDetails = false;
		hopFarmer.companyHopFarmerEdit = true;
	}
	
	
	// Unternehmensdaten bearbeiten und speichern durch Buttons je Datensatz
	$scope.changeHopFarmerStatus = function(hopFarmer) {
		hopFarmer.hopFarmerEditStatus = !hopFarmer.hopFarmerEditStatus;
	}
	
	
	// Felder zum Bearbeiten freigeben
	$scope.enableCompanyHopFarmerEdit = function(hopFarmer) {
		hopFarmer.companyHopFarmerEdit = false;
	}
	
	
	// Felder zum Bearbeiten und Sperren
	$scope.disableCompanyHopFarmerEdit = function(hopFarmer) {
		hopFarmer.companyHopFarmerEdit = true;
	}
	
	
	// Form für createHopFarmerForm beim Klick auf "+" einblenden
	$scope.openHopFarmerForm = function() {
		$scope.createHopFarmerForm = true;
		$scope.showAddHopFarmerButton = false;
	}	
	
	
	// Form für createHopFarmerForm beim Klick auf "Abbrechen" oder "Speichern" ausblenden
	$scope.closeHopFarmerForm = function() {
		$scope.createHopFarmerForm = false;
		$scope.showAddHopFarmerButton = true;
	}
	
	
	// Details aufklappen / zuklappen - Anzeige der kompletten Informationen für eine Unternehmen oder nur Kurzübersicht
	$scope.showHopFarmerDetails = function(hopFarmer) {
		hopFarmer.showHopFarmerDataDetails = !hopFarmer.showHopFarmerDataDetails;
		if (hopFarmer.activeButton == "activeButton") {
			hopFarmer.activeButton = "";
		}
		else if (hopFarmer.activeButton != "activeButton") {
			hopFarmer.activeButton = "activeButton";
		}

	}
	
	// GET company (hopFarmer) - Alle Daten der vorhandenen Unternehmen holen
	$http({
		method: "get",
		url: url + "api/company/hopFarmer",
	}).then(function(response) {
		$scope.hopFarmers = response.data;
		$log.info(response);	
		
			/* 
			Funktionsaufrufe
			Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
			Berechnung der benötigen Produktionskapazität aller Unternehmen je Qualitätsstufe -> direkte Nachfolger
			*/
			$scope.actualPerformanceCalculationHopFarmer();
			$scope.getTargetCapacityBreweryHop();

		/* 
		Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.hopFarmers));
		*/
		
	}, function (response) {
		alert("Error " + response.status + " " + response.statusText + " " + response.data);
	});	
	
	
	// Seite über Aktualisieren-Button neu laden
	$scope.getHopFarmers = function() {
		$http({
			method: "get",
			url: url + "api/company/hopFarmer",
		}).then(function(response) {
			$scope.hopFarmers = response.data;
			$log.info(response);	
			
			/* 
			Funktionsaufrufe
			Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
			Berechnung der benötigen Produktionskapazität aller Unternehmen je Qualitätsstufe -> direkte Nachfolger
			*/
			$scope.actualPerformanceCalculationHopFarmer();
			$scope.getTargetCapacityBreweryHop();

		/* 
		Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.hopFarmers));
		*/
		
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});	
		
	}
	
	
	// POST company (hopFarmer) - Unternehmen anlegen
	$scope.newHopFarmer = {};
	$scope.postNewHopFarmer = function() {
        $http({
            method: "post",
            url: url + "api/company",
            data: {
				// Daten für neues Unternehmen
                companyType: "hopFarmer",
				companySize: $scope.newHopFarmer.companySize,
				qualityType: $scope.newHopFarmer.qualityType,
				budget: $scope.newHopFarmer.budget * 100,
				warehousePercentage: $scope.newHopFarmer.warehousePercentage,
				profitMargin: $scope.newHopFarmer.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getHopFarmers();
			
				// Objekt für Eingabeformular wieder leeren
				$scope.newHopFarmer = {};
			
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				companyType: "hopFarmer",
				companySize: $scope.newHopFarmer.companySize,
				qualityType: $scope.newHopFarmer.qualityType,
				budget: $scope.newHopFarmer.budget,
				warehousePercentage: $scope.newHopFarmer.warehousePercentage,
				profitMargin: $scope.newHopFarmer.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	// POST randomCompany (hopFarmer) - Unternehmen automatisch generieren lassen
	$scope.postRandomHopFarmer = function() {
        $http({
            method: "post",
            url: url + "api/randomCompany",
            data: {
				// Unternehmenstyp übergeben
                companyType: "hopFarmer",
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getHopFarmers();
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});
	}
	
	
	// PUT company (hopFarmer) - Felder: Zielfüllgrad, Gewinnaufschlag des Unternehmens updaten
	$scope.updateHopFarmer = function(hopFarmer) {
		$scope.hopFarmerData = hopFarmer;
        $http({
            method: "put",
            url: url + "api/company",
            data: {
				// Daten für bestehendes Unternehmen updaten
				companyId: $scope.hopFarmerData.companyId,
                companyType: "hopFarmer",
				warehousePercentage: $scope.hopFarmerData.warehousePercentage,
				profitMargin: $scope.hopFarmerData.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				companyId: $scope.hopFarmerData.companyId,
                companyType: "hopFarmer",
				warehousePercentage: $scope.hopFarmerData.warehousePercentage,
				profitMargin: $scope.hopFarmerData.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	

	// DELETE company (hopFarmer) - Unternehmen löschen
	$scope.deleteHopFarmer = function(hopFarmer) {
		$scope.hopFarmerData = hopFarmer;
		$http({
			method: "delete",
			url: url + "api/company",
			data: {
				// Unternehmen löschen
				companyId: $scope.hopFarmerData.companyId,
                companyType: "hopFarmer",
			},
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getHopFarmers();

		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);

			/* 
			Testing JSON Output
			var testData = {
				companyId: $scope.hopFarmerData.companyId,
                companyType: "hopFarmer",
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	/* 
	Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
	Variablen für Soll-Ist-Vergleich je Qualitätsstufe - Mengeneinheit KG / Liter

	Produktionsmengen pro Tag:
	Niedrige Qualität - 1000 KG / Liter
	Mittlere - Qualität - 500 KG / Liter
	Hohe Qualität - 250 KG Liter
		
	-> AUSNAHME WASSERWERKE:
	Niedrige Qualität - 5000 KG / Liter
	Mittlere - Qualität - 2500 KG / Liter
	Hohe Qualität - 1250 KG Liter
	
	Rückwärtsrechnung der Produktion und Mischverhältnisse im Herstellungsprozess
	
	Mischverhältnisse:
	4.0 Wasser + 1.0 Gerste = 0.8 Malz
	1.5 Wasser + 0.5 Malz + 0.2 Hopfen = 1.0 Bier

	Booster bei Produktionsgeschwindigkeit:
	Unternehmensgröße groß - Produktionsmenge * 2.0
	*/
	$scope.actualPerformanceCalculationHopFarmer = function () {
		
		// Ist-Variablen - Aktuelle Produktionskapazität
		$scope.actualPerformanceHopFarmerLow = 0;
		$scope.actualPerformanceHopFarmerMedium = 0;
		$scope.actualPerformanceHopFarmerHigh = 0;
		
		// Berechnung der Produktionskapazitäten aus ng-repeat
		var actualPerformanceLow = 0;
		var actualPerformanceMedium = 0;
		var actualPerformanceHigh = 0;
		var boosterMultiplication = 1;
		
		angular.forEach($scope.hopFarmers.companies, function(hopFarmer){
			// Update boosterMultiplication mit 2 bei Unternehmensgröße groß
			if (hopFarmer.companySize.toLowerCase() == "big") {
				boosterMultiplication = 2;
			}
				
			// Produktionskapazitäten hochzählen je Qualität	
			// Niedrig 
			if (hopFarmer.qualityType.toLowerCase() == "low") {
				actualPerformanceLow += 1000 * parseInt(boosterMultiplication);
			}
			// Mittel
			else if (hopFarmer.qualityType.toLowerCase() == "medium") {
				actualPerformanceMedium += 500 * parseInt(boosterMultiplication);
			}
			// Hoc
			else if (hopFarmer.qualityType.toLowerCase() == "high") {
				actualPerformanceHigh += 250 * parseInt(boosterMultiplication);
			}
			
			// boosterMultiplication wieder auf 1 setzen
			boosterMultiplication = 1;
		});
		
		// IST-WERTE
		$scope.actualPerformanceHopFarmerLow = actualPerformanceLow;
		$scope.actualPerformanceHopFarmerMedium = actualPerformanceMedium;
		$scope.actualPerformanceHopFarmerHigh = actualPerformanceHigh;
		
		/*
		// Testing Ist-Produktionskapazität
		alert("Actual Performance: " + "Low " + $scope.actualPerformanceHopFarmerLow + " " + "Medium " + $scope.actualPerformanceHopFarmerMedium + " " + "High " + $scope.actualPerformanceHopFarmerHigh);
		*/
		
	}
		
	$scope.getTargetCapacityBreweryHop = function() {

		// Berechnung der benötigten Produktionskapazitäten - Brauereien
		// GET company (brewery) - Alle Daten der vorhandenen Unternehmen holen
		$http({
			method: "get",
			url: url + "api/company/brewery",
		}).then(function(response) {
			$scope.brewerys = response.data;
			
			// Funktionsaufruf - Berechnung der Soll-Kapazitäten
			$scope.targetCalculationBreweryHop();

			$log.info(response);	

			/*
			Testing JSON Input
			alert("Eingabe JSON company: " + JSON.stringify($scope.brewerys));
			*/

		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});	


	}

	$scope.targetCalculationBreweryHop = function() {
		
		$scope.actualPerformanceBreweryLow = 0;
		$scope.actualPerformanceBreweryMedium = 0;
		$scope.actualPerformanceBreweryHigh = 0;
		
		// Ist-Variablen - Aktuelle Produktionskapazität
		$scope.actualPerformanceBreweryLow = 0;
		$scope.actualPerformanceBreweryMedium = 0;
		$scope.actualPerformanceBreweryHigh = 0;

		// Berechnung der Produktionskapazitäten aus ng-repeat
		var actualPerformanceLow = 0;
		var actualPerformanceMedium = 0;
		var actualPerformanceHigh = 0;
		var boosterMultiplication = 1;

		angular.forEach($scope.brewerys.companies, function(brewery){
			// Update boosterMultiplication mit 2 bei Unternehmensgröße groß
			if (brewery.companySize.toLowerCase() == "big") {
				boosterMultiplication = 2;
			}

			// Produktionskapazitäten hochzählen je Qualität	
			// Niedrig 
			if (brewery.qualityType.toLowerCase() == "low") {
				actualPerformanceLow += 1000 * parseInt(boosterMultiplication);
			}
			// Mittel
			else if (brewery.qualityType.toLowerCase() == "medium") {
				actualPerformanceMedium += 500 * parseInt(boosterMultiplication);
			}
			// Hoc
			else if (brewery.qualityType.toLowerCase() == "high") {
				actualPerformanceHigh += 250 * parseInt(boosterMultiplication);
			}

			// boosterMultiplication wieder auf 1 setzen
			boosterMultiplication = 1;
		});
		
		// SOLL-WERTE - entsprechend dem geforderten Mischverhältnis
		$scope.actualPerformanceBreweryLow = parseInt(actualPerformanceLow) * 0.2;
		$scope.actualPerformanceBreweryMedium = parseInt(actualPerformanceMedium) * 0.2;
		$scope.actualPerformanceBreweryHigh = parseInt(actualPerformanceHigh) * 0.2;
		
		/*
		// Testing Soll-Produktionskapazität
		alert("Target Performance: " + "Low " + $scope.actualPerformanceBreweryLow + " " + "Medium " + $scope.actualPerformanceBreweryMedium + " " + "High " + $scope.actualPerformanceBreweryHigh);
		*/
		
		// Soll-Ist-Vergleich für Darstellung im Frontend
		$scope.showCapacityHop();

	}
		
	$scope.showCapacityHop = function() {

		// Bedingungen für Diagramm in User-Ansicht
		// Berechnung Soll-/ Ist-Vergleich Produktionskapazitäten
		$scope.capacityComparisonHopFarmerLow = parseInt($scope.actualPerformanceHopFarmerLow) - parseInt($scope.actualPerformanceBreweryLow);
		$scope.capacityComparisonHopFarmerMedium = parseInt($scope.actualPerformanceHopFarmerMedium) - parseInt($scope.actualPerformanceBreweryMedium);
		$scope.capacityComparisonHopFarmerHigh = parseInt($scope.actualPerformanceHopFarmerHigh) - parseInt($scope.actualPerformanceBreweryHigh);

		// Hintergrund rot (negative Produktionskapazität) oder blau (optimale oder positive Produktionskapazität)
		// Anzeige Balken (negative Produktionskapazität) links oder rechts (optimale oder positive Produktionskapazität)
		// Niedrig
		if (parseInt($scope.capacityComparisonHopFarmerLow) >= 0) {
			$scope.colorCapacityLow = "w3-blue";
			$scope.showCapacityDeficitLow = "";
			$scope.showCapacityOverflowLow = "true";
		}
		else {
			$scope.colorCapacityLow = "beer-red";
			$scope.showCapacityOverflowLow = "";
			$scope.showCapacityDeficitLow = "true";
		}
		// Mittel
		if (parseInt($scope.capacityComparisonHopFarmerMedium) >= 0) {
			$scope.colorCapacityMedium = "w3-blue";
			$scope.showCapacityDeficitMedium = ""
			$scope.showCapacityOverflowMedium = "true";
		}
		else {
			$scope.colorCapacityMedium = "beer-red";
			$scope.showCapacityOverflowMedium = "";
			$scope.showCapacityDeficitMedium = "true";
		}
		// Hoch
		if (parseInt($scope.capacityComparisonHopFarmerHigh) >= 0) {
			$scope.colorCapacityHigh = "w3-blue";
			$scope.showCapacityDeficitHigh = "";
			$scope.showCapacityOverflowHigh = "true";
		}
		else {
			$scope.colorCapacityHigh = "beer-red";
			$scope.showCapacityOverflowHigh = "";
			$scope.showCapacityDeficitHigh = "true";
		}
			
	}
	
});





//////////////////////////////
// Gerstenanbau
//////////////////////////////


app.controller('cerealFarmerController', function($scope, $http, $log) {
	
	// Statusvariablen für die Neuanlage von Unternehmen setzen
	$scope.initCreateFormCerealFarmer = function() {
		$scope.createCerealFarmerForm = false;
		$scope.showAddCerealFarmerButton = true;
	}

	
	// Statusvariablen für jeden CerealFarmer in CerealFarmers.companies setzten im ng-Repeat
	$scope.initCerealFarmers = function(cerealFarmer) {
		cerealFarmer.cerealFarmerEditStatus = false;
		cerealFarmer.showCerealFarmerDataDetails = false;
		cerealFarmer.companyCerealFarmerEdit = true;
	}
	
	
	// Unternehmensdaten bearbeiten und speichern durch Buttons je Datensatz
	$scope.changeCerealFarmerStatus = function(cerealFarmer) {
		cerealFarmer.cerealFarmerEditStatus = !cerealFarmer.cerealFarmerEditStatus;
	}
	
	
	// Felder zum Bearbeiten freigeben
	$scope.enableCompanyCerealFarmerEdit = function(cerealFarmer) {
		cerealFarmer.companyCerealFarmerEdit = false;
	}
	
	
	// Felder zum Bearbeiten und Sperren
	$scope.disableCompanyCerealFarmerEdit = function(cerealFarmer) {
		cerealFarmer.companyCerealFarmerEdit = true;
	}
	
	
	// Form für createCerealFarmerForm beim Klick auf "+" einblenden
	$scope.openCerealFarmerForm = function() {
		$scope.createCerealFarmerForm = true;
		$scope.showAddCerealFarmerButton = false;
	}	
	
	
	// Form für createCerealFarmerForm beim Klick auf "Abbrechen" oder "Speichern" ausblenden
	$scope.closeCerealFarmerForm = function() {
		$scope.createCerealFarmerForm = false;
		$scope.showAddCerealFarmerButton = true;
	}
	
	
	// Details aufklappen / zuklappen - Anzeige der kompletten Informationen für eine Unternehmen oder nur Kurzübersicht
	$scope.showCerealFarmerDetails = function(cerealFarmer) {
		cerealFarmer.showCerealFarmerDataDetails = !cerealFarmer.showCerealFarmerDataDetails;
		if (cerealFarmer.activeButton == "activeButton") {
			cerealFarmer.activeButton = "";
		}
		else if (cerealFarmer.activeButton != "activeButton") {
			cerealFarmer.activeButton = "activeButton";
		}

	}
	
	// GET company (cerealFarmer) - Alle Daten der vorhandenen Unternehmen holen
	$http({
		method: "get",
		url: url + "api/company/cerealFarmer",
	}).then(function(response) {
		$scope.cerealFarmers = response.data;
		$log.info(response);	
		
			/* 
			Funktionsaufrufe
			Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
			Berechnung der benötigen Produktionskapazität aller Unternehmen je Qualitätsstufe -> direkte Nachfolger
			*/
			$scope.actualPerformanceCalculationCerealFarmer();
			$scope.getTargetCapacityMalter();

		/* 
		Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.cerealFarmers));
		*/
		
	}, function (response) {
		alert("Error " + response.status + " " + response.statusText + " " + response.data);
	});	
	
	
	// Seite über Aktualisieren-Button neu laden
	$scope.getCerealFarmers = function() {
		$http({
			method: "get",
			url: url + "api/company/cerealFarmer",
		}).then(function(response) {
			$scope.cerealFarmers = response.data;
			$log.info(response);	
			
			/* 
			Funktionsaufrufe
			Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
			Berechnung der benötigen Produktionskapazität aller Unternehmen je Qualitätsstufe -> direkte Nachfolger
			*/
			$scope.actualPerformanceCalculationCerealFarmer();
			$scope.getTargetCapacityMalter();

		/* 
		Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.cerealFarmers));
		*/
		
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});	
		
	}
	
	
	// POST company (cerealFarmer) - Unternehmen anlegen
	$scope.newCerealFarmer = {};
	$scope.postNewCerealFarmer = function() {
        $http({
            method: "post",
            url: url + "api/company",
            data: {
				// Daten für neues Unternehmen
                companyType: "cerealFarmer",
				companySize: $scope.newCerealFarmer.companySize,
				qualityType: $scope.newCerealFarmer.qualityType,
				budget: $scope.newCerealFarmer.budget * 100,
				warehousePercentage: $scope.newCerealFarmer.warehousePercentage,
				profitMargin: $scope.newCerealFarmer.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getCerealFarmers();
			
				// Objekt für Eingabeformular wieder leeren
				$scope.newCerealFarmer = {};
			
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				companyType: "cerealFarmer",
				companySize: $scope.newCerealFarmer.companySize,
				qualityType: $scope.newCerealFarmer.qualityType,
				budget: $scope.newCerealFarmer.budget,
				warehousePercentage: $scope.newCerealFarmer.warehousePercentage,
				profitMargin: $scope.newCerealFarmer.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	// POST randomCompany (cerealFarmer) - Unternehmen automatisch generieren lassen
	$scope.postRandomCerealFarmer = function() {
        $http({
            method: "post",
            url: url + "api/randomCompany",
            data: {
				// Unternehmenstyp übergeben
                companyType: "cerealFarmer",
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getCerealFarmers();
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});
	}

	
	// PUT company (cerealFarmer) - Felder: Zielfüllgrad, Gewinnaufschlag des Unternehmens updaten
	$scope.updateCerealFarmer = function(cerealFarmer) {
		$scope.cerealFarmerData = cerealFarmer;
        $http({
            method: "put",
            url: url + "api/company",
            data: {
				// Daten für bestehendes Unternehmen updaten
				companyId: $scope.cerealFarmerData.companyId,
                companyType: "cerealFarmer",
				warehousePercentage: $scope.cerealFarmerData.warehousePercentage,
				profitMargin: $scope.cerealFarmerData.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				companyId: $scope.cerealFarmerData.companyId,
                companyType: "cerealFarmer",
				warehousePercentage: $scope.cerealFarmerData.warehousePercentage,
				profitMargin: $scope.cerealFarmerData.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	

	// DELETE company (cerealFarmer) - Unternehmen löschen
	$scope.deleteCerealFarmer = function(cerealFarmer) {
		$scope.cerealFarmerData = cerealFarmer;
		$http({
			method: "delete",
			url: url + "api/company",
			data: {
				// Unternehmen löschen
				companyId: $scope.cerealFarmerData.companyId,
                companyType: "cerealFarmer",
			},
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getCerealFarmers();

		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);

			/* 
			Testing JSON Output
			var testData = {
				companyId: $scope.cerealFarmerData.companyId,
                companyType: "cerealFarmer",
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	/* 
	Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
	Variablen für Soll-Ist-Vergleich je Qualitätsstufe - Mengeneinheit KG / Liter

	Produktionsmengen pro Tag:
	Niedrige Qualität - 1000 KG / Liter
	Mittlere - Qualität - 500 KG / Liter
	Hohe Qualität - 250 KG Liter
		
	-> AUSNAHME WASSERWERKE:
	Niedrige Qualität - 5000 KG / Liter
	Mittlere - Qualität - 2500 KG / Liter
	Hohe Qualität - 1250 KG Liter
	
	Rückwärtsrechnung der Produktion und Mischverhältnisse im Herstellungsprozess
	
	Mischverhältnisse:
	4.0 Wasser + 1.0 Gerste = 0.8 Malz
	1.5 Wasser + 0.5 Malz + 0.2 Hopfen = 1.0 Bier

	Booster bei Produktionsgeschwindigkeit:
	Unternehmensgröße groß - Produktionsmenge * 2.0
	*/
	$scope.actualPerformanceCalculationCerealFarmer = function () {
		
		// Ist-Variablen - Aktuelle Produktionskapazität
		$scope.actualPerformanceCerealFarmerLow = 0;
		$scope.actualPerformanceCerealFarmerMedium = 0;
		$scope.actualPerformanceCerealFarmerHigh = 0;
		
		// Berechnung der Produktionskapazitäten aus ng-repeat
		var actualPerformanceLow = 0;
		var actualPerformanceMedium = 0;
		var actualPerformanceHigh = 0;
		var boosterMultiplication = 1;
		
		angular.forEach($scope.cerealFarmers.companies, function(cerealFarmer){
			// Update boosterMultiplication mit 2 bei Unternehmensgröße groß
			if (cerealFarmer.companySize.toLowerCase() == "big") {
				boosterMultiplication = 2;
			}
				
			// Produktionskapazitäten hochzählen je Qualität	
			// Niedrig 
			if (cerealFarmer.qualityType.toLowerCase() == "low") {
				actualPerformanceLow += 1000 * parseInt(boosterMultiplication);
			}
			// Mittel
			else if (cerealFarmer.qualityType.toLowerCase() == "medium") {
				actualPerformanceMedium += 500 * parseInt(boosterMultiplication);
			}
			// Hoc
			else if (cerealFarmer.qualityType.toLowerCase() == "high") {
				actualPerformanceHigh += 250 * parseInt(boosterMultiplication);
			}
			
			// boosterMultiplication wieder auf 1 setzen
			boosterMultiplication = 1;
		});
		
		// IST-Werte
		$scope.actualPerformanceCerealFarmerLow = actualPerformanceLow;
		$scope.actualPerformanceCerealFarmerMedium = actualPerformanceMedium;
		$scope.actualPerformanceCerealFarmerHigh = actualPerformanceHigh;
		
		/*
		// Testing Ist-Produktionskapazität
		alert("Actual Performance: " + "Low " + $scope.actualPerformanceCerealFarmerLow + " " + "Medium " + $scope.actualPerformanceCerealFarmerMedium + " " + "High " + $scope.actualPerformanceCerealFarmerHigh);
		*/
	
	}
		
	$scope.getTargetCapacityMalter = function() {
			
		// Berechnung der benötigten Produktionskapazitäten - Brauereien
		// GET company (malter) - Alle Daten der vorhandenen Unternehmen holen
		$http({
			method: "get",
			url: url + "api/company/malter",
		}).then(function(response) {
			$scope.malters = response.data;
			
			// Funktionsaufruf - Berechnung der Soll-Kapazitäten
			$scope.targetCalculationMalter();

			$log.info(response);	

			/*
			Testing JSON Input
			alert("Eingabe JSON company: " + JSON.stringify($scope.malters));
			*/

		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});	
		
	}
	
	$scope.targetCalculationMalter = function() {
		
		$scope.actualPerformanceMalterLow = 0;
		$scope.actualPerformanceMalterMedium = 0;
		$scope.actualPerformanceMalterHigh = 0;
		
		// Ist-Variablen - Aktuelle Produktionskapazität
		$scope.actualPerformanceMalterLow = 0;
		$scope.actualPerformanceMalterMedium = 0;
		$scope.actualPerformanceMalterHigh = 0;

		// Berechnung der Produktionskapazitäten aus ng-repeat
		var actualPerformanceLow = 0;
		var actualPerformanceMedium = 0;
		var actualPerformanceHigh = 0;
		var boosterMultiplication = 1;

		angular.forEach($scope.malters.companies, function(malter){
			// Update boosterMultiplication mit 2 bei Unternehmensgröße groß
			if (malter.companySize.toLowerCase() == "big") {
				boosterMultiplication = 2;
			}

			// Produktionskapazitäten hochzählen je Qualität	
			// Niedrig 
			if (malter.qualityType.toLowerCase() == "low") {
				actualPerformanceLow += 1000 * parseInt(boosterMultiplication);
			}
			// Mittel
			else if (malter.qualityType.toLowerCase() == "medium") {
				actualPerformanceMedium += 500 * parseInt(boosterMultiplication);
			}
			// Hoc
			else if (malter.qualityType.toLowerCase() == "high") {
				actualPerformanceHigh += 250 * parseInt(boosterMultiplication);
			}

			// boosterMultiplication wieder auf 1 setzen
			boosterMultiplication = 1;
		});
		
		// SOLL-WERTE - entsprechend dem geforderten Mischverhältnis
		$scope.actualPerformanceMalterLow = parseInt(actualPerformanceLow) * 1.25;
		$scope.actualPerformanceMalterMedium = parseInt(actualPerformanceMedium) * 1.25;
		$scope.actualPerformanceMalterHigh = parseInt(actualPerformanceHigh) * 1.25;
		
		/*
		// Testing Soll-Produktionskapazität
		alert("Target Performance: " + "Low " + $scope.actualPerformanceMalterLow + " " + "Medium " + $scope.actualPerformanceMalterMedium + " " + "High " + $scope.actualPerformanceMalterHigh);
		*/
		
		// Soll-Ist-Vergleich für Darstellung im Frontend
		$scope.showCapacityCerealFarmer();

	}

	$scope.showCapacityCerealFarmer = function() {
		
		// Bedingungen für Diagramm in User-Ansicht
		// Berechnung Soll-/ Ist-Vergleich Produktionskapazitäten
		$scope.capacityComparisonCerealFarmerLow = parseInt($scope.actualPerformanceCerealFarmerLow) - parseInt($scope.actualPerformanceMalterLow);
		$scope.capacityComparisonCerealFarmerMedium = parseInt($scope.actualPerformanceCerealFarmerMedium) - parseInt($scope.actualPerformanceMalterMedium);
		$scope.capacityComparisonCerealFarmerHigh = parseInt($scope.actualPerformanceCerealFarmerHigh) - parseInt($scope.actualPerformanceMalterHigh);
		
		// Hintergrund rot (negative Produktionskapazität) oder blau (optimale oder positive Produktionskapazität)
		// Anzeige Balken (negative Produktionskapazität) links oder rechts (optimale oder positive Produktionskapazität)
		// Niedrig
		if (parseInt($scope.capacityComparisonCerealFarmerLow) >= 0) {
			$scope.colorCapacityLow = "w3-blue";
			$scope.showCapacityDeficitLow = "";
			$scope.showCapacityOverflowLow = "true";
		}
		else {
			$scope.colorCapacityLow = "beer-red";
			$scope.showCapacityOverflowLow = "";
			$scope.showCapacityDeficitLow = "true";
		}
		// Mittel
		if (parseInt($scope.capacityComparisonCerealFarmerMedium) >= 0) {
			$scope.colorCapacityMedium = "w3-blue";
			$scope.showCapacityDeficitMedium = "";
			$scope.showCapacityOverflowMedium = "true";
		}
		else {
			$scope.colorCapacityMedium = "beer-red";
			$scope.showCapacityOverflowMedium = "";
			$scope.showCapacityDeficitMedium = "true";
		}
		// Hoch
		if (parseInt($scope.capacityComparisonCerealFarmerHigh) >= 0) {
			$scope.colorCapacityHigh = "w3-blue";
			$scope.showCapacityDeficitHigh = "";
			$scope.showCapacityOverflowHigh = "true";
		}
		else {
			$scope.colorCapacityHigh = "beer-red";
			$scope.showCapacityOverflowHigh = "";
			$scope.showCapacityDeficitHigh = "true";
		}
		
	}
	
});





//////////////////////////////
// Wasserwerke
//////////////////////////////


app.controller('waterplantController', function($scope, $http, $log) {
	
	// Statusvariablen für die Neuanlage von Unternehmen setzen
	$scope.initCreateFormWaterplant = function() {
		$scope.createWaterplantForm = false;
		$scope.showAddWaterplantButton = true;
	}

	
	// Statusvariablen für jede Waterplant in Waterplants.companies setzten im ng-Repeat
	$scope.initWaterplants = function(waterplant) {
		waterplant.waterplantEditStatus = false;
		waterplant.showWaterplantDataDetails = false;
		waterplant.companyWaterplantEdit = true;
	}
	
	
	// Unternehmensdaten bearbeiten und speichern durch Buttons je Datensatz
	$scope.changeWaterplantStatus = function(waterplant) {
		waterplant.waterplantEditStatus = !waterplant.waterplantEditStatus;
	}
	
	
	// Felder zum Bearbeiten freigeben
	$scope.enableCompanyWaterplantEdit = function(waterplant) {
		waterplant.companyWaterplantEdit = false;
	}
	
	
	// Felder zum Bearbeiten und Sperren
	$scope.disableCompanyWaterplantEdit = function(waterplant) {
		waterplant.companyWaterplantEdit = true;
	}
	
	
	// Form für createWaterplantForm beim Klick auf "+" einblenden
	$scope.openWaterplantForm = function() {
		$scope.createWaterplantForm = true;
		$scope.showAddWaterplantButton = false;
	}	
	
	
	// Form für createWaterplantForm beim Klick auf "Abbrechen" oder "Speichern" ausblenden
	$scope.closeWaterplantForm = function() {
		$scope.createWaterplantForm = false;
		$scope.showAddWaterplantButton = true;
	}
	
	
	// Details aufklappen / zuklappen - Anzeige der kompletten Informationen für eine Unternehmen oder nur Kurzübersicht
	$scope.showWaterplantDetails = function(waterplant) {
		waterplant.showWaterplantDataDetails = !waterplant.showWaterplantDataDetails;
		if (waterplant.activeButton == "activeButton") {
			waterplant.activeButton = "";
		}
		else if (waterplant.activeButton != "activeButton") {
			waterplant.activeButton = "activeButton";
		}

	}
	
	// GET company (waterplant) - Alle Daten der vorhandenen Unternehmen holen
	$http({
		method: "get",
		url: url + "api/company/waterplant",
	}).then(function(response) {
		$scope.waterplants = response.data;
		$log.info(response);	
		
			/* 
			Funktionsaufrufe
			Aktualisieren
			Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
			Berechnung der benötigen Produktionskapazität aller Unternehmen je Qualitätsstufe -> direkte Nachfolger
			*/
			$scope.getWaterplants();

		/* 
		Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.waterplants));
		*/
		
	}, function (response) {
		alert("Error " + response.status + " " + response.statusText + " " + response.data);
	});	
	
	
	// Seite über Aktualisieren-Button neu laden
	$scope.getWaterplants = function() {
		$http({
			method: "get",
			url: url + "api/company/waterplant",
		}).then(function(response) {
			$scope.waterplants = response.data;
			$log.info(response);	
			
			/* 
			Funktionsaufrufe
			Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
			Berechnung der benötigen Produktionskapazität aller Unternehmen je Qualitätsstufe -> direkte Nachfolger
			*/
			$scope.actualPerformanceCalculationWaterplant();
			$scope.getTargetCapacityWaterMalter();
			$scope.getTargetCapacityWaterBrewery();
			
		/* 
		Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.waterplants));
		*/
		
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});	
		
	}
	
	
	// POST company (waterplant) - Unternehmen anlegen
	$scope.newWaterplant = {};
	$scope.postNewWaterplant = function() {
        $http({
            method: "post",
            url: url + "api/company",
            data: {
				// Daten für neues Unternehmen
                companyType: "waterplant",
				companySize: $scope.newWaterplant.companySize,
				qualityType: $scope.newWaterplant.qualityType,
				budget: $scope.newWaterplant.budget * 100,
				warehousePercentage: $scope.newWaterplant.warehousePercentage,
				profitMargin: $scope.newWaterplant.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getWaterplants();
			
				// Objekt für Eingabeformular wieder leeren
				$scope.newWaterplant = {};
			
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				companyType: "waterplant",
				companySize: $scope.newWaterplant.companySize,
				qualityType: $scope.newWaterplant.qualityType,
				budget: $scope.newWaterplant.budget,
				warehousePercentage: $scope.newWaterplant.warehousePercentage,
				profitMargin: $scope.newWaterplant.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	// POST randomCompany (waterplant) - Unternehmen automatisch generieren lassen
	$scope.postRandomWaterplant = function() {
        $http({
            method: "post",
            url: url + "api/randomCompany",
            data: {
				// Unternehmenstyp übergeben
                companyType: "waterplant",
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getWaterplants();
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});
	}
	
	
	// PUT company (waterplant) - Felder: Zielfüllgrad, Gewinnaufschlag des Unternehmens updaten
	$scope.updateWaterplant = function(waterplant) {
		$scope.waterplantData = waterplant;
        $http({
            method: "put",
            url: url + "api/company",
            data: {
				// Daten für bestehendes Unternehmen updaten
				companyId: $scope.waterplantData.companyId,
                companyType: "waterplant",
				warehousePercentage: $scope.waterplantData.warehousePercentage,
				profitMargin: $scope.waterplantData.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				companyId: $scope.waterplantData.companyId,
                companyType: "waterplant",
				warehousePercentage: $scope.waterplantData.warehousePercentage,
				profitMargin: $scope.waterplantData.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	

	// DELETE company (waterplant) - Unternehmen löschen
	$scope.deleteWaterplant = function(waterplant) {
		$scope.waterplantData = waterplant;
		$http({
			method: "delete",
			url: url + "api/company",
			data: {
				// Unternehmen löschen
				companyId: $scope.waterplantData.companyId,
                companyType: "waterplant",
			},
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getWaterplants();

		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);

			/* 
			Testing JSON Output
			var testData = {
				companyId: $scope.waterplantData.companyId,
                companyType: "waterplant",
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	/* 
	Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
	Variablen für Soll-Ist-Vergleich je Qualitätsstufe - Mengeneinheit KG / Liter

	Produktionsmengen pro Tag:
	Niedrige Qualität - 1000 KG / Liter
	Mittlere - Qualität - 500 KG / Liter
	Hohe Qualität - 250 KG Liter
		
	-> AUSNAHME WASSERWERKE:
	Niedrige Qualität - 5000 KG / Liter
	Mittlere - Qualität - 2500 KG / Liter
	Hohe Qualität - 1250 KG Liter
	
	Rückwärtsrechnung der Produktion und Mischverhältnisse im Herstellungsprozess
	
	Mischverhältnisse:
	4.0 Wasser + 1.0 Gerste = 0.8 Malz
	1.5 Wasser + 0.5 Malz + 0.2 Hopfen = 1.0 Bier

	Booster bei Produktionsgeschwindigkeit:
	Unternehmensgröße groß - Produktionsmenge * 2.0
	*/
	$scope.actualPerformanceCalculationWaterplant = function () {
		
		// Ist-Variablen - Aktuelle Produktionskapazität
		$scope.actualPerformanceWaterplantLow = 0;
		$scope.actualPerformanceWaterplantMedium = 0;
		$scope.actualPerformanceWaterplantHigh = 0;
		
		// Berechnung der Produktionskapazitäten aus ng-repeat
		var actualPerformanceLow = 0;
		var actualPerformanceMedium = 0;
		var actualPerformanceHigh = 0;
		var boosterMultiplication = 1;
		
		angular.forEach($scope.waterplants.companies, function(waterplant){
			// Update boosterMultiplication mit 2 bei Unternehmensgröße groß
			if (waterplant.companySize.toLowerCase() == "big") {
				boosterMultiplication = 2;
			}
				
			// Produktionskapazitäten hochzählen je Qualität	
			// Niedrig 
			if (waterplant.qualityType.toLowerCase() == "low") {
				actualPerformanceLow += 5000 * parseInt(boosterMultiplication);
			}
			// Mittel
			else if (waterplant.qualityType.toLowerCase() == "medium") {
				actualPerformanceMedium += 2500 * parseInt(boosterMultiplication);
			}
			// Hoc
			else if (waterplant.qualityType.toLowerCase() == "high") {
				actualPerformanceHigh += 1250 * parseInt(boosterMultiplication);
			}
			
			// boosterMultiplication wieder auf 1 setzen
			boosterMultiplication = 1;
		});
		
		// IST-Werte
		$scope.actualPerformanceWaterplantLow = actualPerformanceLow;
		$scope.actualPerformanceWaterplantMedium = actualPerformanceMedium;
		$scope.actualPerformanceWaterplantHigh = actualPerformanceHigh;
		
		/*
		// Testing Ist-Produktionskapazität	
		alert("Actual Performance: " + "Low " + $scope.actualPerformanceWaterplantLow + " " + "Medium " + $scope.actualPerformanceWaterplantMedium + " " + "High " + $scope.actualPerformanceWaterplantHigh);
		*/
		
	}
	
	$scope.getTargetCapacityWaterMalter = function() {
		
		// Berechnung der benötigten Produktionskapazitäten - Brauereien
		// GET company (malter) - Alle Daten der vorhandenen Unternehmen holen
		$http({
			method: "get",
			url: url + "api/company/malter",
		}).then(function(response) {
			$scope.malters = response.data;
			
			// Funktionsaufruf - Berechnung der Soll-Kapazitäten
			$scope.targetCalculationWaterMalter();

			$log.info(response);	

			/*
			Testing JSON Input
			alert("Eingabe JSON company: " + JSON.stringify($scope.malters));
			*/

		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});	
		
	}
	
	$scope.targetCalculationWaterMalter = function() {
	
		$scope.actualPerformanceMalterLow = 0;
		$scope.actualPerformanceMalterMedium = 0;
		$scope.actualPerformanceMalterHigh = 0;
		
		// Ist-Variablen - Aktuelle Produktionskapazität
		$scope.actualPerformanceMalterLow = 0;
		$scope.actualPerformanceMalterMedium = 0;
		$scope.actualPerformanceMalterHigh = 0;

		// Berechnung der Produktionskapazitäten aus ng-repeat
		var actualPerformanceLow = 0;
		var actualPerformanceMedium = 0;
		var actualPerformanceHigh = 0;
		var boosterMultiplication = 1;

		angular.forEach($scope.malters.companies, function(malter){
			// Update boosterMultiplication mit 2 bei Unternehmensgröße groß
			if (malter.companySize.toLowerCase() == "big") {
				boosterMultiplication = 2;
			}

			// Produktionskapazitäten hochzählen je Qualität	
			// Niedrig 
			if (malter.qualityType.toLowerCase() == "low") {
				actualPerformanceLow += 1000 * parseInt(boosterMultiplication);
			}
			// Mittel
			else if (malter.qualityType.toLowerCase() == "medium") {
				actualPerformanceMedium += 500 * parseInt(boosterMultiplication);
			}
			// Hoc
			else if (malter.qualityType.toLowerCase() == "high") {
				actualPerformanceHigh += 250 * parseInt(boosterMultiplication);
			}

			// boosterMultiplication wieder auf 1 setzen
			boosterMultiplication = 1;
		});
		
		// SOLL-WERTE - entsprechend dem geforderten Mischverhältnis
		$scope.actualPerformanceMalterLow = parseInt(actualPerformanceLow) * 5;
		$scope.actualPerformanceMalterMedium = parseInt(actualPerformanceMedium) * 5;
		$scope.actualPerformanceMalterHigh = parseInt(actualPerformanceHigh) * 5;
		
		/*
		// Testing Soll-Produktionskapazität
		alert("Target Performance: " + "Low " + $scope.actualPerformanceMalterLow + " " + "Medium " + $scope.actualPerformanceMalterMedium + " " + "High " + $scope.actualPerformanceMalterHigh);
		*/
	
	}
	
	$scope.getTargetCapacityWaterBrewery = function() {
		
		// Berechnung der benötigten Produktionskapazitäten - Brauereien
		// GET company (brewery) - Alle Daten der vorhandenen Unternehmen holen
		$http({
			method: "get",
			url: url + "api/company/brewery",
		}).then(function(response) {
			$scope.brewerys = response.data;
			
			// Funktionsaufruf - Berechnung der Soll-Kapazitäten
			$scope.targetCalculationWaterBrewery();

			$log.info(response);	

			/*
			Testing JSON Input
			alert("Eingabe JSON company: " + JSON.stringify($scope.brewerys));
			*/

		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});	
		
	}
	
	$scope.targetCalculationWaterBrewery = function() {
	
		$scope.actualPerformanceBreweryLow = 0;
		$scope.actualPerformanceBreweryMedium = 0;
		$scope.actualPerformanceBreweryHigh = 0;
		
		// Ist-Variablen - Aktuelle Produktionskapazität
		$scope.actualPerformanceBreweryLow = 0;
		$scope.actualPerformanceBreweryMedium = 0;
		$scope.actualPerformanceBreweryHigh = 0;

		// Berechnung der Produktionskapazitäten aus ng-repeat
		var actualPerformanceLow = 0;
		var actualPerformanceMedium = 0;
		var actualPerformanceHigh = 0;
		var boosterMultiplication = 1;

		angular.forEach($scope.brewerys.companies, function(brewery){
			// Update boosterMultiplication mit 2 bei Unternehmensgröße groß
			if (brewery.companySize.toLowerCase() == "big") {
				boosterMultiplication = 2;
			}

			// Produktionskapazitäten hochzählen je Qualität	
			// Niedrig 
			if (brewery.qualityType.toLowerCase() == "low") {
				actualPerformanceLow += 1000 * parseInt(boosterMultiplication);
			}
			// Mittel
			else if (brewery.qualityType.toLowerCase() == "medium") {
				actualPerformanceMedium += 500 * parseInt(boosterMultiplication);
			}
			// Hoc
			else if (brewery.qualityType.toLowerCase() == "high") {
				actualPerformanceHigh += 250 * parseInt(boosterMultiplication);
			}

			// boosterMultiplication wieder auf 1 setzen
			boosterMultiplication = 1;
		});
		
		// SOLL-WERTE - entsprechend dem geforderten Mischverhältnis
		$scope.actualPerformanceBreweryLow = parseInt(actualPerformanceLow) * 1.5;
		$scope.actualPerformanceBreweryMedium = parseInt(actualPerformanceMedium) * 1.5;
		$scope.actualPerformanceBreweryHigh = parseInt(actualPerformanceHigh) * 1.5;
		
		/*
		// Testing Soll-Produktionskapazität
		alert("Target Performance: " + "Low " + $scope.actualPerformanceBreweryLow + " " + "Medium " + $scope.actualPerformanceBreweryMedium + " " + "High " + $scope.actualPerformanceBreweryHigh);
		*/
		
		// Soll-Ist-Vergleich für Darstellung im Frontend
		$scope.showCapacityWaterplant();
		
	}
	
	$scope.showCapacityWaterplant = function() {
		
		// Bedingungen für Diagramm in User-Ansicht
		// Berechnung Soll-/ Ist-Vergleich Produktionskapazitäten
		$scope.capacityComparisonWaterplantLow = parseInt($scope.actualPerformanceWaterplantLow) - parseInt($scope.actualPerformanceMalterLow) - parseInt($scope.actualPerformanceBreweryLow);
		$scope.capacityComparisonWaterplantMedium = parseInt($scope.actualPerformanceWaterplantMedium) - parseInt($scope.actualPerformanceMalterMedium) - parseInt($scope.actualPerformanceBreweryMedium);
		$scope.capacityComparisonWaterplantHigh = parseInt($scope.actualPerformanceWaterplantHigh) - parseInt($scope.actualPerformanceMalterHigh) - parseInt($scope.actualPerformanceBreweryHigh);
		
		// Hintergrund rot (negative Produktionskapazität) oder blau (optimale oder positive Produktionskapazität)
		// Anzeige Balken (negative Produktionskapazität) links oder rechts (optimale oder positive Produktionskapazität)
		// Niedrig
		if (parseInt($scope.capacityComparisonWaterplantLow) >= 0) {
			$scope.colorCapacityLow = "w3-blue";
			$scope.showCapacityDeficitLow = "";
			$scope.showCapacityOverflowLow = "true";
		}
		else {
			$scope.colorCapacityLow = "beer-red";
			$scope.showCapacityOverflowLow = "";
			$scope.showCapacityDeficitLow = "true";
		}
		// Mittel
		if (parseInt($scope.capacityComparisonWaterplantMedium) >= 0) {
			$scope.colorCapacityMedium = "w3-blue";
			$scope.showCapacityDeficitMedium = "";
			$scope.showCapacityOverflowMedium = "true";
		}
		else {
			$scope.colorCapacityMedium = "beer-red";
			$scope.showCapacityOverflowMedium = "";
			$scope.showCapacityDeficitMedium = "true";
		}
		// Hoch
		if (parseInt($scope.capacityComparisonWaterplantHigh) >= 0) {
			$scope.colorCapacityHigh = "w3-blue";
			$scope.showCapacityDeficitHigh = "";
			$scope.showCapacityOverflowHigh = "true";
		}
		else {
			$scope.colorCapacityHigh = "beer-red";
			$scope.showCapacityOverflowHigh = "";
			$scope.showCapacityDeficitHigh = "true";
		}
		
		/*
		Testing Ist-Produktionskapazität	
		alert("Actual Performance: " + "Low " + $scope.actualPerformanceWaterplantLow + " " + "Medium " + $scope.actualPerformanceWaterplantMedium + " " + "High " + $scope.actualPerformanceWaterplantHigh);
		*/
	}
	
});





//////////////////////////////
// Mälzereien
//////////////////////////////


app.controller('malterController', function($scope, $http, $log) {
	
	// Statusvariablen für die Neuanlage von Unternehmen setzen
	$scope.initCreateFormMalter = function() {
		$scope.createMalterForm = false;
		$scope.showAddMalterButton = true;
	}

	
	// Statusvariablen für jede Malter in Malters.companies setzten im ng-Repeat
	$scope.initMalters = function(malter) {
		malter.malterEditStatus = false;
		malter.showMalterDataDetails = false;
		malter.companyMalterEdit = true;
	}
	
	
	// Unternehmensdaten bearbeiten und speichern durch Buttons je Datensatz
	$scope.changeMalterStatus = function(malter) {
		malter.malterEditStatus = !malter.malterEditStatus;
	}
	
	
	// Felder zum Bearbeiten freigeben
	$scope.enableCompanyMalterEdit = function(malter) {
		malter.companyMalterEdit = false;
	}
	
	
	// Felder zum Bearbeiten und Sperren
	$scope.disableCompanyMalterEdit = function(malter) {
		malter.companyMalterEdit = true;
	}
	
	
	// Form für createMalterForm beim Klick auf "+" einblenden
	$scope.openMalterForm = function() {
		$scope.createMalterForm = true;
		$scope.showAddMalterButton = false;
	}	
	
	
	// Form für createMalterForm beim Klick auf "Abbrechen" oder "Speichern" ausblenden
	$scope.closeMalterForm = function() {
		$scope.createMalterForm = false;
		$scope.showAddMalterButton = true;
	}
	
	
	// Details aufklappen / zuklappen - Anzeige der kompletten Informationen für eine Unternehmen oder nur Kurzübersicht
	$scope.showMalterDetails = function(malter) {
		malter.showMalterDataDetails = !malter.showMalterDataDetails;
		if (malter.activeButton == "activeButton") {
			malter.activeButton = "";
		}
		else if (malter.activeButton != "activeButton") {
			malter.activeButton = "activeButton";
		}

	}
	
	// GET company (malter) - Alle Daten der vorhandenen Unternehmen holen
	$http({
		method: "get",
		url: url + "api/company/malter",
	}).then(function(response) {
		$scope.malters = response.data;
		$log.info(response);	
		
		/* 
		Funktionsaufrufe
		Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
		Berechnung der benötigen Produktionskapazität aller Unternehmen je Qualitätsstufe -> direkte Nachfolger
		*/
		$scope.actualPerformanceCalculationMalter();
		$scope.getTargetCapacityBrewery();
		

		/* Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.malters));
		*/
		
	}, function (response) {
		alert("Error " + response.status + " " + response.statusText + " " + response.data);
	});	
	
	
	// Seite über Aktualisieren-Button neu laden
	$scope.getMalters = function() {
		$http({
			method: "get",
			url: url + "api/company/malter",
		}).then(function(response) {
			$scope.malters = response.data;
			$log.info(response);	
			
			/* 
			Funktionsaufrufe
			Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
			Berechnung der benötigen Produktionskapazität aller Unternehmen je Qualitätsstufe -> direkte Nachfolger
			*/
			$scope.actualPerformanceCalculationMalter();
			$scope.getTargetCapacityBrewery();

		/* 
		Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.malters));
		*/
		
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});	
		
	}
	
	
	// POST company (malter) - Unternehmen anlegen
	$scope.newMalter = {};
	$scope.postNewMalter = function() {
        $http({
            method: "post",
            url: url + "api/company",
            data: {
				// Daten für neues Unternehmen
                companyType: "malter",
				companySize: $scope.newMalter.companySize,
				qualityType: $scope.newMalter.qualityType,
				budget: $scope.newMalter.budget * 100,
				warehousePercentage: $scope.newMalter.warehousePercentage,
				profitMargin: $scope.newMalter.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getMalters();
			
				// Objekt für Eingabeformular wieder leeren
				$scope.newMalter = {};
			
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				companyType: "malter",
				companySize: $scope.newMalter.companySize,
				qualityType: $scope.newMalter.qualityType,
				budget: $scope.newMalter.budget,
				warehousePercentage: $scope.newMalter.warehousePercentage,
				profitMargin: $scope.newMalter.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	// POST randomCompany (malter) - Unternehmen automatisch generieren lassen
	$scope.postRandomMalter = function() {
        $http({
            method: "post",
            url: url + "api/randomCompany",
            data: {
				// Unternehmenstyp übergeben
                companyType: "malter",
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getMalters();
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});
	}
	
	
	// PUT company (malter) - Felder: Zielfüllgrad, Gewinnaufschlag des Unternehmens updaten
	$scope.updateMalter = function(malter) {
		$scope.malterData = malter;
        $http({
            method: "put",
            url: url + "api/company",
            data: {
				// Daten für bestehendes Unternehmen updaten
				companyId: $scope.malterData.companyId,
                companyType: "malter",
				warehousePercentage: $scope.malterData.warehousePercentage,
				profitMargin: $scope.malterData.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				companyId: $scope.malterData.companyId,
                companyType: "malter",
				warehousePercentage: $scope.malterData.warehousePercentage,
				profitMargin: $scope.malterData.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	

	// DELETE company (malter) - Unternehmen löschen
	$scope.deleteMalter = function(malter) {
		$scope.malterData = malter;
		$http({
			method: "delete",
			url: url + "api/company",
			data: {
				// Unternehmen löschen
				companyId: $scope.malterData.companyId,
                companyType: "malter",
			},
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getMalters();

		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);

			/* 
			Testing JSON Output
			var testData = {
				companyId: $scope.malterData.companyId,
                companyType: "malter",
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	/* 
	Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
	Variablen für Soll-Ist-Vergleich je Qualitätsstufe - Mengeneinheit KG / Liter

	Produktionsmengen pro Tag:
	Niedrige Qualität - 1000 KG / Liter
	Mittlere - Qualität - 500 KG / Liter
	Hohe Qualität - 250 KG Liter
		
	-> AUSNAHME WASSERWERKE:
	Niedrige Qualität - 5000 KG / Liter
	Mittlere - Qualität - 2500 KG / Liter
	Hohe Qualität - 1250 KG Liter
	
	Rückwärtsrechnung der Produktion und Mischverhältnisse im Herstellungsprozess
	
	Mischverhältnisse:
	4.0 Wasser + 1.0 Gerste = 0.8 Malz
	1.5 Wasser + 0.5 Malz + 0.2 Hopfen = 1.0 Bier

	Booster bei Produktionsgeschwindigkeit:
	Unternehmensgröße groß - Produktionsmenge * 2.0
	*/
	$scope.actualPerformanceCalculationMalter = function () {
		
		// Ist-Variablen - Aktuelle Produktionskapazität
		$scope.actualPerformanceMalterLow = 0;
		$scope.actualPerformanceMalterMedium = 0;
		$scope.actualPerformanceMalterHigh = 0;
		
		// Berechnung der Produktionskapazitäten aus ng-repeat
		var actualPerformanceLow = 0;
		var actualPerformanceMedium = 0;
		var actualPerformanceHigh = 0;
		var boosterMultiplication = 1;
		
		angular.forEach($scope.malters.companies, function(malter){
			// Update boosterMultiplication mit 2 bei Unternehmensgröße groß
			if (malter.companySize.toLowerCase() == "big") {
				boosterMultiplication = 2;
			}
				
			// Produktionskapazitäten hochzählen je Qualität	
			// Niedrig 
			if (malter.qualityType.toLowerCase() == "low") {
				actualPerformanceLow += 1000 * parseInt(boosterMultiplication);
			}
			// Mittel
			else if (malter.qualityType.toLowerCase() == "medium") {
				actualPerformanceMedium += 500 * parseInt(boosterMultiplication);
			}
			// Hoc
			else if (malter.qualityType.toLowerCase() == "high") {
				actualPerformanceHigh += 250 * parseInt(boosterMultiplication);
			}
			
			// boosterMultiplication wieder auf 1 setzen
			boosterMultiplication = 1;
		});
		
		// IST-WERTE
		$scope.actualPerformanceMalterLow = actualPerformanceLow;
		$scope.actualPerformanceMalterMedium = actualPerformanceMedium;
		$scope.actualPerformanceMalterHigh = actualPerformanceHigh;
		
		/*
		// Testing Ist-Produktionskapazität	
		alert("Actual Performance: " + "Low " + $scope.actualPerformanceMalterLow + " " + "Medium " + $scope.actualPerformanceMalterMedium + " " + "High " + $scope.actualPerformanceMalterHigh);
		*/
		
	}
	
	$scope.getTargetCapacityBrewery = function() {
			
		// Berechnung der benötigten Produktionskapazitäten - Brauereien
		// GET company (brewery) - Alle Daten der vorhandenen Unternehmen holen
		$http({
			method: "get",
			url: url + "api/company/brewery",
		}).then(function(response) {
			$scope.brewerys = response.data;
			
			// Funktionsaufruf - Berechnung der Soll-Kapazitäten
			$scope.targetCalculationBrewery();

			$log.info(response);	

			/*
			Testing JSON Input
			alert("Eingabe JSON company: " + JSON.stringify($scope.brewerys));
			*/

		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});	
		
	}
	
	$scope.targetCalculationBrewery = function() {
		
		$scope.actualPerformanceBreweryLow = 0;
		$scope.actualPerformanceBreweryMedium = 0;
		$scope.actualPerformanceBreweryHigh = 0;
		
		// Ist-Variablen - Aktuelle Produktionskapazität
		$scope.actualPerformanceBreweryLow = 0;
		$scope.actualPerformanceBreweryMedium = 0;
		$scope.actualPerformanceBreweryHigh = 0;

		// Berechnung der Produktionskapazitäten aus ng-repeat
		var actualPerformanceLow = 0;
		var actualPerformanceMedium = 0;
		var actualPerformanceHigh = 0;
		var boosterMultiplication = 1;

		angular.forEach($scope.brewerys.companies, function(brewery){
			// Update boosterMultiplication mit 2 bei Unternehmensgröße groß
			if (brewery.companySize.toLowerCase() == "big") {
				boosterMultiplication = 2;
			}

			// Produktionskapazitäten hochzählen je Qualität	
			// Niedrig 
			if (brewery.qualityType.toLowerCase() == "low") {
				actualPerformanceLow += 1000 * parseInt(boosterMultiplication);
			}
			// Mittel
			else if (brewery.qualityType.toLowerCase() == "medium") {
				actualPerformanceMedium += 500 * parseInt(boosterMultiplication);
			}
			// Hoc
			else if (brewery.qualityType.toLowerCase() == "high") {
				actualPerformanceHigh += 250 * parseInt(boosterMultiplication);
			}

			// boosterMultiplication wieder auf 1 setzen
			boosterMultiplication = 1;
		});
		
		// SOLL-WERTE - entsprechend dem geforderten Mischverhältnis
		$scope.actualPerformanceBreweryLow = parseInt(actualPerformanceLow) * 0.5;
		$scope.actualPerformanceBreweryMedium = parseInt(actualPerformanceMedium) * 0.5;
		$scope.actualPerformanceBreweryHigh = parseInt(actualPerformanceHigh) * 0.5;
		
		/*
		// Testing Soll-Produktionskapazität
		alert("Target Performance: " + "Low " + $scope.actualPerformanceBreweryLow + " " + "Medium " + $scope.actualPerformanceBreweryMedium + " " + "High " + $scope.actualPerformanceBreweryHigh);
		*/
		
		// Soll-Ist-Vergleich für Darstellung im Frontend
		$scope.showCapacityMalter();

	}
			
	$scope.showCapacityMalter = function() {

		// Bedingungen für Diagramm in User-Ansicht
		// Berechnung Soll-/ Ist-Vergleich Produktionskapazitäten
		$scope.capacityComparisonMalterLow = parseInt($scope.actualPerformanceMalterLow) - parseInt($scope.actualPerformanceBreweryLow);
		$scope.capacityComparisonMalterMedium = parseInt($scope.actualPerformanceMalterMedium) - parseInt($scope.actualPerformanceBreweryMedium);
		$scope.capacityComparisonMalterHigh = parseInt($scope.actualPerformanceMalterHigh) - parseInt($scope.actualPerformanceBreweryHigh);

		// Hintergrund rot (negative Produktionskapazität) oder blau (optimale oder positive Produktionskapazität)
		// Anzeige Balken (negative Produktionskapazität) links oder rechts (optimale oder positive Produktionskapazität)
		// Niedrig
		if (parseInt($scope.capacityComparisonMalterLow) >= 0) {
			$scope.colorCapacityLow = "w3-blue";
			$scope.showCapacityDeficitLow = "";
			$scope.showCapacityOverflowLow = "true";
		}
		else {
			$scope.colorCapacityLow = "beer-red";
			$scope.showCapacityOverflowLow = "";
			$scope.showCapacityDeficitLow = "true";
		}
		// Mittel
		if (parseInt($scope.capacityComparisonMalterMedium) >= 0) {
			$scope.colorCapacityMedium = "w3-blue";
			$scope.showCapacityDeficitMedium = "";
			$scope.showCapacityOverflowMedium = "true";
		}
		else {
			$scope.colorCapacityMedium = "beer-red";
			$scope.showCapacityOverflowMedium = "";
			$scope.showCapacityDeficitMedium = "true";
		}
		// Hoch
		if (parseInt($scope.capacityComparisonMalterHigh) >= 0) {
			$scope.colorCapacityHigh = "w3-blue";
			$scope.showCapacityDeficitHigh = "";
			$scope.showCapacityOverflowHigh = "true";
		}
		else {
			$scope.colorCapacityHigh = "beer-red";
			$scope.showCapacityOverflowHigh = "";
			$scope.showCapacityDeficitHigh = "true";
		}

	}
	
});





//////////////////////////////
// Brauereien
//////////////////////////////


app.controller('breweryController', function($scope, $http, $log) {

	// Statusvariablen für die Neuanlage von Unternehmen setzen
	$scope.initCreateFormBrewery = function() {
		$scope.createBreweryForm = false;
		$scope.showAddBreweryButton = true;
	}

	
	// Statusvariablen für jede Brewery in Brewerys.companies setzten im ng-Repeat
	$scope.initBrewerys = function(brewery) {
		brewery.breweryEditStatus = false;
		brewery.showBreweryDataDetails = false;
		brewery.companyBreweryEdit = true;
	}
	
	
	// Unternehmensdaten bearbeiten und speichern durch Buttons je Datensatz
	$scope.changeBreweryStatus = function(brewery) {
		brewery.breweryEditStatus = !brewery.breweryEditStatus;
	}
	
	
	// Felder zum Bearbeiten freigeben
	$scope.enableCompanyBreweryEdit = function(brewery) {
		brewery.companyBreweryEdit = false;
	}
	
	
	// Felder zum Bearbeiten und Sperren
	$scope.disableCompanyBreweryEdit = function(brewery) {
		brewery.companyBreweryEdit = true;
	}
	
	
	// Form für createBreweryForm beim Klick auf "+" einblenden
	$scope.openBreweryForm = function() {
		$scope.createBreweryForm = true;
		$scope.showAddBreweryButton = false;
	}	
	
	
	// Form für createBreweryForm beim Klick auf "Abbrechen" oder "Speichern" ausblenden
	$scope.closeBreweryForm = function() {
		$scope.createBreweryForm = false;
		$scope.showAddBreweryButton = true;
	}
	
	
	// Details aufklappen / zuklappen - Anzeige der kompletten Informationen für eine Unternehmen oder nur Kurzübersicht
	$scope.showBreweryDetails = function(brewery) {
		brewery.showBreweryDataDetails = !brewery.showBreweryDataDetails;
		if (brewery.activeButton == "activeButton") {
			brewery.activeButton = "";
		}
		else if (brewery.activeButton != "activeButton") {
			brewery.activeButton = "activeButton";
		}

	}
	
	// GET company (brewery) - Alle Daten der vorhandenen Unternehmen holen
	$http({
		method: "get",
		url: url + "api/company/brewery",
	}).then(function(response) {
		$scope.brewerys = response.data;
		
		$log.info(response);	
		
		// Funktionsaufruf - Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
		$scope.actualPerformanceCalculationBrewery();

		/* 
		Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.brewerys));
		*/
		
	}, function (response) {
		alert("Error " + response.status + " " + response.statusText + " " + response.data);
	});	
	
	
	// Seite über Aktualisieren-Button neu laden
	$scope.getBrewerys = function() {
		$http({
			method: "get",
			url: url + "api/company/brewery",
		}).then(function(response) {
			$scope.brewerys = response.data;
			$log.info(response);
			
			// Funktionsaufruf - Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
			$scope.actualPerformanceCalculationBrewery();

		/* 
		Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.brewerys));
		*/
		
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});	
		
	}
	
	
	// POST company (brewery) - Unternehmen anlegen
	$scope.newBrewery = {};
	$scope.postNewBrewery = function() {
        $http({
            method: "post",
            url: url + "api/company",
            data: {
				// Daten für neues Unternehmen
                companyType: "brewery",
				companySize: $scope.newBrewery.companySize,
				qualityType: $scope.newBrewery.qualityType,
				budget: $scope.newBrewery.budget * 100,
				warehousePercentage: $scope.newBrewery.warehousePercentage,
				profitMargin: $scope.newBrewery.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getBrewerys();
			
				// Objekt für Eingabeformular wieder leeren
				$scope.newBrewery = {};
			
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				companyType: "brewery",
				companySize: $scope.newBrewery.companySize,
				qualityType: $scope.newBrewery.qualityType,
				budget: $scope.newBrewery.budget,
				warehousePercentage: $scope.newBrewery.warehousePercentage,
				profitMargin: $scope.newBrewery.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	// POST randomCompany (brewery) - Unternehmen automatisch generieren lassen
	$scope.postRandomBrewery = function() {
        $http({
            method: "post",
            url: url + "api/randomCompany",
            data: {
				// Unternehmenstyp übergeben
                companyType: "brewery",
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getBrewerys();
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
		});
	}
	
	
	// PUT company (brewery) - Felder: Zielfüllgrad, Gewinnaufschlag des Unternehmens updaten
	$scope.updateBrewery = function(brewery) {
		$scope.breweryData = brewery;
        $http({
            method: "put",
            url: url + "api/company",
            data: {
				// Daten für bestehendes Unternehmen updaten
				companyId: $scope.breweryData.companyId,
                companyType: "brewery",
				warehousePercentage: $scope.breweryData.warehousePercentage,
				profitMargin: $scope.breweryData.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				companyId: $scope.breweryData.companyId,
                companyType: "brewery",
				warehousePercentage: $scope.breweryData.warehousePercentage,
				profitMargin: $scope.breweryData.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	

	// DELETE company (brewery) - Unternehmen löschen
	$scope.deleteBrewery = function(brewery) {
		$scope.breweryData = brewery;
		$http({
			method: "delete",
			url: url + "api/company",
			data: {
				// Unternehmen löschen
				companyId: $scope.breweryData.companyId,
                companyType: "brewery",
			},
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Unternehmensinformationen inkl. Berechnung der Produktionskapazitäten neu laden
				$scope.getBrewerys();

		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);

			/* 
			Testing JSON Output
			var testData = {
				companyId: $scope.breweryData.companyId,
                companyType: "brewery",
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	/* 
	Berechnung der aktuellen Produktionskapazität aller Unternehmen je Qualitätsstufe
	Variablen für Soll-Ist-Vergleich je Qualitätsstufe - Mengeneinheit KG / Liter

	Produktionsmengen pro Tag:
	Niedrige Qualität - 1000 KG / Liter
	Mittlere - Qualität - 500 KG / Liter
	Hohe Qualität - 250 KG Liter
	
	-> AUSNAHME WASSERWERKE:
	Niedrige Qualität - 5000 KG / Liter
	Mittlere - Qualität - 2500 KG / Liter
	Hohe Qualität - 1250 KG Liter
	
	Rückwärtsrechnung der Produktion und Mischverhältnisse im Herstellungsprozess
	
	Mischverhältnisse:
	4.0 Wasser + 1.0 Gerste = 0.8 Malz
	1.5 Wasser + 0.5 Malz + 0.2 Hopfen = 1.0 Bier

	Booster bei Produktionsgeschwindigkeit:
	Unternehmensgröße groß - Produktionsmenge * 2.0
	*/
	$scope.actualPerformanceBreweryLow = 0;
	$scope.actualPerformanceBreweryMedium = 0;
	$scope.actualPerformanceBreweryHigh = 0;
	$scope.actualPerformanceCalculationBrewery = function () {
		
		// Ist-Variablen - Aktuelle Produktionskapazität
		$scope.actualPerformanceBreweryLow = 0;
		$scope.actualPerformanceBreweryMedium = 0;
		$scope.actualPerformanceBreweryHigh = 0;
		
		// Berechnung der Produktionskapazitäten aus ng-repeat
		var actualPerformanceLow = 0;
		var actualPerformanceMedium = 0;
		var actualPerformanceHigh = 0;
		var boosterMultiplication = 1;
		
		angular.forEach($scope.brewerys.companies, function(brewery){
			// Update boosterMultiplication mit 2 bei Unternehmensgröße groß
			if (brewery.companySize.toLowerCase() == "big") {
				boosterMultiplication = 2;
			}
				
			// Produktionskapazitäten hochzählen je Qualität	
			// Niedrig 
			if (brewery.qualityType.toLowerCase() == "low") {
				actualPerformanceLow += 1000 * parseInt(boosterMultiplication);
			}
			// Mittel
			else if (brewery.qualityType.toLowerCase() == "medium") {
				actualPerformanceMedium += 500 * parseInt(boosterMultiplication);
			}
			// Hoc
			else if (brewery.qualityType.toLowerCase() == "high") {
				actualPerformanceHigh += 250 * parseInt(boosterMultiplication);
			}
			
			// boosterMultiplication wieder auf 1 setzen
			boosterMultiplication = 1;
		});
		
		// IST-WERTE
		$scope.actualPerformanceBreweryLow = actualPerformanceLow;
		$scope.actualPerformanceBreweryMedium = actualPerformanceMedium;
		$scope.actualPerformanceBreweryHigh = actualPerformanceHigh;
		
		// Berechnung der benötigten Produktionskapazitäten
		// Soll-Variablen - Nachfragemengen Endkunden
		$scope.targetConsumerLow = parseInt($scope.overview.consumerDemand.qualityLow);
		$scope.targetConsumerMedium = parseInt($scope.overview.consumerDemand.qualityMedium);
		$scope.targetConsumerHigh = parseInt($scope.overview.consumerDemand.qualityHigh);
		
		// SOLL-WERTE
		$scope.targetBreweryLow = parseInt($scope.targetConsumerLow);
		$scope.targetBreweryMedium = parseInt($scope.targetConsumerMedium);
		$scope.targetBreweryHigh = parseInt($scope.targetConsumerHigh);
		
		// Bedingungen für Diagramm in User-Ansicht
		// Berechnung Soll-/ Ist-Vergleich Produktionskapazitäten
		$scope.capacityComparisonBreweryLow = parseInt($scope.actualPerformanceBreweryLow) - parseInt($scope.targetBreweryLow);
		$scope.capacityComparisonBreweryMedium = parseInt($scope.actualPerformanceBreweryMedium) - parseInt($scope.targetBreweryMedium);
		$scope.capacityComparisonBreweryHigh = parseInt($scope.actualPerformanceBreweryHigh) - parseInt($scope.targetBreweryHigh);
		
		// Hintergrund rot (negative Produktionskapazität) oder blau (optimale oder positive Produktionskapazität)
		// Anzeige Balken (negative Produktionskapazität) links oder rechts (optimale oder positive Produktionskapazität)
		// Niedrig
		if (parseInt($scope.capacityComparisonBreweryLow) >= 0) {
			$scope.colorCapacityLow = "w3-blue";
			$scope.showCapacityDeficitLow = "";
			$scope.showCapacityOverflowLow = "true";
		}
		else {
			$scope.colorCapacityLow = "beer-red";
			$scope.showCapacityOverflowLow = "";
			$scope.showCapacityDeficitLow = "true";
		}
		// Mittel
		if (parseInt($scope.capacityComparisonBreweryMedium) >= 0) {
			$scope.colorCapacityMedium = "w3-blue";
			$scope.showCapacityDeficitMedium = "";
			$scope.showCapacityOverflowMedium = "true";
		}
		else {
			$scope.colorCapacityMedium = "beer-red";
			$scope.showCapacityOverflowMedium = "";
			$scope.showCapacityDeficitMedium = "true";
		}
		// Hoch
		if (parseInt($scope.capacityComparisonBreweryHigh) >= 0) {
			$scope.colorCapacityHigh = "w3-blue";
			$scope.showCapacityDeficitHigh = "";
			$scope.showCapacityOverflowHigh = "true";
		}
		else {
			$scope.colorCapacityHigh = "beer-red";
			$scope.showCapacityOverflowHigh = "";
			$scope.showCapacityDeficitHigh = "true";
		}			
		
		/*
		// Testing Ist-Produktionskapazität
		alert("Actual Performance: " + "Low " + $scope.actualPerformanceBreweryLow + " " + "Medium " + $scope.actualPerformanceBreweryMedium + " " + "High " + $scope.actualPerformanceBreweryHigh);
		*/

		/*
		// Testing Soll-Produktionskapazität
		alert("Target Performance: " + "Low " + $scope.targetBreweryLow + " " + "Medium " + $scope.targetBreweryMedium + " " + "High " + $scope.targetBreweryHigh);
		*/
		
	}
	
});





//////////////////////////////
// Endkunden
//////////////////////////////


app.controller('consumerController', function($scope, $http, $log) {
	
	// Status bearbeiten oder speichern setzten
	$scope.initDemand = function() {
		$scope.demandEditStatus = false;
	}
	
	
	// Nachfragemenge bearbeiten und speichern durch Buttons
	$scope.changeDemandConsumer = function() {
		$scope.demandEditStatus = !$scope.demandEditStatus;
	}
	
	
	// Felder zum Bearbeiten freigeben
	$scope.enableConsumerDemandEdit = function() {
		$scope.consumerDemandEdit = false;
	}
	
	
	// Felder zum Bearbeiten und Sperren
	$scope.disableConsumerDemandEdit = function() {
		$scope.consumerDemandEdit = true;
	}
	
	
	// Bereich der Nachfrageverteilung ausblenden, wenn alle 3 Nachfragemengen <= 0 sind oder ''
	$scope.checkDemandZero = function () {	
		
		if ((parseInt($scope.demandLow) <= 0 && parseInt($scope.demandMedium) <= 0 && parseInt($scope.demandHigh) <=0) || (!$scope.demandLow && !$scope.demandMedium && !$scope.demandHigh)) {		
			return true;
		}
		else {
			return false;
		}
			
	}

	
	// Prozentuale Nachfrageverteilung einblenden, wenn demand >= 0 für ng-if
	$scope.checkDemandInput = function (demandInput) {
		return parseInt(demandInput) >= 0;
	}
	
	
	// Balken für die Nachfrageverteilung einblenden wenn demand >= 1 für ng-if
	$scope.checkDemand = function(demand) {
		return parseInt(demand) >= 1;
	}
	
	
	// GET dataOverview - Aktuelle Nachfragemengen (demand) aus dataOverview holen
	$http({
		method: "get",
		url: url + "api/dataOverview",
	}).then(function(response) {
		$scope.overview = response.data;
		$log.info(response);
		
		// parseInt - Wichtig für Berechnungen im Frontend
		$scope.demandLow = parseInt(response.data.consumerDemand.qualityLow);
		$scope.demandMedium = parseInt(response.data.consumerDemand.qualityMedium);
		$scope.demandHigh = parseInt(response.data.consumerDemand.qualityHigh);

		/* 
		Testing JSON Input
		alert("Eingabe JSON dataOverview: " + JSON.stringify($scope.overview));
		*/
		
	}, function (response) {
		alert("Error " + response.status + " " + response.statusText + " " + response.data);
	});	
	
	
	// PUT demand - Aktualisierte Nachfragemengen senden
	$scope.postConsumerDemand = function() {
        $http({
            method: "put",
            url: url + "api/demand",
            data: {
				// Nachfragemengen für alle 3 Qualitätsstufen updaten in Liter
                qualityLow: $scope.demandLow,
				qualityMedium: $scope.demandMedium,
				qualityHigh: $scope.demandHigh,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
		}, function (response) {
			alert("Error " + response.status + " " + response.statusText + " " + response.data);
			
			/* 
			Testing JSON Output
			var testData = {
				quality1: $scope.demandLow,
				quality2: $scope.demandMedium,
				quality3: $scope.demandHigh,
			}
			alert("Ausgabe JSON demand: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	// Diagrammbereich für Entwicklung des tatsächlichen Bierkonsums ausblenden, wenn Laufzeit <= 4
	$scope.checkSimDuration = function() {
		
		if ($scope.simDuration <= 4) {
			return true;
		}
		else {
			return false;
		}

	}

	
	// Einstellungen für das Diagramm
	$scope.options = {
		chart: {
			type: 'multiBarChart',
			height: 450,
			margin : {
				top: 45,
				right: 0,
				bottom: 45,
				left: 105
			},		
			clipEdge: true,
			duration: 500,
			stacked: true,
			showControls: true,
			useInteractiveGuideline: true,
			color: [
				"#91220f",
				"#ffa54f",
				"#4fa135"
			],
			controlLabels: {
				"stacked":"Stapeln",
				"grouped":"Gruppieren"
			},
			xAxis: {
				axisLabel: 'Tage',
				showMaxMin: false,
				tickFormat: function(d){
					return d3.format(',f')(d);
				}
			},
			yAxis: {
				axisLabel: 'Konsumierte Menge in Liter',
				showMaxMin: false,
				min: 0,
				axisLabelDistance: 35,
				tickFormat: function(d){
					return d3.format(',.2f')(d);
				}
			},
			forceY: [0
            ]
		}
	};
	

	// Einstellungen für das Diagramm
	$scope.options2 = {
		chart: {
			type: 'lineChart',
			height: 450,
			margin : {
				top: 45,
				right: 15,
				bottom: 45,
				left: 105
			},		
			duration: 500,
			useInteractiveGuideline: true,
			color: [
				"#91220f",
				"#ffa54f",
				"#4fa135"
			],
			xAxis: {
				axisLabel: 'Tage',
				showMaxMin: false,
				tickFormat: function(d){
					return d3.format(',f')(d);
				}
			},
			yAxis: {
				axisLabel: 'Durschnittpreis pro Liter in Euro',
				showMaxMin: false,
				min: 0,
				axisLabelDistance: 35,
				tickFormat: function(d){
					return d3.format(',.2f')(d);
				}
			},
			forceY: [
				0, $scope.maxLine
            ],
		}
	}; 
	
});