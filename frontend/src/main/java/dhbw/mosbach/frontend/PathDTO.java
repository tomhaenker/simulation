package dhbw.mosbach.frontend;

public class PathDTO {
    public String path;

    // lacalhost + Port vom Core holen für alle Http-Requests vom Frontend an Core -> JSON Frontend als path
    public PathDTO(String path) {
        this.path = path;
    }
}
