package dhbw.mosbach.frontend;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {

    @Value("${simulation.core.url}")
    private String corePath;

    // Aufruf index.html als Startseite
    @RequestMapping("/index")
    public String page() {
        return "index";
    }

    // lacalhost + Port vom Core holen für alle Http-Requests vom Frontend an Core
    @GetMapping("/api/getCorePath")
    @ResponseBody
    public PathDTO getCorePath(){
        return new PathDTO(corePath);
    }

}
