package dhbw.mosbach.consumer_service.dto;

public class DailySummaryDTO {
    public int day;
    public int amount;
    public int priceSum;

    public DailySummaryDTO() {
    }

    public DailySummaryDTO(int day, int amount, int priceSum) {
        this.day = day;
        this.amount = amount;
        this.priceSum = priceSum;
    }
}
