package dhbw.mosbach.consumer_service.dto;

class keyValueDTO {
    public int x;
    public int y;

    public keyValueDTO() {
    }

    public keyValueDTO(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
