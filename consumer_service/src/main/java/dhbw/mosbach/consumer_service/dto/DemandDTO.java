package dhbw.mosbach.consumer_service.dto;

import dhbw.mosbach.consumer_service.Demand;

public class DemandDTO {
    public int qualityLow;
    public int qualityMedium;
    public int qualityHigh;

    public DemandDTO() {
    }

    public DemandDTO(int qualityLow, int qualityMedium, int qualityHigh) {
        this.qualityLow = qualityLow;
        this.qualityMedium = qualityMedium;
        this.qualityHigh = qualityHigh;
    }




}
