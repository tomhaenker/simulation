package dhbw.mosbach.consumer_service.dto;

public class DayCountDTO {
    public int dayCount;

    public DayCountDTO() {
    }

    public DayCountDTO(int dayCount) {
        this.dayCount = dayCount;
    }

    public int getDayCount() {
        return dayCount;
    }

    public void setDayCount(int dayCount) {
        this.dayCount = dayCount;
    }
}
