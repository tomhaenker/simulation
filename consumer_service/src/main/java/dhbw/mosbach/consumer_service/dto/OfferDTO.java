package dhbw.mosbach.consumer_service.dto;

import java.util.UUID;

public class OfferDTO implements Comparable<OfferDTO>{
    public UUID id;
    public int quantity;
    public double unitPrice;

    public double getUnitPrice() {
        return unitPrice;
    }

    @Override
    public int compareTo(OfferDTO o) {
        return this.id.compareTo(o.id);
    }


}
