package dhbw.mosbach.consumer_service.dto;

import java.util.ArrayList;
import java.util.List;

public class HistoryDataDTO {
    public String key;
    public List<keyValueDTO> values = new ArrayList<>();

    public void addValue(int x, int y){
        values.add(new keyValueDTO(x, y));
    }

}
