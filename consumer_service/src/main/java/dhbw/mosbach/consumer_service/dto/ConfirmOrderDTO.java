package dhbw.mosbach.consumer_service.dto;

import java.util.UUID;

public class ConfirmOrderDTO {
    public UUID companyId;
    public int quantity;
    public double unitPrice;
}
