package dhbw.mosbach.consumer_service;

import dhbw.mosbach.consumer_service.dto.DemandDTO;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Demand {
    @Id @Type(type="uuid-char")
    private UUID id = UUID.randomUUID();
    private int qualityLow;
    private int qualityMedium;
    private int qualityHigh;

    public Demand() {}

    public Demand(int qualityLow, int qualityMedium, int qualityHigh) {
        this.qualityLow = qualityLow;
        this.qualityMedium = qualityMedium;
        this.qualityHigh = qualityHigh;
    }

    public Demand(DemandDTO demand){
        this.qualityLow = demand.qualityLow;
        this.qualityMedium = demand.qualityMedium;
        this.qualityHigh = demand.qualityHigh;
    }

    public int getQuality(QualityLevel quality){
        switch (quality){
            case LOW: return getQualityLow();
            case MEDIUM: return getQualityMedium();
            case HIGH: return getQualityHigh();
        }
        throw new IllegalArgumentException("Wrong quality");
    }

    public int getQualityLow() {
        return qualityLow;
    }

    public void setQualityLow(int quality1) {
        this.qualityLow = quality1;
    }

    public int getQualityMedium() {
        return qualityMedium;
    }

    public void setQualityMedium(int quality2) {
        this.qualityMedium = quality2;
    }

    public int getQualityHigh() {
        return qualityHigh;
    }

    public void setQualityHigh(int quality3) {
        this.qualityHigh = quality3;
    }

    public UUID getId() {
        return id;
    }
}
