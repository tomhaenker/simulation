package dhbw.mosbach.consumer_service.enums;

public enum Resources {
    WATER, HOP, MALT, BEER
}
