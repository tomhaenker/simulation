package dhbw.mosbach.consumer_service.enums;

public enum TransactionType {
    BUY, SELL
}
