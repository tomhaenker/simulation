package dhbw.mosbach.consumer_service;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DemandRepository extends JpaRepository<Demand, UUID> {

}
