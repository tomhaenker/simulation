package dhbw.mosbach.consumer_service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dhbw.mosbach.consumer_service.dto.ConfirmOrderDTO;
import dhbw.mosbach.consumer_service.dto.OfferDTO;
import dhbw.mosbach.consumer_service.dto.OrderDTO;
import dhbw.mosbach.consumer_service.enums.Resources;
import dhbw.mosbach.consumer_service.enums.TransactionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.*;

@Component
public class DayThread extends Thread{

    @Value("${simulation.brewery.url}")
    private String breweryPath;
    Logger logger = LoggerFactory.getLogger(DayThread.class);

    final DemandRepository demandRepository;
    final TransactionRepository transactionRepository;
    int day;

//    es existiert nur ein DemandObject, daher kann hier immer der Wert an Stelle 0 geladen werden
    Demand demand;

    public DayThread(DemandRepository demandRepository, TransactionRepository transactionRepository) {
        this.demandRepository = demandRepository;
        this.transactionRepository = transactionRepository;
    }

    public synchronized void start(int day) {
        demand = demandRepository.findAll().get(0);
        this.day = day;

        if (demand.getQualityLow() > 0) demandBeer(QualityLevel.LOW);
        if (demand.getQualityMedium() > 0) demandBeer(QualityLevel.MEDIUM);
        if (demand.getQualityHigh() > 0) demandBeer(QualityLevel.HIGH);

//        transactionRepository.save(new Transaction(confirmedOrder, qualityLevel, day));

    }

    /**
     * kapselt sämtliche Funktionen für das Nachfragen von Bier einer Qualität
     * @param qualityLevel Qualität welche nachgefragt werden soll
     */
    private void demandBeer(QualityLevel qualityLevel){
        List<OfferDTO> offers = requestBeer(qualityLevel);
        int unsatisfiedDemand = demand.getQuality(qualityLevel);

        for (int counter = 0; counter < offers.size(); counter++){
            ConfirmOrderDTO confirmation = null;
            if (unsatisfiedDemand == 0) return; //Wenn Bedarf gedeckt ist ist der Beschaffungsprozess abgeschlossen
            if (offers.get(counter).quantity == 0) continue;
            try {
                int orderAmount = Math.min(offers.get(counter).quantity, unsatisfiedDemand);
                confirmation = orderBeer(offers.get(counter).id, orderAmount);
            } catch (Exception e){
                offers = requestBeer(qualityLevel);
                counter = 0;
            }
            if (confirmation != null){
                unsatisfiedDemand = unsatisfiedDemand - confirmation.quantity;
                transactionRepository.save(new Transaction(
                        TransactionType.BUY,
                        day,
                        confirmation.companyId.toString(),
                        "Consumer Service",
                        qualityLevel,
                        confirmation.quantity,
                        BigDecimal.valueOf(confirmation.unitPrice),
                        Resources.BEER
                ));
            }
        }
    }

    /**
     * Frage beim Brewery Service an wie viel Bier einer Qualitätsstufe verfügbar ist
     * @param qualityLevel Angefragte Qualitätsstufe
     * @return sortierte Liste aller Angebote
     */
    private List<OfferDTO> requestBeer(QualityLevel qualityLevel) {
//        Angebot an Bier einer bestimmten Qualität wird eingeholt
        ObjectMapper mapper = new ObjectMapper();
        final String uri = breweryPath + "/api/request/" + qualityLevel;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List> response = null;
        int errorCounter = 0;
        Random random = new Random();
//        Kombination aus Schleife und try-catch dient zum Abfangen vom DayNotStarted Error
//        Error tritt auf, wenn der Consumer service eine Anfrage sendet, bevor der Tag beim BreweryService begonnen hat
        do {
            if (errorCounter++ == 10) {
                logger.info("Anfrage nach 10 Versuchen abgebrochen, Keine Daten übermittelt\n");
                return new ArrayList<>(); // nach 25 Fehlversuchen wird eine leere Liste zurückgegeben
            }
            try{
                response = restTemplate.exchange(uri, HttpMethod.GET, null, List.class);
                sleep((1 + random.nextInt(5)) * 100); // Falls Fehler auftreten soll kurz pausiert werden
            } catch (ResourceAccessException e){
//                Wenn Service nicht verfogbar ist wird auch keine erneute Anfrage abgesendet
                logger.error("Server nicht erreichbar\n" + e.getMessage());
                return new ArrayList<>();
            } catch (Exception e){
                if (!e.getMessage().contains("Start day before request data")){
                    logger.error(e.getMessage());
                } else{
                    logger.info("Start day at" + uri);
                }
            }
        } while (response == null);
        List<OfferDTO> offers = mapper.convertValue(response.getBody(), new TypeReference<List<OfferDTO>>(){});
        offers.sort(Comparator.comparing(OfferDTO::getUnitPrice));
        return offers;
    }

    /**
     * Kaufe Bier bei einem Unternehmen
     * Wichtig: Mit Empfang der Kaufbestätigung gilt der Kauf als abgeschlossen und auf der Gegenseite verbucht.
     * @param id Unternehmen von welchem gekauft werden soll
     * @param neededAmount Menge die gekauft werden soll
     * @return Gibt die Kaufbestätigung zurük
     */
    private ConfirmOrderDTO orderBeer(UUID id, int neededAmount){
        OrderDTO order = new OrderDTO(id, neededAmount);
        String uri = breweryPath + "/api/order/";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<OrderDTO> entity = new HttpEntity<>(order, headers);
        ResponseEntity<ConfirmOrderDTO> response = null;
        try {
            response = restTemplate.exchange(uri, HttpMethod.POST, entity, ConfirmOrderDTO.class);
        } catch (Exception e){
            logger.error("Fehler beim Aufruf von " + uri + "\nFehlercode: " + e.getMessage());
            return null; // null signalisiert einen Fehler im Kaufprozess
        }
        return response.getBody();
    }
}
