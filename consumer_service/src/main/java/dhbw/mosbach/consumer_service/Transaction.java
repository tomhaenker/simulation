package dhbw.mosbach.consumer_service;

import dhbw.mosbach.consumer_service.enums.Resources;
import dhbw.mosbach.consumer_service.enums.TransactionType;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
public class Transaction implements Comparable<Transaction>{
    @Id
    @Type(type="uuid-char")
    private final UUID transactionID = UUID.randomUUID();
    private TransactionType transactionType;
    private int day;
    private String sellerID;
    private String buyerID;
    private QualityLevel quality;
    private int amount;
    @Column(columnDefinition="DECIMAL(19,10)")
    private BigDecimal unitPrice; //in €
    private Resources resource;

    public Transaction() {
    }

    public Transaction(TransactionType transactionType, int day, String sellerID, String buyerID, QualityLevel quality, int amount, BigDecimal unitPrice, Resources resource) {
        this.transactionType = transactionType;
        this.day = day;
        this.sellerID = sellerID;
        this.buyerID = buyerID;
        this.quality = quality;
        this.amount = amount;
        this.unitPrice = unitPrice;
        this.resource = resource;
    }

    public BigDecimal getTotalPrice(){
        return unitPrice.multiply(BigDecimal.valueOf(amount));
    }

    public UUID getTransactionID() {
        return transactionID;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public int getDay() {
        return day;
    }

    public String getSellerID() {
        return sellerID;
    }

    public String getBuyerID() {
        return buyerID;
    }

    public QualityLevel getQuality() {
        return quality;
    }

    public int getAmount() {
        return amount;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public Resources getResource() {
        return resource;
    }

    @Override
    public int compareTo(Transaction transaction) {
        return this.transactionID.compareTo(transaction.transactionID);
    }
}
