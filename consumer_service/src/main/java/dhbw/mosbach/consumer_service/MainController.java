package dhbw.mosbach.consumer_service;

import dhbw.mosbach.consumer_service.dto.DailySummaryDTO;
import dhbw.mosbach.consumer_service.dto.DayCountDTO;
import dhbw.mosbach.consumer_service.dto.DemandDTO;
import dhbw.mosbach.consumer_service.dto.HistoryDataDTO;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@RestController
public class MainController {

    private final ApplicationContext applicationContext;

    final DemandRepository demandRepository;
    final TransactionRepository transactionRepository;
    private int day = 0;

    public MainController(ApplicationContext applicationContext, DemandRepository demandRepository, TransactionRepository transactionRepository) {
        this.applicationContext = applicationContext;
        this.demandRepository = demandRepository;
        this.transactionRepository = transactionRepository;
        prepareDemand();
    }

    /**
     * Startet den nächsten Simulationsdurchlauf
     * @throws InterruptedException
     */
    @PostMapping("/api/startDay")
    public void startDay(@RequestBody DayCountDTO dayCount) throws InterruptedException {
        day = dayCount.dayCount;
        DayThread dayThread = new DayThread(demandRepository, transactionRepository);
//        ohne diese Zeile können die Beans nicht geladen werden: konkreter Fehlerfall --> properties werden nicht gelesen
        applicationContext.getAutowireCapableBeanFactory().autowireBean(dayThread);
        dayThread.start(day);
        dayThread.join();
    }

    /**
     * Überschreibt die Nachfragep der Konsumenten mit neuen Werten
     * @param newDemand Konsumentennachfrage
     */
    @PutMapping("/api/demand")
    public void setDemand(
            @RequestBody DemandDTO newDemand){
        Demand demand = demandRepository.findAll().get(0);
        demand.setQualityLow(newDemand.qualityLow);
        demand.setQualityMedium(newDemand.qualityMedium);
        demand.setQualityHigh(newDemand.qualityHigh);
        demandRepository.save(demand);
    }

    /**
     * Setzt Nachfrage auf 0
     */
    @PostMapping("/api/endSimulation")
    public void deleteData(){
        if (transactionRepository.count() > 0) transactionRepository.deleteAllInBatch();
        setDemand(new DemandDTO(0, 0, 0));
    }

    /**
     * @return Liefert die aktuelle Nachfrage der Qualitätsstufen zurück
     */
    @GetMapping("/api/demand")
    public DemandDTO getDemand(){
        Demand demand = demandRepository.findAll().get(0);
        return new DemandDTO(demand.getQualityLow(), demand.getQualityMedium(), demand.getQualityHigh());
    }

    /**
     * Erzeugt auf Basis der getätigten Transaktionen Historiendaten
     * @return
     */
    @GetMapping("/api/orderHistory")
    public List<HistoryDataDTO> getOrderHistory(){
        List<HistoryDataDTO> response = new ArrayList<>();
        for (QualityLevel quality : QualityLevel.values()) {
            HistoryDataDTO historyForQuality = new HistoryDataDTO();
            historyForQuality.key = quality.toString().toLowerCase();
            for (DailySummaryDTO dailySummary : createDailySummary(quality)) {
                historyForQuality.addValue(dailySummary.day, dailySummary.amount);
            }
            response.add(historyForQuality);
        }
        return response;
    }

    @GetMapping("/api/priceHistory")
    public List<HistoryDataDTO> getPriceHistory() {
        List<HistoryDataDTO> response = new ArrayList<>();
        for (QualityLevel quality : QualityLevel.values()) {
            HistoryDataDTO historyForQuality = new HistoryDataDTO();
            historyForQuality.key = quality.toString().toLowerCase();
            for (DailySummaryDTO dailySummary : createDailySummary(quality)) {
                if (dailySummary.amount != 0)
                historyForQuality.addValue(dailySummary.day, BigDecimal.valueOf(dailySummary.priceSum).divide(BigDecimal.valueOf(dailySummary.amount), RoundingMode.DOWN).intValue());
            }
            response.add(historyForQuality);
        }
        return response;
    }


    /**
     * Stellt sicher, dass es sich um ein Sigelton Objekt handelt.
     * Wird einmalig beim Start des Services aufgerufen.
     */
    private void prepareDemand(){
        long numberOfEntries = demandRepository.count();
        if (numberOfEntries == 1) return;
        demandRepository.deleteAll();
        demandRepository.save(new Demand(0,0,0));
    }

    /**
     * Alle Transaktionen einer Qualität werden anhand des Ausführungstages zusammengefasst.
     * ACHTUNG: Summe der Transaktionen wird in Cent zurückgegeben
     * @param quality Qualitätsstufe nach welcher gefiltert werden soll
     * @return Kummulierte Tageswerte
     */
    private List<DailySummaryDTO> createDailySummary(QualityLevel quality){
        List<Transaction> transactions = transactionRepository.findAll();
        transactions.sort(Comparator.comparing(Transaction::getDay));
        List<DailySummaryDTO> summary= new ArrayList<>();
        if (transactions.size() == 0) return summary;
        int highestDay = transactions.get(transactions.size()-1).getDay();
        //Liste muss mit 0 vorgefüllt werden da das so vom Frontend für die grafik benötigt wird
        for (int day = 1; day <= highestDay; day++) {
            summary.add(new DailySummaryDTO(day, 0,0));
        }
        tranactionloop:
        for (Transaction transaction: transactions) {
            if (!transaction.getQuality().equals(quality))continue;
            for (DailySummaryDTO dailySummary: summary) {
                if (dailySummary.day == transaction.getDay()){
                    dailySummary.amount = dailySummary.amount + transaction.getAmount();
                    dailySummary.priceSum = dailySummary.priceSum + transaction.getTotalPrice().movePointRight(2).intValue();
                    continue tranactionloop;
                }
            }
        }
        return summary;
    }
}
