package dhbw.mosbach.malter_service;

import dhbw.mosbach.malter_service.dto.MalterGetDTO;
import dhbw.mosbach.malter_service.dto.MalterPostDTO;
import dhbw.mosbach.malter_service.dto.MalterPutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
public class CrudController {

    final
    MalterRepository malterRepository;

    public CrudController(MalterRepository malterRepository) {
        this.malterRepository = malterRepository;
    }

    @PostMapping("/api/company")
    public void createMalter(@RequestBody MalterPostDTO malterPostDTO){
        malterRepository.save(new Malter(malterPostDTO));
    }

    @GetMapping("/api/company/{id}")
    public MalterGetDTO getMalter(@PathVariable UUID id){
        Optional<Malter> malter = malterRepository.findById(id);
        if (malter.isEmpty()) throw new IllegalArgumentException("Wrong CompanyID");
        return new MalterGetDTO(malter.get());
    }

    @PutMapping("/api/company/{id}")
    public MalterGetDTO updateMalter(@PathVariable UUID id, @RequestBody MalterPutDTO malterPutDTO){
        Optional<Malter> optionalMalter = malterRepository.findById(id);
        if (optionalMalter.isEmpty()) throw new IllegalArgumentException("Wrong CompanyID");
        Malter malter = optionalMalter.get();
        malter.updateTargetAmount(malterPutDTO.getWarehousePercentage());
        malter.setProfit_margin(malterPutDTO.getProfitMargin());
        malterRepository.save(malter);
        return new MalterGetDTO(malter);
    }

    @DeleteMapping("/api/company/{id}")
    public void deleteCompany(@PathVariable UUID id){
        if (malterRepository.findById(id).isEmpty()) return;
        malterRepository.deleteById(id);
    }

    @PostMapping("/api/randomCompany")
    public void createRandomMalter(){
        malterRepository.save(Malter.createRandom());
    }


}
