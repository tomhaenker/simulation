package dhbw.mosbach.malter_service;

import dhbw.mosbach.malter_service.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MainController {

    final MalterRepository malterRepository;
    final ApplicationContext applicationContext;
    public static List<OfferDTO> availableOffers = new ArrayList<>();

    public MainController(MalterRepository malterRepository, ApplicationContext applicationContext) {
        this.malterRepository = malterRepository;
        this.applicationContext = applicationContext;
    }

    /**
     * Startet einen neuen Tag
     * @throws InterruptedException
     */
    @PostMapping("/api/startDay")
    public void startDay() throws InterruptedException {
        synchronized (availableOffers) {
            availableOffers.clear(); // Angebote des Vortages werden gelöscht
        }
        List<Malter> malters = malterRepository.findAll();
        List<DayThread> companyDayThreads = new ArrayList<>();
//        Angebote werden vor Produktion erstellt.
//        So wird sichergestellt, dass keine Waren angebote bzw. verkauft werden, die am selben Tag produziert wurden
        synchronized (availableOffers) {
            for (Malter malter : malters) {
                availableOffers.add(new OfferDTO(malter));
            }
        }
        for (Malter malter : malters) {
//            für jedes Unternehmen wird ein separater Thread zur Simulation geöffnet
            DayThread dayThread = new DayThread(malterRepository);
            applicationContext.getAutowireCapableBeanFactory().autowireBean(dayThread);
            dayThread.start(malter);
            companyDayThreads.add(dayThread);
        }
        for (DayThread daythread : companyDayThreads) {
//            es wird nacheinander geprüft ob jedes Unternehmen fertig berechnet wurde
            daythread.join();
        }
//      Wenn alle Unternehmen simuliert wurden gilt die Methode als abgeschlossen
//        Erst jetzt wird durch den HTTP Status Code 200 signalisiert, dass der Tag fertig simuliert wurde
    }

    /**
     * Liefert alle Angebote einer Qualität zurück
     * @param qualityLevel Angefragte Qualität
     * @return Liste aller Unternehmen die Malz dieser Qualität liefern können
     */
    @GetMapping("/api/request/{qualityLevel}")
    public List<OfferDTO> receiveRequest(
            @PathVariable QualityLevel qualityLevel) {
//        QualityLevel qualityLevel = QualityLevel.getQualityLevel(qualityLevelInt);
        List<OfferDTO> matchingOffers = new ArrayList<>();
        int errorCounter = 0;
        long malterAmount = malterRepository.count();
        while (availableOffers.size() == 0 && malterAmount > 0){ //Wenn keine Mälzereien angelegt sind, kann es auch keine Angebote geben
            if (errorCounter++ == 100) {
                System.out.println("Anfrage an Mälzerei konnte nicht beantwortet werden");
                return matchingOffers; //nach hundert Fehlversuchen wird eine leere Liste zurückgegeben (100 Fehlschläge => 10 Sekunden)
            }
            try {
                Thread.sleep(100);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        synchronized (availableOffers){
            for (OfferDTO offerDTO : availableOffers) {
                if (offerDTO.getQualityLevel(malterRepository).equals(qualityLevel)) matchingOffers.add(offerDTO);
            }
        }
        return matchingOffers;
    }


    /**
     * Führt eine Bestellung aus
     * @param order Bestelldetails (VerkäuferID und Menge)
     * @return Bestellbestätigung
     */
    @PostMapping("/api/order")
    public ConfirmOrderDTO receiveOrder(
            @RequestBody OrderDTO order) {
        if (malterRepository.findById(order.companyId).isEmpty())
            throw new IllegalArgumentException("Company does not exist");
        Malter malter = malterRepository.findById(order.companyId).get();
        synchronized (availableOffers) {
//        Der Bestand vom Malz muss in den Offers geprüft werden. Stellt sicher, dass der Bestand zu Beginn des Tages geprüft wird
            for (OfferDTO offer : availableOffers) {
                if (offer.id.equals(order.companyId)) {
                    if (offer.quantity < order.quantity)
                        throw new IllegalArgumentException("Not enough goods available");
                    else {
                        offer.quantity = offer.quantity - order.quantity; // Update der heutigen Daten
                        malter.sellProduct(order.quantity, BigDecimal.valueOf(offer.unitPrice)); // Update der Gesamtdaten
                        malterRepository.save(malter);
                        return new ConfirmOrderDTO(malter.getId(), order.quantity, offer.unitPrice);
                    }
                }
            }
        }
        throw new IllegalArgumentException("Malter - Receive Order - Something went wrong");
    }

    /**
     * Liefert detaillierte Daten der vorhandenen Unternehmen zurück
     * @return Brauereidaten
     */
    @GetMapping("/api/company")
    public DataDTO getAllCompanyData(){
        List<Malter> malters =  malterRepository.findAll();
        List<MalterGetDTO> dtoList = new ArrayList<>();
        for (Malter malter: malters) {
            dtoList.add(new MalterGetDTO(malter));
        }
        return new DataDTO(dtoList);
    }

    /**
     * Liefert zusammengefasste Daten der vorhandenen Unternehmen zurück
     * @return Brauereidaten
     */
    @GetMapping("/api/dataOverview")
    public OverviewDTO getAllCompanyOverview(){
        List<Malter> malters =  malterRepository.findAll();
        OverviewQualityDetailDTO qualityLow = new OverviewQualityDetailDTO();
        OverviewQualityDetailDTO qualityMiddle = new OverviewQualityDetailDTO();
        OverviewQualityDetailDTO qualityHigh = new OverviewQualityDetailDTO();
        for (Malter malter: malters) {
            switch (malter.getQualityLevel()){
                case LOW: qualityLow.addCompany(malter); break;
                case MEDIUM: qualityMiddle.addCompany(malter); break;
                case HIGH: qualityHigh.addCompany(malter); break;
            }
        }

        return new OverviewDTO(qualityLow, qualityMiddle, qualityHigh);
    }

    /**
     * Erstellt Unternehmen mit zufälligen Parametern
     * Schnittstelle ist für das Testen gedacht
     * @param number Anzahl der Unternehmen die erstellt werden soll
     */
    @PostMapping("/api/createDummies/{number}")
    public void createDummies(
            @PathVariable int number){
        int counter = 0;
        while (counter < number){
            Malter malter = Malter.createRandom();
//            Für die Tests sollen Unternehmen mit vollen Lagern erzeugt werden
            malter.increaseCereal(malter.getMax_amount_cereal(), new BigDecimal(100));
            malter.increaseWater(malter.getMax_amount_water(), new BigDecimal(100));
            malter.setAmount_product(malter.getMax_amount_product());
            malterRepository.save(malter);
            counter++;
        }
    }

    /**
     * Wird aufgerufen um alle Daten zu löschen und bevor eine neue Simulation gestartet wird
     */
    @PostMapping("/api/endSimulation")
    public void deleteData(){
        if (malterRepository.count() > 0) malterRepository.deleteAllInBatch();
        Malter.resetCounter();
    }
}
