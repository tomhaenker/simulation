package dhbw.mosbach.malter_service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dhbw.mosbach.malter_service.dto.ConfirmOrderDTO;
import dhbw.mosbach.malter_service.dto.OfferDTO;
import dhbw.mosbach.malter_service.dto.OrderDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.*;

@Component
public class DayThread extends Thread{


    @Value("${simulation.waterplant.url}") private String waterplantPath;
    @Value("${simulation.cerealfarmer.url}") private String cerealfarmerPath;
    
    private final MalterRepository malterRepository;

    public DayThread(MalterRepository malterRepository) {
        this.malterRepository = malterRepository;
    }

    public synchronized void start(Malter malter) {
        /*
        Täglich anfallende Kosten verbuchen
         */
        malter.bookDailyCosts();
        malterRepository.save(malter);

        /*
        Verkauf:
        Beim Start des Tages wird im Controller ein zwischenstand gezogen mit dem die Nachfrage bedient wird.
        Änderungen an Bestand und Preisen können daher nun direkt am Objekt vorgenommen werden
         */

        /*
        Produktion
        Sämtlich Logik ist im Unternehmen vorhanden
         */
        malter.produceProduct();
        malterRepository.save(malter);


        /*
        Nachkauf
        Zielmenge beim Einkauf ist immer ein volles Lager
         */

        
        orderWater(malter);
        orderCereal(malter);

        /*
        prüfen ob Unternehmen noch liquide ist
        Wenn nicht --> Aus dem Markt entfernen
         */
        if (malter.getBudget().compareTo(new BigDecimal(0)) < 0){
            malterRepository.delete(malter);
        }

    }


   /**
     * Kauft neues Wasser für übergebene Mälzerei ein
     * @param malter Mälzerei für welche der Einkauf durchgeführt werden soll
     */
    private void orderWater(Malter malter){
        int neededWater = malter.getMax_amount_water() - malter.getAmount_water();
        if (neededWater == 0) return; //Wenn Bedarf gedeckt ist ist der Beschaffungsprozess abgeschlossen

        boolean error;
        do {
            error = false;
            List<OfferDTO> offers = requestResources(waterplantPath, malter.getQualityLevel());
            for (int counter = 0; counter < offers.size(); counter++) {
                try {
                    if (offers.get(counter).quantity == 0) continue;
                    ConfirmOrderDTO confirmation = orderResources(neededWater, waterplantPath, offers.get(counter));
                    if (confirmation.equals(null)) continue;
                    malter.increaseWater(confirmation.quantity, BigDecimal.valueOf(confirmation.unitPrice));
                    malter.decreaseBudgetAfterBuying(confirmation.quantity, BigDecimal.valueOf(confirmation.unitPrice));

                    try {
                        malterRepository.save(malter);
                    } catch (IllegalArgumentException iae) {
                        /* SHOULD NEVER HAPPEN */
                        iae.printStackTrace();
                        return;
                    }

                    neededWater -= confirmation.quantity;
                    if (neededWater == 0) return; //Wenn Bedarf gedeckt ist ist der Beschaffungsprozess abgeschlossen
                } catch (Exception e) {
                    // e.printStackTrace();
                    System.out.println(" Fehler in Mälzerei - OrderWater ");
                    error = true; //set error true to rerun
                    break; // for
                }
            }
        }
        while(error);
    }


        private void orderCereal(Malter malter) {
            int neededCereal = malter.getMax_amount_cereal() - malter.getAmount_cereal();
            if (neededCereal == 0) return; //Wenn Bedarf gedeckt ist ist der Beschaffungsprozess abgeschlossen

            boolean error;
            do {
                error = false;
                List<OfferDTO> offers = requestResources(cerealfarmerPath, malter.getQualityLevel());
                for (int counter = 0; counter < offers.size(); counter++) {
                    try {
                        if (offers.get(counter).quantity == 0) continue;
                        ConfirmOrderDTO confirmation = orderResources(neededCereal, cerealfarmerPath, offers.get(counter));
                        if (confirmation.equals(null)) continue;

                        malter.increaseCereal(confirmation.quantity, BigDecimal.valueOf(confirmation.unitPrice));
                        malter.decreaseBudgetAfterBuying(confirmation.quantity, BigDecimal.valueOf(confirmation.unitPrice));

                        try {
                            malterRepository.save(malter);
                        } catch (IllegalArgumentException iae) {
                            /* SHOULD NEVER HAPPEN */
                            iae.printStackTrace();
                            return;
                        }

                        neededCereal -= confirmation.quantity;
                        if (neededCereal == 0) return; //Wenn Bedarf gedeckt ist ist der Beschaffungsprozess abgeschlossen
                    } catch (Exception e) {
                        // e.printStackTrace();
                        System.out.println("Fehler in Mälzerei -OrderCereal");
                        error = true; //set error true to rerun
                        break; // for
                    }
                }
            }
            while(error);
        }


    /**
     * Anfrage bei Lieferanten nach Rohstoffen einer bestimmten Qualität
     * @return sortierte Liste aller Angebote
     */
    private List<OfferDTO> requestResources(String uri, QualityLevel qualityLevel){
        RestTemplate restTemplate = new RestTemplate();

        int tries = 3;
        do {
            try {
                ResponseEntity<List> response = restTemplate.exchange(
                        uri + "/api/request/" + qualityLevel,
                        HttpMethod.GET,
                        null,
                        List.class);

                ObjectMapper mapper = new ObjectMapper();
                return mapper.convertValue(response.getBody(), new TypeReference<List<OfferDTO>>(){});
            } catch (Exception e) {
                Random random = new Random();
                try {
                    sleep((1 + random.nextInt(5)) * 100); // Falls Fehler auftreten soll kurz pausiert werden
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
                e.printStackTrace();
            }
        } while(--tries > 0);
        return new ArrayList<>();
    }

    /**
     * Kauft für die Produktion benötigte Rohstoffe und verbucht direkt die Budgetänderung
     * @param neededAmount Menge an nachgefragten Rohstoffen
     * @param targetService Zielmicroservice (definiert welche Waren gekauft werden)
     * @param offer Angebot des Zielmicroservices
     * @return Menge der eingekauften Waren
     */
    private ConfirmOrderDTO orderResources(int neededAmount, String targetService, OfferDTO offer)throws Exception{
        return sendHttpOrder(targetService, Math.min(offer.quantity, neededAmount), offer.id);
    }



    /**
     * Sendet einen Einkaufsbefehl an einen anderen Microservice
     * @param uri Adresse des Microservice
     * @param neededAmount Benötigte Menge
     * @param id UUID des verkaufenden Unternehmens
     * @return Bestätigung des Einkaufs
     */
    private ConfirmOrderDTO sendHttpOrder(String uri, int neededAmount, UUID id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        OrderDTO order = new OrderDTO(id, neededAmount);
        HttpEntity<OrderDTO> entity = new HttpEntity<>(order, headers);
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<ConfirmOrderDTO> response = restTemplate.exchange(
                    uri + "/api/order/",
                    HttpMethod.POST,
                    entity,
                    ConfirmOrderDTO.class);
            return response.getBody();
        } catch (HttpClientErrorException httpClientErrorException) {
            switch (httpClientErrorException.getStatusCode()) {
                case BAD_REQUEST:
                case INTERNAL_SERVER_ERROR:
                    throw httpClientErrorException;
                case GONE:
                default:  return null; // null signalisiert einen Fehler im Kaufprozess
            }
        }
    }
}





