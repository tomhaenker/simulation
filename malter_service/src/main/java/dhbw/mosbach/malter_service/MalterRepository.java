package dhbw.mosbach.malter_service;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface MalterRepository extends JpaRepository<Malter, UUID> {

    public List<Malter> findAllByQualityLevel(QualityLevel qualityLevel);
}
