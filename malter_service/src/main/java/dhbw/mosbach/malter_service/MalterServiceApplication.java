package dhbw.mosbach.malter_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MalterServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MalterServiceApplication.class, args);
    }

}
