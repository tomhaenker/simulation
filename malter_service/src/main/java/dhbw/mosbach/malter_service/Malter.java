package dhbw.mosbach.malter_service;

import dhbw.mosbach.malter_service.dto.MalterPostDTO;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
public class Malter {
    public static final int default_productProduceRate = 1_000;
    public static final int default_waterConsumeRate = 5_000;
    public static final int default_cerealConsumeRate = 1_250;
    private static final AtomicInteger count = new AtomicInteger(0);
    @Id @Type(type="uuid-char")
    private UUID id = UUID.randomUUID();
    private int numericalId;
    private BigDecimal budget; //in €
    private BigDecimal startBudget; //in €
    private int amount_water; //in l
    private int amount_cereal; //in kg
    private int amount_product; //in kg
    private int max_amount_water;
    private int max_amount_cereal;
    private int max_amount_product;
    private int target_amount_product;
    private BigDecimal breaking_factor; // Bremserate in Abhängigkeit zur Qualität
    private BigDecimal cost_factor; // Teuerungsrate bei der Herstellung
    private BigDecimal profit_margin;
    private BigDecimal dailyFixCosts;
    private CompanySize size;
    private QualityLevel qualityLevel;

    @Column(columnDefinition="DECIMAL(19,10)")
    private BigDecimal value_water_unit = new BigDecimal(0);
    @Column(columnDefinition="DECIMAL(19,10)")
    private BigDecimal value_cereal_unit = new BigDecimal(0);
    @Column(columnDefinition="DECIMAL(19,10)")
    private BigDecimal value_product_unit = new BigDecimal(0); // beim Verkauf muss hier noch die Gewinnspanne draufgerechnet werden

    /**
     * @return Gibt ein Unternehmen mit zufälligen Parameter zurück
     */
    public static Malter createRandom(){
        Random random = new Random();
        CompanySize size = CompanySize.getComanySize(random.nextInt(3) + 1);
        QualityLevel quality = QualityLevel.getQualityLevel(random.nextInt(3) + 1);
        BigDecimal budget = BigDecimal.valueOf(random.nextInt(40_000)+10_000);
        BigDecimal profit_margin = BigDecimal.valueOf(random.nextInt(40)+ 10).movePointLeft(2);
        BigDecimal targetPercentage = BigDecimal.valueOf(random.nextInt(50)+ 50).movePointLeft(2);
        return new Malter(budget, size, quality, targetPercentage, profit_margin);
    }

    /**
     * Erzeugt eine neue Mälzerei
     * @param budget in €
     * @param size Unternehmensgröße
     * @param qualityLevel Qualität
     * @param targetPercentage Zielfüllmenge des Lagers
     * @param profit_margin Angestrebte Gewinnspanne
     */
    public Malter(BigDecimal budget,
                  CompanySize size,
                  QualityLevel qualityLevel,
                  BigDecimal targetPercentage,
                  BigDecimal profit_margin) {
        this.budget = budget;
        this.startBudget = budget;
        int max_storage_size = Params.getMaxiumumStorage(size);
//        Bei der Erzeugng eines Unternehmens ist jedes Lager erstmal leer
        value_water_unit = new BigDecimal(0);
        //Input Lager würden für 5 Tage Produktion reichen

        //this.max_amount_water = max_storage_size;
        //this.max_amount_cereal = max_storage_size;


        this.max_amount_product = max_storage_size;
        target_amount_product = targetPercentage.multiply(new BigDecimal(max_amount_product)).intValue();
        this.size = size;
        this.qualityLevel = qualityLevel;
        breaking_factor = Params.getBreakingFactor(qualityLevel);
        // große Unternehmen können doppelt so schnell produzieren
        if (size == CompanySize.BIG) breaking_factor = breaking_factor.multiply(Params.boost);
        this.max_amount_water = new BigDecimal(5 * default_waterConsumeRate).multiply(breaking_factor).intValue();
        this.max_amount_cereal = new BigDecimal(5 * default_cerealConsumeRate).multiply(breaking_factor).intValue();
        cost_factor = Params.getCostFactor(qualityLevel);
        this.profit_margin = profit_margin;
        this.dailyFixCosts = Params.getDailyCost(qualityLevel);
        this.numericalId = count.incrementAndGet();
    }

    /**
     * Erzeugt eine Mälzerei aus dem übergebenen DTO
     * @param malterPostDTO Von anderem Microservice übergebenes MalterDTO
     */
    public Malter(MalterPostDTO malterPostDTO){
        this(
                new BigDecimal(malterPostDTO.budget).movePointLeft(2),
                CompanySize.getCompanySize(malterPostDTO.companySize),
                QualityLevel.getQualityLevel(malterPostDTO.qualityType),
                new BigDecimal(malterPostDTO.warehousePercentage).movePointLeft(2),
                new BigDecimal(malterPostDTO.profitMargin).movePointLeft(2)
        );
    }

    /**
     * Zurücksetzen des Unternehmenszählers
     */
    public static void resetCounter()
    {
        count.set(0);
    }

    /**
     * Erhöht den Getreidebestand und berechnet den Stückpreis neu
     * @param amount Menge in kg
     * @param price Stückpreis pro kg
     */
    public void increaseCereal(int amount, BigDecimal price){
        BigDecimal[] recalculatedValues = recalcValues(amount_cereal, amount, value_cereal_unit, price);
        amount_cereal = recalculatedValues[0].toBigInteger().intValue();
        value_cereal_unit = recalculatedValues[1];
    }

    /**
     * Erhöht den Wasserbestand und berechnet den Literpreis neu
     * @param amount Menge in Litern
     * @param price Literpreis
     */
    public void increaseWater(int amount, BigDecimal price){
        BigDecimal[] recalculatedValues = recalcValues(amount_water, amount, value_water_unit, price);
        amount_water = recalculatedValues[0].toBigInteger().intValue();
        value_water_unit = recalculatedValues[1];
    }


    /**
     * Methode dient zum Starten eines Produktionsdurchgangs.
     * Einmal täglich aufrufen.
     * Sämltiche relevante Prüfungen sind hierin enthalten.
     */
    public void produceProduct(){
//        Bestandsänderungen werden berechnet
        BigDecimal consumed_water = breaking_factor.multiply(new BigDecimal(default_waterConsumeRate));
        BigDecimal consumed_cereal = breaking_factor.multiply(new BigDecimal(default_cerealConsumeRate));
        BigDecimal produced_product = breaking_factor.multiply(new BigDecimal(default_productProduceRate));

//        Bestände werden geprüft
        if (max_amount_product < (amount_product + produced_product.intValue()))return; //Es kann nicht mehr produziert werden als ins Lager passt
        if (target_amount_product < amount_product) return; //Es wird nicht mehr wenn die Zielmenge erreicht wurde
        if (amount_water < consumed_water.intValue()) return;
        if (amount_cereal < consumed_cereal.intValue()) return;

//        Berechnung von Zwischensummen zur Preisermittlung
//        Preis ändert sich somit bei jeder Produktion
        BigDecimal value_consumedGoods =
                consumed_water.multiply(value_water_unit).add(
                        consumed_cereal.multiply(value_cereal_unit));
        BigDecimal cost_production = value_consumedGoods.multiply(cost_factor);
        if(!enoughBudgetAvailable(cost_production)) return;
        BigDecimal[] recalculatedValues = recalcValues(amount_product, produced_product.intValue(), value_product_unit, cost_production.add(value_consumedGoods).divide(produced_product, Params.roundingScale, RoundingMode.DOWN));

//        Lagerbestände werden angepasst
        amount_water = amount_water - consumed_water.intValue();
        amount_cereal = amount_cereal - consumed_cereal.intValue();
        amount_product = recalculatedValues[0].intValue();
        value_product_unit = recalculatedValues[1];
        budget = budget.subtract(cost_production);
    }

    /**
     * Preisberechung nach Bestandszuwachs
     * @param oldAmount Menge vor Bestandsänderung
     * @param increaseAmount Menge des Bestandzuwachses
     * @param oldPrice bisheriger Preis pro Einheit
     * @param buyPrice Stückpreis des Zuwachses
     * @return [neue Gesamtmenge, neuer Stückpreis]
     */
    private BigDecimal[] recalcValues(int oldAmount, int increaseAmount, BigDecimal oldPrice, BigDecimal buyPrice){
        int total_amount = oldAmount + increaseAmount;
        BigDecimal oldValue = new BigDecimal(oldAmount).multiply(oldPrice);
        BigDecimal increasingValue = new BigDecimal(increaseAmount).multiply(buyPrice);
        BigDecimal total_value = oldValue.add(increasingValue);
        BigDecimal unit_value =  total_value.divide(new BigDecimal(total_amount), Params.roundingScale, RoundingMode.DOWN);  // Durchschnittlicher Wert einer Einheit im Lager
        BigDecimal[] returnValue = new BigDecimal[2];
        returnValue[0] = new BigDecimal(total_amount);
        returnValue[1] = unit_value;
        return returnValue;
    }

    /**
     * Verkaufspreisberechnung
     * Ist notwendig, da die Gewinnspanne beim Warenwert des Produktes berücksichtig wird
     * @return Verkaufspreis pro kg Malz
     */
    public BigDecimal calcSellPrice(){
        return value_product_unit.multiply(profit_margin.add(new BigDecimal(1)));
    }

    /**
     * Verbucht die Änderungen die durch einen Verkauf entstehen
     * @param sell_amount_product Menge des Malzes die verkauft wird
     * @param price_singleUnit Verkaufspreis von einem kg Malz (inkl. Profitmarge)
     */
    public void sellProduct(int sell_amount_product, BigDecimal price_singleUnit){
        if (sell_amount_product > amount_product) throw new IllegalArgumentException("Order Amount too high");
        amount_product = amount_product - sell_amount_product;
        budget = budget.add(price_singleUnit.multiply(new BigDecimal(sell_amount_product)));
    }

    /**
     * Temporäre Methode
     * @param amount
     * @param price
     */
    public void decreaseBudgetAfterBuying(int amount, BigDecimal price){
        budget = budget.subtract(price.multiply(BigDecimal.valueOf(amount)));
    }


    /**
     * Setzt die Zielfüllmenge des Lagers anhand eines prozentualen Inputs fest
     * @param targetPercentage Prozentuale Zielfüllmenge des Lagers
     */
    public void updateTargetAmount(BigDecimal targetPercentage){
        if (targetPercentage.intValue() < 0) throw new IllegalArgumentException("Negative percentage");
        if (targetPercentage.intValue() > 100) throw new IllegalArgumentException("More then 100 percent");
        target_amount_product = targetPercentage.multiply(new BigDecimal(max_amount_product)).intValue();
    }

    public boolean enoughBudgetAvailable(BigDecimal cost){
        return cost.compareTo(this.budget) <= 0;
    }

    /**
     * Zieht die täglichen Kosten (Fix- plus Lagerkosten) vom Budget ab
     */
    public void bookDailyCosts(){
        BigDecimal totalCosts = dailyFixCosts.add(Params.storageCostPerKg.multiply(new BigDecimal(amount_product)));
        budget = budget.subtract(totalCosts);
//        es findet keine Prüfung statt. Es ist bewusst möglich hierbei ins Minus zu rutschen
    }









//    Standardmethoden

    /*
    Kennzeichnung als Deprecated hat den Zweck, dass die Methode nicht unwissentlich verwendet wird
    Wird lediglich für die Field based access von Hibernate benötigt
     */

    public Malter() {
    }

    public UUID getId() {
        return id;
    }

    @Deprecated
    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    @Deprecated
    public void setBudget(BigDecimal budget) {
        this.budget = budget;
    }

    public int getAmount_water() {
        return amount_water;
    }

    @Deprecated
    public void setAmount_water(int amount_water) {
        this.amount_water = amount_water;
    }

    public int getAmount_cereal() {
        return amount_cereal;
    }

    @Deprecated
    public void setAmount_cereal(int amount_cereal) {
        this.amount_cereal = amount_cereal;
    }


    public int getAmount_product() {
        return amount_product;
    }

    @Deprecated
    public void setAmount_product(int amount_product) {
        this.amount_product = amount_product;
    }

    public BigDecimal getValue_water_unit() {
        return value_water_unit;
    }

    @Deprecated
    public void setValue_water_unit(BigDecimal value_water_unit) {
        this.value_water_unit = value_water_unit;
    }

    public BigDecimal getValue_cereal_unit() {
        return value_cereal_unit;
    }

    @Deprecated
    public void setValue_cereal_unit(BigDecimal value_cereal_unit) {
        this.value_cereal_unit = value_cereal_unit;
    }


    @Deprecated
    public BigDecimal getValue_product_unit() {
        return value_product_unit;
    }

    @Deprecated
    public void setValue_product_unit(BigDecimal value_product_unit) {
        this.value_product_unit = value_product_unit;
    }

    public int getMax_amount_water() {
        return max_amount_water;
    }

    @Deprecated
    public void setMax_amount_water(int max_amount_water) {
        this.max_amount_water = max_amount_water;
    }

    public int getMax_amount_cereal() {
        return max_amount_cereal;
    }

    @Deprecated
    public void setMax_amount_cereal(int max_amount_cereal) {
        this.max_amount_cereal = max_amount_cereal;
    }


    public int getMax_amount_product() {
        return max_amount_product;
    }

    @Deprecated
    public void setMax_amount_product(int max_amount_product) {
        this.max_amount_product = max_amount_product;
    }

    public int getTarget_amount_product() {
        return target_amount_product;
    }

    @Deprecated
    public void setTarget_amount_product(int target_amount_product) {
        this.target_amount_product = target_amount_product;
    }

    public BigDecimal getBreaking_factor() {
        return breaking_factor;
    }

    @Deprecated
    public void setBreaking_factor(BigDecimal breaking_factor) {
        this.breaking_factor = breaking_factor;
    }

    public BigDecimal getCost_factor() {
        return cost_factor;
    }

    @Deprecated
    public void setCost_factor(BigDecimal cost_factor) {
        this.cost_factor = cost_factor;
    }

    public BigDecimal getProfit_margin() {
        return profit_margin;
    }

    public void setProfit_margin(BigDecimal profit_margin) {
        this.profit_margin = profit_margin;
    }

    public CompanySize getSize() {
        return size;
    }

    @Deprecated
    public void setSize(CompanySize size) {
        this.size = size;
    }

    public QualityLevel getQualityLevel() {
        return qualityLevel;
    }

    @Deprecated
    public void setQualityLevel(QualityLevel qualityLevel) {
        this.qualityLevel = qualityLevel;
    }

    public BigDecimal getStartBudget() {
        return startBudget;
    }

    @Deprecated
    public void setStartBudget(BigDecimal startBudget) {
        this.startBudget = startBudget;
    }

    public int getNumericalId() {
        return numericalId;
    }

    @Deprecated
    public void setNumericalId(int numericalId) {
        this.numericalId = numericalId;
    }

    public BigDecimal getDailyFixCosts() {
        return dailyFixCosts;
    }

    public void setDailyFixCosts(BigDecimal dailyFixCosts) {
        this.dailyFixCosts = dailyFixCosts;
    }
}
