package dhbw.mosbach.malter_service.dto;

import java.math.BigDecimal;

public class MalterPutDTO {
    private int warehousePercentage;
    private int profitMargin;

    public BigDecimal getWarehousePercentage() {
        return new BigDecimal(warehousePercentage).movePointLeft(2);
    }

    public BigDecimal getProfitMargin() {
        return new BigDecimal(profitMargin).movePointLeft(2);
    }
}
