package dhbw.mosbach.malter_service.dto;

import dhbw.mosbach.malter_service.Malter;

public class OverviewQualityDetailDTO {
    private int count;
    private int balance;
    private int amount;

    public void addCompany(Malter malter){
        count++;
        balance = balance + malter.getBudget().subtract(malter.getStartBudget()).movePointRight(2).intValue();
        amount = amount + malter.getAmount_product();
    }

    public int getCount() {
        return count;
    }

    public int getBalance() {
        return balance;
    }

    public int getAmount() {
        return amount;
    }
}
