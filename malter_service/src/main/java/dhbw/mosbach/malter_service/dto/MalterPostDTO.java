package dhbw.mosbach.malter_service.dto;

public class MalterPostDTO {
    public String companySize;
    public String qualityType;
    public int budget;
    public int warehousePercentage;
    public int profitMargin;
}
