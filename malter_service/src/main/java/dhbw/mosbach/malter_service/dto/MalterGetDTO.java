package dhbw.mosbach.malter_service.dto;

import dhbw.mosbach.malter_service.Malter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

public class MalterGetDTO {
    public UUID companyId;
    public int companyIdNumeric;
    public String companySize;
    public String qualityType;
    public int budget;
    public int balance; // steht für die Differenz zwischen Startbudget und aktuellem
    public int currentStock;
    public int warehousePercentage;
    public int profitMargin;

    public MalterGetDTO(Malter malter) {
        this.companyId = malter.getId();
        this.qualityType = malter.getQualityLevel().toString();
        this.budget = malter.getBudget().movePointRight(2).intValue();
        this.balance = malter.getBudget().subtract(malter.getStartBudget()).movePointRight(2).intValue();
        this.warehousePercentage = new BigDecimal(malter.getTarget_amount_product()).divide(new BigDecimal(malter.getMax_amount_product()), 10, RoundingMode.DOWN).movePointRight(2).intValue(); //Wert ist immer ein Ganzzahl
        this.profitMargin = malter.getProfit_margin().movePointRight(2).intValue();
        this.currentStock = malter.getAmount_product();
        this.companySize = malter.getSize().toString();
        this.companyIdNumeric = malter.getNumericalId();
    }
}
