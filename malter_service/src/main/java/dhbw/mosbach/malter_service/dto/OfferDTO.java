package dhbw.mosbach.malter_service.dto;

import dhbw.mosbach.malter_service.Malter;
import dhbw.mosbach.malter_service.MalterRepository;
import dhbw.mosbach.malter_service.QualityLevel;

import java.util.UUID;

public class OfferDTO implements Comparable<OfferDTO>{
    public UUID id;
    public int quantity;
    public double unitPrice;

    public OfferDTO() { }

    public OfferDTO(Malter malter) {
        this(malter.getId(), malter.getAmount_product(), malter.calcSellPrice().doubleValue());
    }

    public OfferDTO(UUID id, int quantity, double unitPrice) {
        this.id = id;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    public QualityLevel getQualityLevel(MalterRepository repository){
        if (repository.findById(id).isEmpty()) throw new IllegalArgumentException("Wrong Id");
        return repository.findById(id).get().getQualityLevel();
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    @Override
    public int compareTo(OfferDTO o) {
        return this.id.compareTo(o.id);
    }
}