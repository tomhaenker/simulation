package dhbw.mosbach.malter_service.dto;

import java.util.ArrayList;
import java.util.List;

public class DataDTO {
    public List<MalterGetDTO> companies = new ArrayList<>();

    public DataDTO(List<MalterGetDTO> companies) {
        this.companies = companies;
    }
}
