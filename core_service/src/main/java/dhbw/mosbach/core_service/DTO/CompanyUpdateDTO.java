package dhbw.mosbach.core_service.DTO;

public class CompanyUpdateDTO {
    public String companyId, companyType;
    public int warehousePercentage, profitMargin;
}
