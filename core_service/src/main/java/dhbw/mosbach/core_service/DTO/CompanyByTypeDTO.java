package dhbw.mosbach.core_service.DTO;

import java.util.List;

public class CompanyByTypeDTO {
    public String companyId, companySize, qualityType;
    public int budget, balance, currentStock, warehousePercentage, profitMargin, companyIdNumeric;
}
