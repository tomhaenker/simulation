package dhbw.mosbach.core_service.DTO;

import java.util.List;

public class OrderHistoryDTO {
    public List<Integer> qualityLow, qualityMedium, qualityHigh;
}
