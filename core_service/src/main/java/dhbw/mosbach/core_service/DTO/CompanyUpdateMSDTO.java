package dhbw.mosbach.core_service.DTO;

public class CompanyUpdateMSDTO {
    public int warehousePercentage, profitMargin;

    public CompanyUpdateMSDTO(int warehousePercentage, int profitMargin) {
        this.warehousePercentage = warehousePercentage;
        this.profitMargin = profitMargin;
    }
}
