package dhbw.mosbach.core_service.DTO;

import java.util.List;

public class ListCompanyByTypeDTO {
    public List<CompanyByTypeDTO> companies;
}
