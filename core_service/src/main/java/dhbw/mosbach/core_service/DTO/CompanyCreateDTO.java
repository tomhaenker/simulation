package dhbw.mosbach.core_service.DTO;

import dhbw.mosbach.core_service.CompanyType;

public class CompanyCreateDTO {
    public CompanyType companyType;
    public String qualityType, companySize;
    public int budget, warehousePercentage, profitMargin;
}