package dhbw.mosbach.core_service.DTO;

public class SimulationStatusDTO {
    public boolean running, paused;
    public int day;
}
