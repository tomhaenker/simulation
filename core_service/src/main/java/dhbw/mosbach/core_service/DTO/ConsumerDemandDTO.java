package dhbw.mosbach.core_service.DTO;

public class ConsumerDemandDTO {
    public int qualityLow;
    public int qualityMedium;
    public int qualityHigh;
}
