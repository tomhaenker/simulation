package dhbw.mosbach.core_service.DTO;

public class DataOverviewDTO {
    public CompanyDataOverview waterplants, cerealFarmers, hopFarmers, malters, brewerys;
    public ConsumerDemandDTO consumerDemand;
    public int day;
}

class SummaryByQuality{
    public int count;
    public int balance;
    public int amount;
}

