package dhbw.mosbach.core_service;

import org.springframework.http.HttpMethod;
import org.springframework.web.client.HttpServerErrorException;

public class DayThread implements Runnable {

    private RequestService requestService;
    private CompanyType companyType;
    private String endpointPath;
    private Integer dayCount;

    /**
     * Konstruktor der Klasse DayThread
     * @param companyType Der zugehörige CompanyType
     * @param startDay Gibt an, ob startDay oder endSimulation an die Services gesendet werden soll
     * @param dayCount Der aktuell zu startende Tag
     */
    public DayThread(CompanyType companyType, boolean startDay, Integer dayCount){
        this.dayCount = dayCount;
        this.companyType = companyType;
        requestService = new RequestService();
        if (startDay)
            endpointPath = "startDay";
        else
            endpointPath = "endSimulation";
    }
    /**
     * Konstruktor der Klasse DayThread
     * @param companyType Der zugehörige CompanyType
     * @param startDay Gibt an, ob startDay oder endSimulation an die Services gesendet werden soll
     */
    public DayThread(CompanyType companyType, boolean startDay){
        this(companyType, startDay, null);
    }


        @Override
    public void run() {
        try {
            requestService.sendRequest(this.companyType, endpointPath, HttpMethod.POST, dayCount, null);
        } catch (HttpServerErrorException e) {
            System.out.println("Error caused by calling /api/startDay in " + companyType.name() + "\n" + e.getMessage());
        }
    }
}
