package dhbw.mosbach.core_service;

import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;

import dhbw.mosbach.core_service.DTO.*;

@CrossOrigin(origins = "http://localhost:8090")
@RestController
public class MainController {

    private boolean simulationIsRunning = false;
    StartDay startDay;
    RequestService requestService;

    public MainController(){
        this.requestService = new RequestService();
        this.startDay = StartDay.getInstance();
    }

    @GetMapping("/api/simulationStatus")
    public SimulationStatusDTO getSimulationStatus(){
        SimulationStatusDTO result = new SimulationStatusDTO();
        result.paused = startDay.getPaused();
        result.running = this.simulationIsRunning;
        result.day = startDay.getDayCount();
        return result;
    }

    @PostMapping("/api/unitPrices")
    public void initializePrices(@RequestBody PricesDTO prices){
        //Verteilen der Preise aus dem Frontend auf die MicroServices
        requestService.sendRequest(CompanyType.hopFarmer, "unitPrice", HttpMethod.POST, prices.unitPriceHop, null);
        requestService.sendRequest(CompanyType.cerealFarmer, "unitPrice", HttpMethod.POST, prices.unitPriceCereal, null);
        requestService.sendRequest(CompanyType.waterplant, "unitPrice", HttpMethod.POST, prices.unitPriceWater, null);
    }

    @GetMapping("/api/unitPrices")
    public PricesDTO initializePrices(){
        PricesDTO prices = new PricesDTO();
        //Preise aus den MicroServices erhalten
        prices.unitPriceHop =  requestService.sendRequest(CompanyType.hopFarmer, "unitPrice", HttpMethod.GET, null, Integer.class);
        prices.unitPriceCereal = requestService.sendRequest(CompanyType.cerealFarmer, "unitPrice", HttpMethod.GET, null, Integer.class);
        prices.unitPriceWater = requestService.sendRequest(CompanyType.waterplant, "unitPrice", HttpMethod.GET, null, Integer.class);

        return prices;
    }

    @PostMapping("/api/pauseSimulation")
    public void pauseSimulation(@RequestBody PauseDTO pauseDTO){
        startDay.setPause(pauseDTO.pause);
        if (!pauseDTO.pause)
            this.simulationIsRunning = true;
    }

    @PostMapping("/api/endSimulation")
    public void endSimulation(){
        this.simulationIsRunning = false;
        startDay.setPause(true);
        //Jeden MicroService auffordern, die Simulation zu beenden
        for (CompanyType companyType:
                CompanyType.values()) {
            new Thread(new DayThread(companyType, false)).start();
        }

        startDay.resetDayCount();
    }

    @GetMapping("/api/dataOverview")
    public DataOverviewDTO getDataOverview(){
        // alle Services anpingen und Daten holen
        // Daten hier "aufbereiten" bzw. zusammenfassen
        // Daten zurückschicken

        DataOverviewDTO result = new DataOverviewDTO();
        result.brewerys = requestService.sendRequest(CompanyType.brewery, "dataOverview", HttpMethod.GET, null, CompanyDataOverview.class);
        result.cerealFarmers = requestService.sendRequest(CompanyType.cerealFarmer, "dataOverview", HttpMethod.GET, null, CompanyDataOverview.class);
        result.hopFarmers = requestService.sendRequest(CompanyType.hopFarmer, "dataOverview", HttpMethod.GET, null, CompanyDataOverview.class);
        result.malters = requestService.sendRequest(CompanyType.malter, "dataOverview", HttpMethod.GET, null, CompanyDataOverview.class);
        result.waterplants = requestService.sendRequest(CompanyType.waterplant, "dataOverview", HttpMethod.GET, null, CompanyDataOverview.class);
        result.consumerDemand = requestService.sendRequest(CompanyType.consumer, "demand", HttpMethod.GET, null, ConsumerDemandDTO.class);
        result.day = startDay.getDayCount();

        return result;
    }

    @PostMapping("/api/company")
    public void createCompany(@RequestBody CompanyCreateDTO companyCreateDTO){
        //Gewünschten Microservice ansprechen, um Unternehmen zu erzeugen
        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.budget = companyCreateDTO.budget;
        companyDTO.profitMargin = companyCreateDTO.profitMargin;
        companyDTO.qualityType = companyCreateDTO.qualityType;
        companyDTO.warehousePercentage = companyCreateDTO.warehousePercentage;
        companyDTO.companySize = companyCreateDTO.companySize;

        requestService.sendRequest(companyCreateDTO.companyType, "company", HttpMethod.POST, companyDTO, null);
    }

    @GetMapping("/api/company/{companyType}")
    public ListCompanyByTypeDTO getDataByCompanyType(@PathVariable CompanyType companyType){
        return requestService.sendRequest(companyType, "company", HttpMethod.GET, null, ListCompanyByTypeDTO.class);
    }

    @PutMapping("/api/company")
    public void changeCompanyData(@RequestBody CompanyUpdateDTO companyUpdateDTO){
            requestService.sendRequest(
                    CompanyType.valueOf(companyUpdateDTO.companyType),
                    "company" + "/" + companyUpdateDTO.companyId,
                    HttpMethod.PUT,
                    new CompanyUpdateMSDTO(companyUpdateDTO.warehousePercentage, companyUpdateDTO.profitMargin),
                    null
            );
    }

    @DeleteMapping("/api/company")
    public void deleteCompany(@RequestBody CompanyDeleteDTO company){
        requestService.sendRequest(CompanyType.valueOf(company.companyType), "company/" + company.companyId, HttpMethod.DELETE, null, null);
    }

    @PutMapping("/api/demand")
    public void setCustomerDemand(@RequestBody ConsumerDemandDTO consumerDemand){
        requestService.sendRequest(CompanyType.consumer, "demand", HttpMethod.PUT, consumerDemand, null);
    }

    @PostMapping("/api/randomCompany")
    public void createRandomCompany(@RequestBody CompanyTypeDTO companyTypeDTO){
        requestService.sendRequest(companyTypeDTO.companyType, "randomCompany", HttpMethod.POST, null, null);
    }

    @GetMapping("/api/orderHistory/{companyType}")
    public String getOrderHistory(@PathVariable CompanyType companyType){
        return requestService.sendRequest(companyType, "orderHistory", HttpMethod.GET, null, String.class);
    }

    @GetMapping("/api/priceHistory/{companyType}")
    public String getPriceHistory(@PathVariable CompanyType companyType){
        return requestService.sendRequest(companyType, "priceHistory", HttpMethod.GET, null, String.class);
    }

}
