package dhbw.mosbach.core_service;

public enum CompanyType {

    brewery(5),
    malter(4),
    hopFarmer(3),
    cerealFarmer(2),
    waterplant(1),
    consumer(0);

    private int value;

    CompanyType(int value){
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
