package dhbw.mosbach.core_service;

import java.io.*;
import java.util.Properties;

public class PropertiesCache {
    private final Properties configProp = new Properties();

    private PropertiesCache()
    {
        //Private constructor to restrict new instances
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("application.properties");
        System.out.println("Reading all properties from the file");
        try {
            configProp.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Bill Pugh Solution for singleton pattern
    private static class LazyHolder
    {
        private static final PropertiesCache INSTANCE = new PropertiesCache();
    }

    public static PropertiesCache getInstance()
    {
        return LazyHolder.INSTANCE;
    }

    public String getProperty(String key){
        return configProp.getProperty(key);
    }

    public boolean containsKey(String key){
        return configProp.containsKey(key);
    }

    public void setProperty(String key, String value){
        configProp.setProperty(key, value);
        this.flush();
    }

    public void flush() {
        try (final OutputStream outputstream
                     = new FileOutputStream(getProperty("simulation.propertiesFilePath"))) {
            configProp.store(outputstream,"File Updated");
        } catch (FileNotFoundException fnf){
            System.out.println("FileNotFoundException when trying to flush: " + fnf.getMessage());
        } catch (IOException ioe){
            System.out.println("IOException  when trying to flush: " + ioe.getMessage());
        }
    }
}