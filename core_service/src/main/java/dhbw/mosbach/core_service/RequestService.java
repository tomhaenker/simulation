package dhbw.mosbach.core_service;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

/**
 * Stellt einen Service dar, um Anfragen an eine bestimmte API zu stellen
 */
public class RequestService {
    /*
    @Value("${simulation.waterplant.url}") private String waterplantPath;
    @Value("${simulation.cerealfarmer.url}") private String cerealfarmerPath;
    @Value("${simulation.consumer.url}") private String consumerPath;
    @Value("${simulation.brewery.url}") private String breweryPath;
    @Value("${simulation.hopfarmer.url}") private String hopfarmerPath;
    @Value("${simulation.malter.url}") private String malterPath;
     */

    /**
     *
     * @param companyType Durch den companyType wird entschieden, welcher Service angesprochen werden soll.
     * @param path Der Pfad stellt den Endpunkt dar
     * @param httpMethod Die zu verwendende HTTP-Methode
     * @param requestObject Das Objekt, welches als JSON an die API gesendet werden soll.
     * @param returnType Der Typ, welcher von der API als Antwort erwartet wird.
     * @param <T> Generischer Parameter des requestObject.
     * @param <G> Generischer Parameter des returnType.
     * @return Gibt ein Objekt vom Typ <G> zurück
     */
    public <T, G> G sendRequest(CompanyType companyType, String path, HttpMethod httpMethod, T requestObject, Class returnType){
        //Ganze Url generieren
        final String uri = getUrlByCompanyType(companyType) + path;
        //API-Request vorbereiten
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<G> response = null;
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<T> entity = new HttpEntity<>(requestObject, headers);
            try {
                if (returnType == null) {    //Wenn keine Antwort erwartet wird
                    if (httpMethod == HttpMethod.POST) {    // & ein POST verwendet wird
                        restTemplate.postForLocation(uri, requestObject);   // muss diese Funktion verwendet werden
                        return null;
                    }
                }
                // Senden der Anfrage an die API des entsprechenden Services
                response = restTemplate.exchange(uri, httpMethod, entity, returnType);

            } catch (Exception e) {
                // Wenn der Service momentan nicht zur Verfügung steht, wird null zurückgegeben
                if(e.getCause().toString().equals("java.net.ConnectException: Connection refused")) {
                    System.out.println("Connecting to service " + companyType.name().toUpperCase() + " not possible. Check, if service is running. (" + httpMethod.name() + ": " + uri + ")");
                    return null;
                } else {
                    System.out.println("Sending request to "+ companyType.name().toUpperCase() + " failed. (" + httpMethod.name() + ": " + uri + ")");
                    System.out.println(e.getMessage());

                    if (response == null)
                        return null;
                }
            }
        // Gibt die Antwort der Anfrage als Objekt vom Typ <G> zurück
        return response.getBody();
    }


//    private String getUrlByCompanyType(CompanyType companyType){
//        String resultUrl;
//
//        switch (companyType){
//            case brewery:
//                resultUrl = breweryPath;
//                break;
//            case malter:
//                resultUrl = malterPath;
//                break;
//            case hopFarmer:
//                resultUrl = hopfarmerPath;
//                break;
//            case cerealFarmer:
//                resultUrl = cerealfarmerPath;
//                break;
//            case waterplant:
//                resultUrl = waterplantPath;
//                break;
//            case consumer:
//                resultUrl = consumerPath;
//                break;
//            default:
//                throw new IllegalArgumentException();
//        }
//        return resultUrl + "/api/";
//    }


    private String getUrlByCompanyType(CompanyType companyType){
        String resultUrl = "http://localhost:";
        switch (companyType){
            case brewery:
                resultUrl += "8082";
                break;
            case malter:
                resultUrl += "8083";
                break;
            case hopFarmer:
                resultUrl += "8084";
                break;
            case cerealFarmer:
                resultUrl += "8085";
                break;
            case waterplant:
                resultUrl += "8086";
                break;
            case consumer:
                resultUrl += "8081";
                break;
            default:
                throw new IllegalArgumentException();
        }
        return resultUrl + "/api/";
    }
}
