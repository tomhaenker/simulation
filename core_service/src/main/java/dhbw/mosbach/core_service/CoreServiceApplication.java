package dhbw.mosbach.core_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoreServiceApplication {

//    @Autowired
//    static ApplicationContext applicationContext;

    public static void main(String[] args) {
        SpringApplication.run(CoreServiceApplication.class, args);
        Thread thread1 = new Thread(StartDay.getInstance());
//        ohne diese Zeile können die Beans nicht geladen werden: konkreter Fehlerfall --> properties werden nicht gelesen
//        applicationContext.getAutowireCapableBeanFactory().autowireBean(thread1);
        thread1.start();
    }

}
