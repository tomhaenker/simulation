package dhbw.mosbach.core_service;

import org.springframework.beans.factory.annotation.Value;

import java.util.*;

public class StartDay implements Runnable {
    
    private static StartDay instance;
    private Boolean paused = true;

    private int dayCount;

    PropertiesCache propertiesCache;

    private StartDay(){
        propertiesCache = PropertiesCache.getInstance();
        if(propertiesCache.containsKey("simulation.dayCount"))
            dayCount = Integer.valueOf(propertiesCache.getProperty("simulation.dayCount"));
    }

    /**
     * Singleton-Pattern Implementation für die Klasse StartDay
     * @return Gibt die (einzige) Instanz der Klasse StartDay zurück
     */
    public static StartDay getInstance(){
        if(instance == null)
            instance = new StartDay();
        return instance;
    }
    public boolean getPaused() {
        return paused;
    }

    public int getDayCount() {
        return dayCount;
    }

    private void setDayCount(Integer newDayCount){
        this.dayCount = newDayCount;
        propertiesCache.setProperty("simulation.dayCount", String.valueOf(dayCount));
    }

    public void resetDayCount() {
        setDayCount(0);
    }

    public void setPause(boolean pause){
        this.paused = pause;
    }

    @Override
    public void run(){
        while(true){        //Thread läuft für immer
                if(this.paused){    //Wenn die Simulation pausiert ist, wartet der Thread
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
            }

            startDayServices();
            try {
                // 2 Sekunden warten, um einen zeitlichen Abstand zwischen den Tagen zu erreichen
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Jedem MicroService Bescheid geben, dass ein neuer Tag begonnen hat.
     * dayCount wird hochgezählt.
     */
    private void startDayServices(){
        this.setDayCount(dayCount+1);
        System.out.println("Starting day " + this.dayCount + " of services..");

        List<Thread> threadList = new ArrayList<>();
        for (CompanyType companyType: CompanyType.values()) {
            Thread t = new Thread(new DayThread(companyType, true, dayCount));
            threadList.add(t);
            t.start();
        }
        
        for (Thread t : threadList) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
         
    }
}
