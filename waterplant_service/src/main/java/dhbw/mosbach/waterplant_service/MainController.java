package dhbw.mosbach.waterplant_service;

import dhbw.mosbach.waterplant_service.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class MainController {

    final WaterplantRepository waterplantRepository;
    final ResourceValueRepository resourceValueRepository;
    final ApplicationContext applicationContext;
    public static List<OfferDTO> availableOffers = new ArrayList<>();

    public MainController(WaterplantRepository waterplantRepository, ApplicationContext applicationContext, ResourceValueRepository resourceValueRepository) {
        this.waterplantRepository = waterplantRepository;
        this.applicationContext = applicationContext;
        this.resourceValueRepository = resourceValueRepository;
    }

    /**
     * Startet einen neuen Tag
     * @throws InterruptedException
     */
    @PostMapping("/api/startDay")
    public void startDay() throws InterruptedException {

        //Sollte der Service neugestartet werden, wird der in der DB hinterlegte Rohstoffwert herangezogen.
        //Ist die DB allerdings leer, existiert kein Rohstoffwert und "0.0" wird akzeptiert
        //Ist ein Wert vorhanden, findet ebenfalls keine neue Belegung statt.
        if((Waterplant.getParagon().compareTo(BigDecimal.ZERO) <= 0) && (resourceValueRepository.count()>0))
            Waterplant.setParagon(resourceValueRepository.findAll().get(0).getValue());


        synchronized (availableOffers) {
            availableOffers.clear(); // Angebote des Vortages werden gelöscht
        }
        List<Waterplant> waterplants = waterplantRepository.findAll();
        List<DayThread> companyDayThreads = new ArrayList<>();
        synchronized (availableOffers) {
//        Angebote werden vor Produktion erstellt.
//        So wird sichergestellt, dass keine Waren angebote bzw. verkauft werden, die am selben Tag produziert wurden
            for (Waterplant waterplant : waterplants) {
                availableOffers.add(new OfferDTO(waterplant));
            }
        }
        for (Waterplant waterplant : waterplants) {
//            für jedes Unternehmen wird ein separater Thread zur Simulation geöffnet
            DayThread dayThread = new DayThread(waterplantRepository);
            applicationContext.getAutowireCapableBeanFactory().autowireBean(dayThread);
            dayThread.start(waterplant);
            companyDayThreads.add(dayThread);
        }
        for (DayThread daythread : companyDayThreads) {
//            es wird nacheinander geprüft ob jedes Unternehmen fertig berechnet wurde
            daythread.join();
        }
//        Wenn alle Unternehmen simuliert wurden gilt die Methode als abgeschlossen
//        Erst jetzt wird durch den HTTP Status Code 200 signalisiert, dass der Tag fertig simuliert wurde
    }

    /**
     * Liefert alle Angebote einer Qualität zurück
     * @param qualityLevel Angefragte Qualität
     * @return Liste aller Unternehmen die Bier dieser Qualität liefern können
     */
    @GetMapping("/api/request/{qualityLevel}")
    public List<OfferDTO> receiveRequest(
            @PathVariable QualityLevel qualityLevel) {
//        QualityLevel qualityLevel = QualityLevel.getQualityLevel(qualityLevelInt);
        List<OfferDTO> matchingOffers = new ArrayList<>();
        while (availableOffers.size() == 0 && waterplantRepository.count() > 0){ //Wenn keine Brauereien angelegt sind kann es auch keine Angebote geben
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
        synchronized (availableOffers) {
//        throw new IllegalArgumentException("Start day before request data"); // Liste wird erstmals beim Starten von Tag 1 gefüllt
            for (OfferDTO offerDTO : availableOffers) {
                if (offerDTO.getQualityLevel(waterplantRepository).equals(qualityLevel)) matchingOffers.add(offerDTO);
            }
        }
        return matchingOffers;
    }



    /**
     * Führt eine Bestellung aus
     * @param order Bestelldetails (VerkäuferID und Menge)
     * @return Bestellbestätigung
     * @exception ResponseStatusException
     */
    @PostMapping("/api/order")
    public ConfirmOrderDTO receiveOrder(
            @RequestBody OrderDTO order) throws IllegalArgumentException, ResponseStatusException {
        if (waterplantRepository.findById(order.companyId).isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Company does not exist");
        Waterplant waterplant = waterplantRepository.findById(order.companyId).get();
        synchronized (availableOffers) {
//        Der Bestand des Wassers muss in den Offers geprüft werden. Stellt sicher, dass der Bestand zu Beginn des Tages geprüft wird
            for (OfferDTO offer : availableOffers) {
                if (offer.id.equals(order.companyId)) {
                    if (offer.quantity < order.quantity)
                        throw new ResponseStatusException(HttpStatus.GONE, "Not enough goods available");
                    else {
                        offer.quantity = offer.quantity - order.quantity; // Update der heutigen Daten
                        waterplant.sellProduct(order.quantity, BigDecimal.valueOf(offer.unitPrice)); // Update der Gesamtdaten
                        waterplantRepository.save(waterplant);
                        return new ConfirmOrderDTO(waterplant.getId(), order.quantity, offer.unitPrice);
                    }
                }
            }
        }
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong" );
    }

    /**
     * Liefert detaillierte Daten der vorhandenen Unternehmen zurück
     * @return Brauereidaten
     */
    @GetMapping("/api/company")
    public DataDTO getAllCompanyData(){
        List<Waterplant> waterplants =  waterplantRepository.findAll();
        List<WaterplantGetDTO> dtoList = new ArrayList<>();
        for (Waterplant waterplant: waterplants) {
            dtoList.add(new WaterplantGetDTO(waterplant));
        }
        return new DataDTO(dtoList);
    }

    /**
     * Liefert zusammengefasste Daten der vorhandenen Unternehmen zurück
     * @return Brauereidaten
     */
    @GetMapping("/api/dataOverview")
    public OverviewDTO getAllCompanyOverview(){
        List<Waterplant> waterplants =  waterplantRepository.findAll();
        OverviewQualityDetailDTO qualityLow = new OverviewQualityDetailDTO();
        OverviewQualityDetailDTO qualityMiddle = new OverviewQualityDetailDTO();
        OverviewQualityDetailDTO qualityHigh = new OverviewQualityDetailDTO();
        for (Waterplant waterplant: waterplants) {
            switch (waterplant.getQualityLevel()){
                case LOW: qualityLow.addCompany(waterplant); break;
                case MEDIUM: qualityMiddle.addCompany(waterplant); break;
                case HIGH: qualityHigh.addCompany(waterplant); break;
            }
        }

        return new OverviewDTO(qualityLow, qualityMiddle, qualityHigh);
    }

    /**
     * Erstellt Unternehmen mit zufälligen Parametern
     * Schnittstelle ist für das Testen gedacht
     * @param number Anzahl der Unternehmen die erstellt werden soll
     */
    @PostMapping("/api/createDummies/{number}")
    public void createDummies(
            @PathVariable int number){
        int counter = 0;
        while (counter < number){
            Waterplant waterplant = Waterplant.createRandom();
//            Für die Tests sollen Unternehmen mit vollen Lagern erzeugt werden
            waterplant.setAmount_product(waterplant.getMax_amount_product());
            waterplantRepository.save(waterplant);
            counter++;
        }
    }

    /**
     * Wird aufgerufen um alle Daten zu löschen und bevor eine neue Simulation gestartet wird
     */
    @PostMapping("/api/endSimulation")
    public void deleteData(){
        if (waterplantRepository.count() > 0) waterplantRepository.deleteAllInBatch();
        Waterplant.resetCounter();
        Waterplant.setParagon(BigDecimal.ZERO);
        if (resourceValueRepository.count() > 0) resourceValueRepository.deleteAllInBatch();
    }


    /**
     * setzt den Wert des Rohstoffs Wasser auf
     * @param unitPrice
     */
    @PostMapping("api/unitPrice")
    public void setResourceValue(@RequestBody int unitPrice)
    {

        //Wandelt eingetragenen Rohstoffwert von Cent pro Tonne auf Euro pro Kilo (=resourceValue)
        double kiloValue = (double)unitPrice/100000;
        BigDecimal resourceValue = BigDecimal.valueOf(kiloValue);

        //Wert wird für später angelegte Unternehmen als Muster initialisiert
        Waterplant.setParagon(resourceValue);

        //vorheriger Wert in DB wird gelöscht und neu gesetzt
        if (resourceValueRepository.count() > 0) resourceValueRepository.deleteAllInBatch();
        ResourceValue r = new ResourceValue(resourceValue);
        resourceValueRepository.save(r);

        //In der Datenbank vorhandene Unternehmen bekommen den Wert zugewiesen
        long amount = waterplantRepository.count();
        List<Waterplant> AllCompanies = waterplantRepository.findAll();

        for (Waterplant waterplant: AllCompanies)
        {
            waterplant.setValueResource(resourceValue);
            waterplantRepository.save(waterplant);
        }

        Waterplant.setParagon(resourceValue);


        }

    /**
     * Gibt den Rohstoffwert in Cent pro Tonne zurück
     */
    @GetMapping("/api/unitPrice")
    public int getResourceValue(){
        return Waterplant.getParagon().multiply(new BigDecimal("100000")).intValue();
    }

}

