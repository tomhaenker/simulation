package dhbw.mosbach.waterplant_service.dto;

import dhbw.mosbach.waterplant_service.Waterplant;

public class OverviewQualityDetailDTO {
    private int count;
    private int balance;
    private int amount;

    public void addCompany(Waterplant waterplant){
        count++;
        balance = balance + waterplant.getBudget().subtract(waterplant.getStartBudget()).movePointRight(2).intValue();
        amount = amount + waterplant.getAmount_product();
    }

    public int getCount() {
        return count;
    }

    public int getBalance() {
        return balance;
    }

    public int getAmount() {
        return amount;
    }
}
