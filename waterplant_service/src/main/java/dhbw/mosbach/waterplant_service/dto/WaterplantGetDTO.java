package dhbw.mosbach.waterplant_service.dto;

import dhbw.mosbach.waterplant_service.Waterplant;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

public class WaterplantGetDTO {
    public UUID companyId;
    public int companyIdNumeric;
    public String companySize;
    public String qualityType;
    public int budget;
    public int balance; // steht für die Differenz zwischen Startbudget und aktuellem
    public int currentStock;
    public int warehousePercentage;
    public int profitMargin;

    public WaterplantGetDTO(Waterplant waterplant) {
        this.companyId = waterplant.getId();
        this.qualityType = waterplant.getQualityLevel().toString();
        this.budget = waterplant.getBudget().movePointRight(2).intValue();
        this.balance = waterplant.getBudget().subtract(waterplant.getStartBudget()).movePointRight(2).intValue();
        this.warehousePercentage = new BigDecimal(waterplant.getTarget_amount_product()).divide(new BigDecimal(waterplant.getMax_amount_product()), 10, RoundingMode.DOWN).movePointRight(2).intValue(); //Wert ist immer ein Ganzzahl
        this.profitMargin = waterplant.getProfit_margin().movePointRight(2).intValue();
        this.currentStock = waterplant.getAmount_product();
        this.companySize = waterplant.getSize().toString();
        this.companyIdNumeric = waterplant.getNumericalId();
    }
}
