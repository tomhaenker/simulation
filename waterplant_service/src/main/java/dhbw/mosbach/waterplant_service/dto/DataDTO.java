package dhbw.mosbach.waterplant_service.dto;

import java.util.ArrayList;
import java.util.List;

public class DataDTO {
    public List<WaterplantGetDTO> companies = new ArrayList<>();

    public DataDTO(List<WaterplantGetDTO> companies) {
        this.companies = companies;
    }
}
