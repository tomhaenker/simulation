package dhbw.mosbach.waterplant_service.dto;

import dhbw.mosbach.waterplant_service.Waterplant;
import dhbw.mosbach.waterplant_service.WaterplantRepository;
import dhbw.mosbach.waterplant_service.QualityLevel;

import java.util.UUID;

public class OfferDTO implements Comparable<OfferDTO>{
    public UUID id;
    public int quantity;
    public double unitPrice;

    public OfferDTO() { }


    public OfferDTO(Waterplant waterplant) {
        this(waterplant.getId(), waterplant.getAmount_product(), waterplant.calcSellPrice().doubleValue());
    }

    public OfferDTO(UUID id, int quantity, double unitPrice) {
        this.id = id;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }


    public QualityLevel getQualityLevel(WaterplantRepository repository){
        if (repository.findById(id).isEmpty()) throw new IllegalArgumentException("Wrong Id");
        return repository.findById(id).get().getQualityLevel();
    }


    public double getUnitPrice() {
        return unitPrice;
    }

    @Override
    public int compareTo(OfferDTO o) {
        return this.id.compareTo(o.id);
    }
}
