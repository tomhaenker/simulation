package dhbw.mosbach.waterplant_service.dto;

public class WaterplantPostDTO {
    public String companySize;
    public String qualityType;
    public int budget;
    public int warehousePercentage;
    public int profitMargin;
}
