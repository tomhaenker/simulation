package dhbw.mosbach.waterplant_service.dto;

import java.util.UUID;


public class OrderDTO {
    public UUID companyId;
    public int quantity;

    public OrderDTO(UUID id, int amount) {
        this.companyId = id;
        this.quantity = amount;
    }

}
