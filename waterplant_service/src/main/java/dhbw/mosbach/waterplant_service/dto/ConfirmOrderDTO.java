package dhbw.mosbach.waterplant_service.dto;

import java.util.UUID;

public class ConfirmOrderDTO {
    public UUID companyId;
    public int quantity;
    public double unitPrice;

    public ConfirmOrderDTO() { }

    public ConfirmOrderDTO(UUID companyId, int quantity, double unitPrice) {
        this.companyId = companyId;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }
}
