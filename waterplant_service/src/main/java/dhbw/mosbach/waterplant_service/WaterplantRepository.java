package dhbw.mosbach.waterplant_service;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface WaterplantRepository extends JpaRepository<Waterplant, UUID> {

    public List<Waterplant> findAllByQualityLevel(QualityLevel qualityLevel);
}
