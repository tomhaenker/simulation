package dhbw.mosbach.waterplant_service;

import dhbw.mosbach.waterplant_service.dto.WaterplantGetDTO;
import dhbw.mosbach.waterplant_service.dto.WaterplantPostDTO;
import dhbw.mosbach.waterplant_service.dto.WaterplantPutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
public class CrudController {

    final
    WaterplantRepository waterplantRepository;

    public CrudController(WaterplantRepository waterplantRepository) {
        this.waterplantRepository = waterplantRepository;
    }

    @PostMapping("/api/company")
    public void createWaterplant(@RequestBody WaterplantPostDTO waterplantPostDTO){
        waterplantRepository.save(new Waterplant(waterplantPostDTO));
    }

    @GetMapping("/api/company/{id}")
    public WaterplantGetDTO getWaterplant(@PathVariable UUID id){
        Optional<Waterplant> waterplant = waterplantRepository.findById(id);
        if (waterplant.isEmpty()) throw new IllegalArgumentException("Wrong CompanyID");
        return new WaterplantGetDTO(waterplant.get());
    }

    @PutMapping("/api/company/{id}")
    public WaterplantGetDTO updateWaterplant(@PathVariable UUID id, @RequestBody WaterplantPutDTO waterplantPutDTO){
        Optional<Waterplant> optionalWaterplant = waterplantRepository.findById(id);
        if (optionalWaterplant.isEmpty()) throw new IllegalArgumentException("Wrong CompanyID");
        Waterplant waterplant = optionalWaterplant.get();
        waterplant.updateTargetAmount(waterplantPutDTO.getWarehousePercentage());
        waterplant.setProfit_margin(waterplantPutDTO.getProfitMargin());
        waterplantRepository.save(waterplant);
        return new WaterplantGetDTO(waterplant);
    }

    @DeleteMapping("/api/company/{id}")
    public void deleteCompany(@PathVariable UUID id){
        if (waterplantRepository.findById(id).isEmpty()) return;
        waterplantRepository.deleteById(id);
    }

    @PostMapping("/api/randomCompany")
    public void createRandomWaterplant(){
        waterplantRepository.save(Waterplant.createRandom());
    }

}
