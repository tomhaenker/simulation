package dhbw.mosbach.waterplant_service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dhbw.mosbach.waterplant_service.dto.ConfirmOrderDTO;
import dhbw.mosbach.waterplant_service.dto.OfferDTO;
import dhbw.mosbach.waterplant_service.dto.OrderDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Component
public class DayThread extends Thread{

    private final WaterplantRepository waterplantRepository;

    public DayThread(WaterplantRepository waterplantRepository) {
        this.waterplantRepository = waterplantRepository;
    }

    public synchronized void start(Waterplant waterplant) {
        /*
        Täglich anfallende Kosten verbuchen
         */
        waterplant.bookDailyCosts();
        waterplantRepository.save(waterplant);

        /*
        Verkauf:
        Beim Start des Tages wird im Controller ein zwischenstand gezogen mit dem die Nachfrage bedient wird.
        Änderungen an Bestand und Preisen können daher nun direkt am Objekt vorgenommen werden
         */

        /*
        Produktion
        Sämtlich Logik ist im Unternehmen vorhanden
         */
        waterplant.produceProduct();
        waterplantRepository.save(waterplant);

        /*
        prüfen ob Unternehmen noch liquide ist
        Wenn nicht --> Aus dem Markt entfernen
         */
        if (waterplant.getBudget().compareTo(new BigDecimal(0)) < 0){
            waterplantRepository.delete(waterplant);
        }

    }





}


