//////////////////////////////
// Angular Route für ng-view
//////////////////////////////
var app = angular.module('indexPage', ['ngRoute']);

app.config(function ($routeProvider) {
	$routeProvider
		.when("/overview", {
			templateUrl : 'views/overview.html',
		})
		.when("/hopfarmer", {
			templateUrl : 'views/hopfarmer.html', 
		})
		.when("/cerealfarmer", {
			templateUrl : 'views/cerealfarmer.html', 
		})
		.when("/waterplant", {
			templateUrl : 'views/waterplant.html', 
		})
		.when("/malter", {
			templateUrl : 'views/malter.html', 
		})
		.when("/brewery", {
			templateUrl : 'views/brewery.html', 
		})
		.when("/consumer", {
			templateUrl : "views/consumer.html", 
		})
		// Klick auf Logo -> zeigt index.html (Startpage)
		.otherwise({
			template : '',
	});
	
});



//////////////////////////////
// App-Service
//////////////////////////////
app.factory('beerService', function($scope, $http, $log, $location, $window) {
	
});



//////////////////////////////
// App-Directive - StringToNumber für input type "number"
//////////////////////////////
app.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
});






// Index
app.controller('mainController', function($scope, $http, $log, $location, $window, $interval) {
	
	// Status Slider
	$scope.initSimStatus = function(){
		$scope.simStatus = false;
		// Status für pauseSimulation an Core -> true = Pause | false = Läuft
		$scope.postSimStatus = true;
	}	
	$scope.changeStatus = function(){
		$scope.simStatus = !$scope.simStatus;
		$scope.postSimStatus = !$scope.postSimStatus;
		$scope.postPauseSimulation();
	}
	
	
	// Status Slider und initializePricesForm beim Beenden auf false setzen
	$scope.disableSlider = function() {
		$scope.simStatus = false;
		$scope.indexSlider = false;
		$scope.initializePricesForm = false;
	}
	
	
	// Simulation starten oder beenden
	$scope.initSimAction = function(){
		$scope.simButton = true;
		$scope.showSidebarMenu = false;
		$scope.disableFunctions = true;
		$scope.initializePricesForm = false;
	}
	$scope.changeSimulation = function(){
		$scope.simButton = !$scope.simButton;
		$scope.disableFunctions = !$scope.disableFunctions;
	}
	
	
	// Form für initializePrices beim Start der Simulation einblenden / ausblenden
	$scope.openPricesForm = function(){
		$scope.initializePricesForm = true;
	}
	$scope.closePricesForm = function(){
		$scope.initializePricesForm = false;
	}
	
	
	// Laufzeit beim Start auf 0 setzen und letzte 2 Ziffern für Progress Bar % verwenden
	$scope.initSimProgress = function(){
		$scope.simDuration = 0;
		$scope.numberLimit = -2;
	}	
	
	
	// Einblenden Sidebar Icon Navigation im Burger Menü für Medium Size 
	$scope.changeMenuIconStatus = function() {
			$scope.showSidebarMenu = !$scope.showSidebarMenu;
	}
	
	
	// Ausblenden Sidebar
	$scope.closeSidebarMenu = function() {
		$scope.showSidebarMenu = false;
	}
	
	
	// POST pauseSimulation - Simulation laufen lassen und pausieren
	$scope.postPauseSimulation = function() {
        $http({
            method: "post",
            url: "http://localhost:3000/api/pauseSimulation",
            data: {
				// true = Pause | false = Läuft
                pause: $scope.postSimStatus,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			$log.info(response);
			
		}, function (response) {
			alert("Error " + response.data);
			
			/* Testing JSON Output
			var testData = {
				pause: $scope.postSimStatus,
			}
			alert("Ausgabe JSON pauseSimulation: " + JSON.stringify(testData));
			*/
			
		});
	}
	
	
	// POST endSimulation - Simulation komplett beenden
	$scope.postEndSimulation = function() {
        $http({
            method: "post",
            url: "http://localhost:3000/api/endSimulation",
            data: {
				// keine Daten übermitteln
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			$log.info(response);
			
		}, function (response) {
			alert("Error " + response.data);
			
			/* Testing JSON Output
			var testHeaders = {
				'Content-Type': 'application/json'
			}
			alert("Ausgabe JSON headers endSimulation: " + JSON.stringify(testHeaders));
			*/
			
		});
	}
	
	
	// POST initializePrices - Einkaufspreise für Grundstoffe + "Start" der Simulation
	$scope.newInitializePrices = {};
    $scope.postInitializePrices = function() {
        $http({
            method: "post",
            url: "http://localhost:3000/api/initializePrices",
            data: {
				/* Übergabe Preise umgerechnet auf Euro pro KG/Liter */
                unitPriceHop: $scope.newInitializePrices.unitPriceHop/1000,
                unitPriceCereal: $scope.newInitializePrices.unitPriceCereal/1000,
                unitPriceWater: $scope.newInitializePrices.unitPriceWater/1000,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			$log.info(response);
			
		}, function (response) {
			alert("Error " + response.data);
			
			/* Testing JSON Output
			var testData = {
				unitPriceHop: $scope.newInitializePrices.unitPriceHop/1000,
                unitPriceCereal: $scope.newInitializePrices.unitPriceCereal/1000,
                unitPriceWater: $scope.newInitializePrices.unitPriceWater/1000,
			}
			alert("Ausgabe JSON initializePrices: " + JSON.stringify(testData));
			*/
			
		});
	}	
	
	
	// Aufruf der Get-Funktionen dataOverview + company (hopFarmer, cerealFarmer, waterplant, malter, brewery) alle 5 Sekunden
	$interval(function() { $scope.getDataOverview(); }, 5000);
	
	
	// GET dataOverview - Alle Übersichtsdaten + Simulationsdauer (Laufzeit) - Aufruf beim Start
	$http({
		method: "get",
		url: "http://localhost:3000/api/dataOverview",
	}).then(function(response) {
		$scope.overview = response.data;
		$log.info(response);

		// Laufzeit der Simulation - Zeiteinheit
		$scope.simDuration = response.data.day;			

		/* Testing JSON Input
		alert("Eingabe JSON dataOverview: " + JSON.stringify($scope.overview));
		*/
		
	}, function (response) {
		alert("Error " + response.data);
	});
	
	
	// GET dataOverview - Alle Übersichtsdaten + Simulationsdauer (Laufzeit) - Aufruf alle 5 Sekunden
	$scope.getDataOverview = function() {
		$http({
			method: "get",
			url: "http://localhost:3000/api/dataOverview",
		}).then(function(response) {
			$scope.overview = response.data;
			$log.info(response);
			
			// Laufzeit der Simulation - Zeiteinheit
			$scope.simDuration = response.data.day;			
			
			/* Testing JSON Input
			alert("Eingabe JSON dataOverview: " + JSON.stringify($scope.overview));
			*/
			
		}, function (response) {
			alert("Error " + response.data);
		});	
	}
	
});



//////////////////////////////
// Overview
//////////////////////////////
app.controller('overviewController', function($scope, $http, $log, $location, $window, $interval) {
	
});



//////////////////////////////
//Hopfarmer
//////////////////////////////
app.controller('hopfarmerController', function($scope, $http, $log, $location, $window) {
	
	// GET company (hopFarmer) - Alle Daten der vorhandenen Unternehmen holen
	
	
	// POST company (hopFarmer) - Unternehmen anlegen
	
	
	// PUT company (hopFarmer) - Felder: kontinuerlicher Beschaffungsgrad, Gewinnaufschlag des Unternehmens updaten
	
	
	// DELETE company (hopFarmer) - Unternehmen löschen
	
	
});



//////////////////////////////
// Cerealfarmer
//////////////////////////////
app.controller('cerealfarmerController', function($scope, $http, $log, $location, $window) {
	
	// GET company (cerealFarmer) - Alle Daten der vorhandenen Unternehmen holen
	
	
	// POST company (cerealFarmer) - Unternehmen anlegen
	
	
	// PUT company (cerealFarmer) - Felder: kontinuerlicher Beschaffungsgrad, Gewinnaufschlag des Unternehmens updaten
	
	
	// DELETE company (cerealFarmer) - Unternehmen löschen
	
	
});



//////////////////////////////
// Waterplant
//////////////////////////////
app.controller('waterplantController', function($scope, $http, $log, $location, $window) {
	
	// GET company (waterplant) - Alle Daten der vorhandenen Unternehmen holen
	
	
	// POST company (waterplant) - Unternehmen anlegen
	
	
	// PUT company (waterplant) - Felder: kontinuerlicher Beschaffungsgrad, Gewinnaufschlag des Unternehmens updaten
	
	
	// DELETE company (waterplant) - Unternehmen löschen
	
	
});



//////////////////////////////
// Malter
//////////////////////////////
app.controller('malterController', function($scope, $http, $log, $location, $window) {
	
	// GET company (malter) - Alle Daten der vorhandenen Unternehmen holen
	
	
	// POST company (malter) - Unternehmen anlegen
	
	
	// PUT company (malter) - Felder: kontinuerlicher Beschaffungsgrad, Gewinnaufschlag des Unternehmens updaten
	
	
	// DELETE company (malter) - Unternehmen löschen
	
	
});



//////////////////////////////
// Brewery
//////////////////////////////
app.controller('breweryController', function($scope, $http, $log, $location, $window) {

	// Statusvariablen setzten
	$scope.initBrewerys = function(brewery){
		$scope.breweryEditStatus = false;
		$scope.createBreweryForm = false;
		$scope.showBreweryDataDetails = false;
		$scope.showAddBreweryButton = true;
		$scope.companyBreweryEdit = true;
	}
	
	
	// Unternehmensdaten bearbeiten und speichern durch Buttons
	$scope.changeBreweryStatus = function(brewery){
		$scope.breweryEditStatus = !$scope.breweryEditStatus;
	}
	
	
	// Felder zum Bearbeiten freigeben
	$scope.enableCompanyBreweryEdit = function(brewery){
		$scope.companyBreweryEdit = false;
	}
	
	
	// Felder zum Bearbeiten und Sperren
	$scope.disableCompanyBreweryEdit = function(brewery){
		$scope.companyBreweryEdit = true;
	}
	
	
	// Form für createBreweryForm beim Klick auf "+" einblenden
	$scope.openBreweryForm = function(){
		$scope.createBreweryForm = true;
		$scope.showAddBreweryButton = false;
		
		
	// Form für createBreweryForm beim Klick auf "Abbrechen" oder "Speichern" ausblenden
	}
	$scope.closeBreweryForm = function(){
		$scope.createBreweryForm = false;
		$scope.showAddBreweryButton = true;
	}
	
	
	// Details aufklappen / zuklappen - Anzeige der kompletten Informationen für eine Unternehmen
	$scope.showBreweryDetails = function() {
		$scope.showBreweryDataDetails = !$scope.showBreweryDataDetails;
		if ($scope.activeButton == "activeButton") {
			$scope.activeButton = "";
		}
		else if ($scope.activeButton != "activeButton") {
			$scope.activeButton = "activeButton";
		}

	}
	
	// GET company (brewery) - Alle Daten der vorhandenen Unternehmen holen
	$http({
		method: "get",
		url: "http://localhost:3000/api/company/brewery",
	}).then(function(response) {
		$scope.brewerys = response.data;
		$log.info(response);	

		/* Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.brewerys));
		*/
		
	}, function (response) {
		alert("Error " + response.data);
	});	
	
	
	// Seite über Aktualisieren-Button neu laden
	$scope.getBrewerys = function() {
		$http({
			method: "get",
			url: "http://localhost:3000/api/company/brewery",
		}).then(function(response) {
			$scope.brewerys = response.data;
			$log.info(response);	

		/* Testing JSON Input
		alert("Eingabe JSON company: " + JSON.stringify($scope.brewerys));
		*/
		
		}, function (response) {
			alert("Error " + response.data);
		});	
		
	}
	
	
	// POST company (brewery) - Unternehmen anlegen
	$scope.newBrewery = {};
	$scope.postNewBrewery = function() {
        $http({
            method: "post",
            url: "http://localhost:3000/api/company",
            data: {
				// Daten für neues Unternehmen
                companyType: "brewery",
				companySize: $scope.newBrewery.companySize,
				qualityType: $scope.newBrewery.qualityType,
				budget: $scope.newBrewery.budget,
				warehousePercentage: $scope.newBrewery.warehousePercentage,
				profitMargin: $scope.newBrewery.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
				
				// Neuen Datensatz in der Übersicht anzeigen - Kennzeichnen als "neu" da noch ohne CounterID
				$scope.brewerys.companies.push( {
					companySize: $scope.newBrewery.companySize,
					qualityType: $scope.newBrewery.qualityType,
					budget: $scope.newBrewery.budget,
					balance: "0",
					currentStock: "0",
					warehousePercentage: $scope.newBrewery.warehousePercentage,
					profitMargin: $scope.newBrewery.profitMargin, 
					companyCounter : "neu", 
				});	
			
				// Objekt für Eingabeformular wieder leeren
				$scope.newBrewery = {};
			
			
		}, function (response) {
			alert("Error " + response.data);
			
			/* Testing JSON Output
			var testData = {
				companyType: "brewery",
				companySize: $scope.newBrewery.companySize,
				qualityType: $scope.newBrewery.qualityType,
				budget: $scope.newBrewery.budget,
				warehousePercentage: $scope.newBrewery.warehousePercentage,
				profitMargin: $scope.newBrewery.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
	
	// PUT company (brewery) - Felder: Kontinuerlicher Beschaffungsgrad, Gewinnaufschlag des Unternehmens updaten
	$scope.updateBrewery = function(brewery) {
		$scope.breweryData = brewery;
        $http({
            method: "put",
            url: "http://localhost:3000/api/company",
            data: {
				// Daten für bestehendes Unternehmen updaten
				companyId: $scope.breweryData.companyId,
                companyType: "brewery",
				warehousePercentage: $scope.breweryData.warehousePercentage,
				profitMargin: $scope.breweryData.profitMargin,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
		}, function (response) {
			alert("Error " + response.data);
			
			/* Testing JSON Output */
			var testData = {
				companyId: $scope.breweryData.companyId,
                companyType: "brewery",
				warehousePercentage: $scope.breweryData.warehousePercentage,
				profitMargin: $scope.breweryData.profitMargin,
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			
		});
	}
	

	// DELETE company (brewery) - Unternehmen löschen
	$scope.deleteBrewery = function(brewery) {
		$scope.breweryData = brewery;
		$http({
			method: "delete",
			url: "http://localhost:3000/api/company",
			data: {
				// Unternehmen löschen
				companyId: $scope.breweryData.companyId,
                companyType: "brewery",
			},
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
				// Datensatz aus der Übersicht entfernen
				var index = $scope.brewerys.companies.indexOf(brewery);
				$scope.brewerys.companies.splice(index, 1);

		}, function (response) {
			alert("Error " + response.data);

			/* Testing JSON Output
			var testData = {
				companyId: $scope.breweryData.companyId,
                companyType: "brewery",
			}
			alert("Ausgabe JSON company: " + JSON.stringify(testData));
			*/
		});
	}
	
});



//////////////////////////////
// Consumer
//////////////////////////////
app.controller('consumerController', function($scope, $http, $log, $location, $window) {
	
	// Status bearbeiten oder speichern setzten
	$scope.initDemand = function(){
		$scope.demandEditStatus = false;
	}
	
	
	// Nachfragemenge bearbeiten und speichern durch Buttons
	$scope.changeDemandConsumer = function(){
		$scope.demandEditStatus = !$scope.demandEditStatus;
	}
	
	
	// Felder zum Bearbeiten freigeben
	$scope.enableConsumerDemandEdit = function(){
		$scope.consumerDemandEdit = false;
	}
	
	
	// Felder zum Bearbeiten und Sperren
	$scope.disableConsumerDemandEdit = function(){
		$scope.consumerDemandEdit = true;
	}
	
	
	// GET dataOverview - Aktuelle Nachfragemengen (demand) aus dataOverview holen
	$http({
		method: "get",
		url: "http://localhost:3000/api/dataOverview",
	}).then(function(response) {
		$scope.overview = response.data;
		$log.info(response);
		
		// parseInt - Wichtig für Berechnungen im Frontend
		$scope.demandQ1 = parseInt(response.data.consumerDemand.qualityLow);
		$scope.demandQ2 = parseInt(response.data.consumerDemand.qualityMedium);
		$scope.demandQ3 = parseInt(response.data.consumerDemand.qualityHigh);

		/* Testing JSON Input
		alert("Eingabe JSON dataOverview: " + JSON.stringify($scope.overview));
		*/
		
	}, function (response) {
		alert("Error " + response.data);
	});	
	
	
	// PUT demand - Aktualisierte Nachfragemengen senden
	$scope.postConsumerDemand = function() {
        $http({
            method: "put",
            url: "http://localhost:3000/api/demand",
            data: {
				// Nachfragemengen für alle 3 Qualitätsstufen updaten in Liter
                quality1: $scope.demandQ1,
				quality2: $scope.demandQ2,
				quality3: $scope.demandQ3,
            },
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {
			if (response.data)
				$log.info(response);
			
		}, function (response) {
			alert("Error " + response.data);
			
			/* Testing JSON Output
			var testData = {
				quality1: $scope.demandQ1,
				quality2: $scope.demandQ2,
				quality3: $scope.demandQ3,
			}
			alert("Ausgabe JSON demand: " + JSON.stringify(testData));
			*/
		});
	}
	
});