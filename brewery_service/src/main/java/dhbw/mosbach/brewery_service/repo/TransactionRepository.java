package dhbw.mosbach.brewery_service.repo;

import dhbw.mosbach.brewery_service.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TransactionRepository extends CustomRepository<Transaction, UUID> {
}
