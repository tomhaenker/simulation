package dhbw.mosbach.brewery_service.repo;

import dhbw.mosbach.brewery_service.Brewery;
import dhbw.mosbach.brewery_service.enums.QualityLevel;
import dhbw.mosbach.brewery_service.repo.CustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import javax.persistence.Cacheable;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

@Repository @Cacheable(value = false)
public interface BreweryRepository extends CustomRepository<Brewery, UUID> {

    List<Brewery> findAllByQualityLevel(QualityLevel qualityLevel);
}
