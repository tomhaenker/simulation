package dhbw.mosbach.brewery_service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dhbw.mosbach.brewery_service.dto.ConfirmOrderDTO;
import dhbw.mosbach.brewery_service.dto.OfferDTO;
import dhbw.mosbach.brewery_service.dto.OrderDTO;
import dhbw.mosbach.brewery_service.enums.Ressource;
import dhbw.mosbach.brewery_service.enums.TransactionType;
import dhbw.mosbach.brewery_service.repo.BreweryRepository;
import dhbw.mosbach.brewery_service.repo.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import java.lang.Exception;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.*;

@Component
@Transactional
public class DayThread extends Thread{
    @Value("${simulation.waterplant.url}") private String waterplantPath;
    @Value("${simulation.hopfarmer.url}") private String hopfarmerPath;
    @Value("${simulation.malter.url}") private String malterPath;
    private final BreweryRepository breweryRepository;
    private final TransactionRepository transactionRepository;
    private int day;
    Logger logger = LoggerFactory.getLogger(DayThread.class);

    public DayThread(BreweryRepository breweryRepository, TransactionRepository transactionRepository) {
        this.breweryRepository = breweryRepository;
        this.transactionRepository = transactionRepository;
    }

    public void start(Brewery brewery, int day) {
        this.day = day;

        /*
        zufällige Wartezeit stellt sicher, dass nicht immer die selben Unternehmen Rohstoffe einkaufen können
         */
        Random r = new Random();
        try {
            sleep(r.nextInt(250));
        }
        catch (Exception e){
            e.printStackTrace();
        }

        /*
        Täglich anfallende Kosten verbuchen
         */
        brewery.bookDailyCosts();
        breweryRepository.save(brewery);


        /*
        Verkauf:
        Beim Start des Tages wird im Controller ein zwischenstand gezogen mit dem die Nachfrage bedient wird.
        Änderungen an Bestand und Preisen können daher nun direkt am Objekt vorgenommen werden
         */

        /*
        Produktion
        Sämtlich Logik ist im Unternehmen vorhanden
         */
        brewery.produceBeer();
        breweryRepository.save(brewery);


        /*
        Nachkauf
        Zielmenge beim Einkauf ist immer ein volles Lager
         */

        order(brewery, Ressource.HOP);
        order(brewery, Ressource.MALT);
        order(brewery, Ressource.WATER);


        /*
        prüfen ob Unternehmen noch liquide ist
        Wenn nicht --> Aus dem Markt entfernen
         */
        if (brewery.getBudget().compareTo(new BigDecimal(0)) < 0){
            breweryRepository.delete(brewery);
            logger.info("Delete: " + brewery);
        }
        logger.info("Brewery: " + brewery.getId() + "\nTag Abgeschlossen");

    }

    /**
     * Stößt den Kaufprozess für ein Unternehmen und die übergebenen Rohstoffe an
     * @param brewery Unternehmen für welches Rohstoffe gekauft werden sollen
     * @param resources Rohstoffe die eingekauft werden sollen
     */
    private void order(Brewery brewery, Ressource resources){
        String path = "";
        int neededAmount = 0;
        switch (resources){
            case HOP:
                path = hopfarmerPath;
                neededAmount = brewery.getMax_amount_hops() - brewery.getAmount_hops();
                break;
            case MALT:
                path = malterPath;
                neededAmount = brewery.getMax_amount_malt() - brewery.getAmount_malt();
                break;
            case WATER:
                path = waterplantPath;
                neededAmount = brewery.getMax_amount_water() - brewery.getAmount_water();
                break;
            default:
                return;
        }
        List<Offer> offers = requestResources(path, brewery);
        for (int counter = 0; counter < offers.size(); counter++){
            ConfirmOrderDTO confirmation = null;
            if (neededAmount == 0) return; //Wenn Bedarf gedeckt ist ist der Beschaffungsprozess abgeschlossen
            if (offers.get(counter).quantity == 0) continue;
            try {
                int orderAmount = Math.min(offers.get(counter).quantity, neededAmount);
                confirmation = sendHttpOrder(path, orderAmount, offers.get(counter).id);
            } catch (Exception e){
                offers = requestResources(path, brewery);
                counter = 0;
            }
            if (confirmation != null){
                neededAmount = neededAmount - confirmation.quantity;
                transactionRepository.save(
                        new Transaction(
                                TransactionType.BUY,
                                day,
                                offers.get(counter).id.toString(), //sellerID
                                brewery.getId().toString(), //buyerID
                                brewery.getQualityLevel(),
                                confirmation.quantity,
                                BigDecimal.valueOf(confirmation.unitPrice),
                                resources));
                BigDecimal priceInEuro = new BigDecimal(0);
                switch (Params.inferfaceMoneyUnit){
                    case EURO: priceInEuro = BigDecimal.valueOf(confirmation.unitPrice); break;
                    case CENT: priceInEuro = BigDecimal.valueOf(confirmation.unitPrice).movePointLeft(2); break;
                    default: throw new IllegalArgumentException("Unknown moneyunit");
                }
                brewery.buyRessources(confirmation.quantity, priceInEuro, resources);
                breweryRepository.save(brewery);
            }
        }
    }

    /**
     * Anfrage bei Lieferanten nach Rohstoffen einer bestimmten Qualität
     * @return sortierte Liste aller Angebote
     */
    private List<Offer> requestResources(String uri, Brewery brewery){
        ObjectMapper mapper = new ObjectMapper();
        uri = uri + "/api/request/" + brewery.getQualityLevel();
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List> response = null;
        int errorCounter = 0;
        Random random = new Random();
//        Kombination aus Schleife und try-catch dient zum Abfangen vom DayNotStarted Error
//        Error tritt auf, wenn der Consumer service eine Anfrage sendet, bevor der Tag beim BreweryService begonnen hat
        do {
            if (errorCounter++ == 10) {
                logger.info("\nBrauerei: " + brewery.getId() +
                        "\nZielservice: " + uri +
                        "\nAnfrage abgebrochen, Keine Daten übermittelt\n");
                return new ArrayList<>(); // nach 25 Fehlversuchen wird eine leere Liste zurückgegeben
            }
            try{
                response = restTemplate.exchange(uri, HttpMethod.GET, null, List.class);
                sleep((1 + random.nextInt(5)) * 100); // Falls Fehler auftreten soll kurz pausiert werden
            } catch (ResourceAccessException e){
//                Wenn Service nicht verfogbar ist wird auch keine erneute Anfrage abgesendet
                logger.error("Server nicht erreichbar\n" + e.getMessage());
                return new ArrayList<>();
            } catch (Exception e){
                if (!e.getMessage().contains("Start day before request data")){
                    logger.error(e.getMessage());
                } else{
                    logger.info("Start day at" + uri);
                }
            }
        } while (response == null);
        List<OfferDTO> offerDTOs = mapper.convertValue(response.getBody(), new TypeReference<List<OfferDTO>>(){});
        offerDTOs.sort(Comparator.comparing(OfferDTO::getUnitPrice));
        List<Offer> offers = new ArrayList<>();
        for (OfferDTO offerdto: offerDTOs) {
            offers.add(Offer.createFromDTO(offerdto, Params.inferfaceMoneyUnit));
        }
        return offers;
    }

    /**
     * Sendet einen Einkaufsbefehl an einen anderen Microservice
     * @param uri Adresse des Microservice
     * @param neededAmount Benötigte Menge
     * @param id UUID des verkaufenden Unternehmens
     * @return Bestätigung des Einkaufs
     */
    private ConfirmOrderDTO sendHttpOrder(String uri, int neededAmount, UUID id){
        OrderDTO order = new OrderDTO(id, neededAmount);
        uri = uri + "/api/order/";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<OrderDTO> entity = new HttpEntity<>(order, headers);
        ResponseEntity<ConfirmOrderDTO> response = null;
        try {
            response = restTemplate.exchange(uri, HttpMethod.POST, entity, ConfirmOrderDTO.class);
        } catch (Exception e){
            logger.error("Fehler beim Aufruf von " + uri + "\nFehlercode: " + e.getMessage());
            return null; // null signalisiert einen Fehler im Kaufprozess
        }
        return response.getBody();
    }



}


