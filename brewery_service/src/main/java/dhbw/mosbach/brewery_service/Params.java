package dhbw.mosbach.brewery_service;

import dhbw.mosbach.brewery_service.enums.CompanySize;
import dhbw.mosbach.brewery_service.enums.MoneyUnit;
import dhbw.mosbach.brewery_service.enums.QualityLevel;

import java.math.BigDecimal;

/**
 * Klasse bündelt unternehmensübergreifende Daten
 * Daten könnten auch für andere Unternehmensdaten genutzt werden.
 * Ziel ist diese Daten über den Core bereitzustellen
 */
public class Params {
    public static final int dataBasePrecision = 19; //Precision = Gesamtlänger relevanter Zahlen
    public static final int dataBaseScale = 10; //Scale = Nachkommastellen
    public static final String bigDecimalColumDefinition = "DECIMAL(" + dataBasePrecision + "," + dataBaseScale + ")";
    public static final int roundingScale = dataBaseScale;

    public static final BigDecimal boost = new BigDecimal("2");
    public static BigDecimal storageCostPerKg = new BigDecimal("0.15");
    public static BigDecimal inputStorageSizeModifier = BigDecimal.valueOf(5); //zB. 5 bedeutet, dass das Inputlager Waren fasst um fünf Tage ohne Nachkauf produzieren zu können

    public static final MoneyUnit inferfaceMoneyUnit = MoneyUnit.EURO;

    /**
     * Lagergröße ist abhängig zur Unternehmensgröße
     * @param size Unternehmensgröße
     * @return Lagergröße
     */
    public static int getMaxiumumStorage(CompanySize size){
        switch (size){
            case SMALL: return 5_000;
            case MEDIUM: return 20_000;
            case BIG: return 50_000;
        }
        throw new IllegalArgumentException("Wrong size");
    }

    /**
     * Produktionsgeschwindigkeit ist abhängig zur produzierten Qualität
     * @param qualityLevel Produktqualität des Unternehmens
     * @return Bremsfaktor, der dazu dient die Produktionsmenge zu reduzieren
     */
    public static BigDecimal getBreakingFactor(QualityLevel qualityLevel) {
        switch (qualityLevel){
            case LOW: return new BigDecimal(1);
            case MEDIUM: return new BigDecimal("0.5");
            case HIGH: return new BigDecimal("0.25");
        }
        throw new IllegalArgumentException("Wrong Qualitylevel");
    }

    /**
     * Produktionskosten sind abhängig zur produzierten Qualität
     * @param qualityLevel Produktqualität des Unternehmens
     * @return Kostenfaktor der bei der Produktion verrechnet wird
     */
    public static BigDecimal getCostFactor(QualityLevel qualityLevel) {
        switch (qualityLevel){
            case LOW: return new BigDecimal("0.1");
            case MEDIUM: return new BigDecimal("0.2");
            case HIGH: return new BigDecimal("0.3");
        }
        throw new IllegalArgumentException("Wrong Qualitylevel");
    }

    /**
     * Betriebskosten sind abhängig zur produzierten Qualität
     * @param qualityLevel Produktqualität des Unternehmens
     * @return Betriebskosten des Unternehmens
     */
    public static BigDecimal getDailyCost(QualityLevel qualityLevel){
        switch (qualityLevel){
            case LOW: return new BigDecimal(20);
            case MEDIUM: return new BigDecimal(100);
            case HIGH: return new BigDecimal(200);
        }
        throw new IllegalArgumentException("Wrong Qualitlevel");
    }

}

