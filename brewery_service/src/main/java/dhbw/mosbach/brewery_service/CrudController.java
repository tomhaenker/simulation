package dhbw.mosbach.brewery_service;

import dhbw.mosbach.brewery_service.dto.BreweryGetDTO;
import dhbw.mosbach.brewery_service.dto.BreweryPostDTO;
import dhbw.mosbach.brewery_service.dto.BreweryPutDTO;
import dhbw.mosbach.brewery_service.repo.BreweryRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
public class CrudController {

    final
    BreweryRepository breweryRepository;

    public CrudController(BreweryRepository breweryRepository) {
        this.breweryRepository = breweryRepository;
    }

    /**
     * Erstellt eine neue Brauerei
     * @param breweryPostDTO BreweryPostDTO mit allen relevanten Daten zur Unternehmenserstellung
     */
    @PostMapping("/api/company")
    public void createBrewery(@RequestBody BreweryPostDTO breweryPostDTO){
        if (breweryPostDTO.warehousePercentage > 100) throw new IllegalArgumentException("warehousePercentage more than 1ßß%");
        breweryRepository.save(new Brewery(breweryPostDTO));
    }

    /**
     * Liefert das Datenbankobjekt einer Brauerei zurück
     * @param id ID anhand welcher in der DB gesucht wird
     * @return Brewery wie in der DB gespeichert
     */
    @GetMapping("/api/company/{id}")
    public BreweryGetDTO getBrewery(@PathVariable UUID id){
        Optional<Brewery> brewery = breweryRepository.findById(id);
        if (brewery.isEmpty()) throw new IllegalArgumentException("Wrong CompanyID");
        return new BreweryGetDTO(brewery.get());
    }

    /**
     * Ändert Werte einer in der Datenbank gespeicherten Brauerei
     * @param id ANhand dieser wird die Brauerei bestimmt und ausgewählt
     * @param breweryPutDTO Daten welche geändert werden sollen
     * @return Gibt geänderte Brauerei zurück
     */
    @PutMapping("/api/company/{id}")
    public BreweryGetDTO updateBrewery(@PathVariable UUID id, @RequestBody BreweryPutDTO breweryPutDTO){
        Optional<Brewery> optionalBrewery = breweryRepository.findById(id);
        if (optionalBrewery.isEmpty()) throw new IllegalArgumentException("Wrong CompanyID");
        Brewery brewery = optionalBrewery.get();
        brewery.updateTargetAmount(breweryPutDTO.getWarehousePercentage());
        brewery.setProfit_margin(breweryPutDTO.getProfitMargin());
        breweryRepository.save(brewery);
        return new BreweryGetDTO(brewery);
    }

    /**
     * Löscht ein Unternehmen
     * @param id UUID des Unternehmens welches gelöscht werden soll
     */
    @DeleteMapping("/api/company/{id}")
    public void deleteCompany(@PathVariable UUID id){
        if (breweryRepository.findById(id).isEmpty()) return;
        breweryRepository.deleteById(id);
    }

}
