package dhbw.mosbach.brewery_service;

import dhbw.mosbach.brewery_service.dto.OfferDTO;
import dhbw.mosbach.brewery_service.enums.MoneyUnit;
import dhbw.mosbach.brewery_service.enums.QualityLevel;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Klasse bündelt die Logik für das Aufbereiten von Angeboten bevor oder nachdem diese über die Schnittstellen ausgetauscht werden
 */
public class Offer{
    public UUID id;
    public int quantity;
    private BigDecimal unitPrice; //in €

    public Offer() {
    }

    public Offer(UUID id, int quantity, BigDecimal unitPrice) {
        this.id = id;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    public static Offer createFromBrewery(Brewery brewery){
        return new Offer(brewery.getId(), brewery.getAmount_beer(), brewery.calcSellPrice());
    }

    public OfferDTO createDTO(MoneyUnit unit){
        return new OfferDTO(id, quantity, getUnitPrice(unit).doubleValue());
    }

    public static Offer createFromDTO(OfferDTO offerDTO, MoneyUnit unit){
        switch (unit){
            case EURO: return new Offer(offerDTO.id, offerDTO.quantity, BigDecimal.valueOf(offerDTO.unitPrice));
            case CENT: return new Offer(offerDTO.id, offerDTO.quantity, BigDecimal.valueOf(offerDTO.unitPrice).movePointLeft(2));
        }
        throw new IllegalArgumentException("Unknown moneyunit");
    }

    public BigDecimal getUnitPrice(MoneyUnit unit) {
        switch (unit){
            case EURO: return unitPrice;
            case CENT: return unitPrice.movePointRight(2);
        }
        throw new IllegalArgumentException("Unknown moneyunit");
    }
}

