package dhbw.mosbach.brewery_service;

import dhbw.mosbach.brewery_service.dto.*;
import dhbw.mosbach.brewery_service.enums.MoneyUnit;
import dhbw.mosbach.brewery_service.enums.QualityLevel;
import dhbw.mosbach.brewery_service.enums.Ressource;
import dhbw.mosbach.brewery_service.enums.TransactionType;
import dhbw.mosbach.brewery_service.repo.BreweryRepository;
import dhbw.mosbach.brewery_service.repo.TransactionRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
@Transactional
public class MainController {

    public BreweryRepository breweryRepository;
    final ApplicationContext applicationContext;
    TransactionRepository transactionRepository;
    public static List<Offer> availableOffers = new ArrayList<>();
    private int day = 0;
    boolean dayRunning = false;

    public MainController(BreweryRepository breweryRepository, ApplicationContext applicationContext, TransactionRepository transactionRepository) {
        this.breweryRepository = breweryRepository;
        this.applicationContext = applicationContext;
        this.transactionRepository = transactionRepository;

//        hier wird der Unternehmenscounter mit der aktuell höchsten ID befüllt
        int highestNumericalID = 0;
        for (Brewery brewery: breweryRepository.findAll()) {
            if (brewery.getNumericalId() > highestNumericalID) highestNumericalID = brewery.getNumericalId();
        }
        Brewery.resetCounter(highestNumericalID);
    }

    /**
     * Startet einen neuen Tag
     * @throws InterruptedException
     */
    @PostMapping("/api/startDay")
    public void startDay(@RequestBody DayCountDTO dayCount) throws InterruptedException {
        dayRunning = true;
        List<DayThread> runningDays = new ArrayList<>();
        day = dayCount.dayCount;
        availableOffers.clear(); // Angebote des Vortages werden gelöscht
        List<Brewery> breweries = breweryRepository.findAll();
//        Angebote werden vor Produktion erstellt.
//        So wird sichergestellt, dass keine Waren angebote bzw. verkauft werden, die am selben Tag produziert wurden
        for (Brewery brewery : breweries) {
            availableOffers.add(
                    Offer.createFromBrewery(brewery));
        }
        for (Brewery brewery : breweries) {
//            für jedes Unternehmen wird ein separater Thread zur Simulation geöffnet
            DayThread dayThread = new DayThread(breweryRepository, transactionRepository);
            applicationContext.getAutowireCapableBeanFactory().autowireBean(dayThread);
            dayThread.start(brewery, day);
            runningDays.add(dayThread);
        }
        for (DayThread daythread : runningDays) {
//            es wird nacheinander geprüft ob jedes Unternehmen fertig berechnet wurde
            daythread.join();
        }
//        Wenn alle Unternehmen simuliert wurden gilt die Methode als abgeschlossen
//        Erst jetzt wird durch den HTTP Status Code 200 signalisiert, dass der Tag fertig simuliert wurde
        dayRunning = false;
    }

    /**
     * Liefert alle Angebote einer Qualität zurück
     * @param qualityLevel Angefragte Qualität
     * @return Liste aller Unternehmen die Bier dieser Qualität liefern können
     */
    @GetMapping("/api/request/{qualityLevel}")
    public List<OfferDTO> receiveRequest(
            @PathVariable QualityLevel qualityLevel) {
        List<OfferDTO> matchingOffers = new ArrayList<>();
        int errorCounter = 0;
        long breweryAmount = breweryRepository.count();
        while (availableOffers.size() == 0 && breweryAmount > 0){ //Wenn keine Brauereien angelegt sind kann es auch keine Angebote geben
            if (errorCounter++ == 100) {
                System.out.println("Anfrage konnte nicht beantwortet werden");
                return matchingOffers; //nach hundert Fehlversuchen wird eine leere Liste zurückgegeben (100 Fehlschläge => 10 Sekunden)
            }
            try {
                Thread.sleep(100);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        for (Offer offer: availableOffers){
            if (breweryRepository.findById(offer.id).isEmpty()) continue;
            Brewery brewery = breweryRepository.findById(offer.id).get();
            if (brewery.getQualityLevel().equals(qualityLevel) && offer.quantity > 0){
                matchingOffers.add(offer.createDTO(Params.inferfaceMoneyUnit));
            }
        }
        return matchingOffers;
    }

    /**
     * Führt eine Bestellung aus
     * @param order Bestelldetails (VerkäuferID und Menge)
     * @return Bestellbestätigung
     */
    @PostMapping("/api/order")
    public ConfirmOrderDTO receiveOrder(
            @RequestBody OrderDTO order) {
//        Kauf findet erst statt, wenn der Tag fertig berechnet wurde
//        Anderern Falls kann die Lost-Update Problematik auftreten
        while (dayRunning){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (breweryRepository.findById(order.companyId).isEmpty())
            throw new IllegalArgumentException("Company does not exist");
        Brewery brewery = breweryRepository.findById(order.companyId).get();
//        Der Bestand vom Bier muss in den Offers geprüft werden. Stellt sicher, dass der Bestand zu Beginn des Tages geprüft wird
        for (Offer offer : availableOffers) {
            if (offer.id.equals(order.companyId)){
                if (offer.quantity < order.quantity) throw new IllegalArgumentException("Not enough goods available");
                else{
                    offer.quantity = offer.quantity - order.quantity; // Update der heutigen Daten
                    brewery.sellBeer(order.quantity, offer.getUnitPrice(MoneyUnit.EURO)); // Update der Gesamtdaten
                    breweryRepository.saveAndFlush(brewery);
                    breweryRepository.refresh(brewery);
                    ConfirmOrderDTO confirmation = new ConfirmOrderDTO(brewery.getId(), order.quantity, offer.getUnitPrice(Params.inferfaceMoneyUnit).doubleValue());
                    transactionRepository.save(new Transaction(
                                    TransactionType.SELL,
                                    day,
                                    brewery.getId().toString(), //sellerID
                                    "Consumer Service", //buyerID
                                    brewery.getQualityLevel(),
                                    confirmation.quantity,
                                    BigDecimal.valueOf(confirmation.unitPrice),
                                    Ressource.BEER));
                    return confirmation;
                }
            }
        }
        throw new IllegalArgumentException("Something went wrong");
    }

    /**
     * Liefert detaillierte Daten der vorhandenen Unternehmen zurück
     * @return Brauereidaten
     */
    @GetMapping("/api/company")
    public DataDTO getAllCompanyData(){
        List<Brewery> breweries =  breweryRepository.findAll();
        List<BreweryGetDTO> dtoList = new ArrayList<>();
        for (Brewery brewery: breweries) {
            dtoList.add(new BreweryGetDTO(brewery));
        }
        return new DataDTO(dtoList);
    }

    /**
     * Liefert zusammengefasste Daten der vorhandenen Unternehmen zurück
     * @return Brauereidaten
     */
    @GetMapping("/api/dataOverview")
    public OverviewDTO getAllCompanyOverview(){
        List<Brewery> breweries =  breweryRepository.findAll();
        OverviewQualityDetailDTO qualityLow = new OverviewQualityDetailDTO();
        OverviewQualityDetailDTO qualityMiddle = new OverviewQualityDetailDTO();
        OverviewQualityDetailDTO qualityHigh = new OverviewQualityDetailDTO();
        for (Brewery brewery: breweries) {
            switch (brewery.getQualityLevel()){
                case LOW: qualityLow.addCompany(brewery); break;
                case MEDIUM: qualityMiddle.addCompany(brewery); break;
                case HIGH: qualityHigh.addCompany(brewery); break;
            }
        }
        return new OverviewDTO(qualityLow, qualityMiddle, qualityHigh);
    }

    /**
     * Erstellt Unternehmen mit zufälligen Parametern
     */

    @PostMapping("/api/randomCompany")
    public void createRandomBrewery(){
        breweryRepository.save(Brewery.createRandom());
    }

    /**
     * Wird aufgerufen um alle Daten zu löschen und bevor eine neue Simulation gestartet wird
     */
    @PostMapping("/api/endSimulation")
    public void deleteData(){
        if (breweryRepository.count() > 0) breweryRepository.deleteAllInBatch();
        if (transactionRepository.count() > 0) transactionRepository.deleteAllInBatch();
        Brewery.resetCounter();
    }
}
