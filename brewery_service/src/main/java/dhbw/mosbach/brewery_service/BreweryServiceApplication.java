package dhbw.mosbach.brewery_service;

import dhbw.mosbach.brewery_service.repo.CustomRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(repositoryBaseClass = CustomRepositoryImpl.class)
public class BreweryServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(BreweryServiceApplication.class, args);
    }

}
