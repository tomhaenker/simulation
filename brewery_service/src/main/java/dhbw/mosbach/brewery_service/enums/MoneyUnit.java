package dhbw.mosbach.brewery_service.enums;

public enum MoneyUnit {
    EURO, CENT
}
