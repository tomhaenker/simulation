package dhbw.mosbach.brewery_service.enums;

public enum Ressource {
    WATER, HOP, MALT, BEER
}
