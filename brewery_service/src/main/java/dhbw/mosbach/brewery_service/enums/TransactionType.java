package dhbw.mosbach.brewery_service.enums;

public enum TransactionType {
    BUY, SELL
}
