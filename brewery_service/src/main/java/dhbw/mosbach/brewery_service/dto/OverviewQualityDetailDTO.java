package dhbw.mosbach.brewery_service.dto;

import dhbw.mosbach.brewery_service.Brewery;

public class OverviewQualityDetailDTO {
    private int count;
    private int balance;
    private int amount;

    public void addCompany(Brewery brewery){
        count++;
        balance = balance + brewery.getBudget().subtract(brewery.getStartBudget()).movePointRight(2).intValue();
        amount = amount + brewery.getAmount_beer();
    }

    public int getCount() {
        return count;
    }

    public int getBalance() {
        return balance;
    }

    public int getAmount() {
        return amount;
    }
}
