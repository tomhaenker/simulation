package dhbw.mosbach.brewery_service.dto;

public class BreweryPostDTO {
    public String companySize;
    public String qualityType;
    public int budget;
    public int warehousePercentage;
    public int profitMargin;
}
