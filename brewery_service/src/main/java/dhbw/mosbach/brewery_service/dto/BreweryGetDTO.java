package dhbw.mosbach.brewery_service.dto;

import dhbw.mosbach.brewery_service.Brewery;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

public class BreweryGetDTO {
    public UUID companyId;
    public int companyIdNumeric;
    public String companySize;
    public String qualityType;
    public int budget;
    public int balance; // steht für die Differenz zwischen Startbudget und aktuellem
    public int currentStock;
    public int warehousePercentage;
    public int profitMargin;

    public BreweryGetDTO(Brewery brewery) {
        this.companyId = brewery.getId();
        this.qualityType = brewery.getQualityLevel().toString();
        this.budget = brewery.getBudget().movePointRight(2).intValue();
        this.balance = brewery.getBudget().subtract(brewery.getStartBudget()).movePointRight(2).intValue();
        this.warehousePercentage = new BigDecimal(brewery.getTarget_amount_beer()).divide(new BigDecimal(brewery.getMax_amount_beer()), 10, RoundingMode.DOWN).movePointRight(2).intValue(); //Wert ist immer ein Ganzzahl
        this.profitMargin = brewery.getProfit_margin().movePointRight(2).intValue();
        this.currentStock = brewery.getAmount_beer();
        this.companySize = brewery.getSize().toString();
        this.companyIdNumeric = brewery.getNumericalId();
    }
}
