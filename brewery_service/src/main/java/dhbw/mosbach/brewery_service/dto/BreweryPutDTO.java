package dhbw.mosbach.brewery_service.dto;

import java.math.BigDecimal;

public class BreweryPutDTO {
    public int warehousePercentage;
    public int profitMargin;

    public BigDecimal getWarehousePercentage() {
        return new BigDecimal(warehousePercentage).movePointLeft(2);
    }

    public BigDecimal getProfitMargin() {
        return new BigDecimal(profitMargin).movePointLeft(2);
    }
}
