package dhbw.mosbach.brewery_service.dto;

import java.util.UUID;

public class OfferDTO implements Comparable<OfferDTO>{
    public UUID id;
    public int quantity;
    public double unitPrice;

    public OfferDTO() { }

    /**
     * Erezugt ein OfferDTO.
     * Vorsicht: Erwartet cent
     * @param id uuid des Unternehmens
     * @param quantity Menge in Liter
     * @param unitPrice Preis pro Liter
     */
    public OfferDTO(UUID id, int quantity, double unitPrice) {
        this.id = id;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    @Override
    public int compareTo(OfferDTO o) {
        return this.id.compareTo(o.id);
    }
}
