package dhbw.mosbach.brewery_service.dto;

import java.util.ArrayList;
import java.util.List;

public class DataDTO {
    public List<BreweryGetDTO> companies = new ArrayList<>();

    public DataDTO(List<BreweryGetDTO> companies) {
        this.companies = companies;
    }
}
