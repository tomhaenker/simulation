package dhbw.mosbach.brewery_service;

import dhbw.mosbach.brewery_service.dto.BreweryPostDTO;
import dhbw.mosbach.brewery_service.enums.CompanySize;
import dhbw.mosbach.brewery_service.enums.QualityLevel;
import dhbw.mosbach.brewery_service.enums.Ressource;
import org.hibernate.annotations.Check;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Entity @Cacheable(value = false)
public class Brewery {
    public static final int default_beerProduceRate = 1_000;
    public static final int default_waterConsumeRate = 1_500;
    public static final int default_maltConsumeRate = 500;
    public static final int default_hopConsumeRate = 200;
    private static final AtomicInteger count = new AtomicInteger(0);
    @Id @Type(type="uuid-char")
    private UUID id = UUID.randomUUID();
    private int numericalId;
    @Version
    private long version;
    private BigDecimal budget; //in €
    private BigDecimal startBudget; //in €
    private int amount_water; //in l
    private int amount_hops; //in kg
    private int amount_malt; //in kg
    private int amount_beer; //in l
    @Column(columnDefinition=Params.bigDecimalColumDefinition)
    private BigDecimal value_water_unit = new BigDecimal(0);
    @Column(columnDefinition=Params.bigDecimalColumDefinition)
    private BigDecimal value_hop_unit = new BigDecimal(0);
    @Column(columnDefinition=Params.bigDecimalColumDefinition)
    private BigDecimal value_malt_unit = new BigDecimal(0);
    @Column(columnDefinition=Params.bigDecimalColumDefinition)
    private BigDecimal value_beer_unit = new BigDecimal(0); // beim Verkauf muss hier noch die Gewinnspanne draufgerechnet werden
    private int max_amount_water;
    private int max_amount_hops;
    private int max_amount_malt;
    private int max_amount_beer;
    private int target_amount_beer;
    private BigDecimal breaking_factor; // Bremserate in Abhängigkeit zur Qualität
    private BigDecimal cost_factor; // Teuerungsrate bei der Herstellung
    private BigDecimal profit_margin;
    private BigDecimal dailyFixCosts;
    private CompanySize size;
    private QualityLevel qualityLevel;

    /**
     * @return Gibt ein Unternehmen mit zufälligen Parameter zurück
     */
    public static Brewery createRandom(){
        Random random = new Random();
        CompanySize size = CompanySize.getComanySize(random.nextInt(3) + 1);
        QualityLevel quality = QualityLevel.getQualityLevel(random.nextInt(3) + 1);
        BigDecimal budget = BigDecimal.valueOf(random.nextInt(100_000)+100_000);
        BigDecimal targetPercentage = BigDecimal.valueOf(random.nextInt(50)+ 50).movePointLeft(2);
        BigDecimal profit_margin = BigDecimal.valueOf(random.nextInt(40)+ 10).movePointLeft(2);
        return new Brewery(budget, size, quality, targetPercentage, profit_margin);
    }

    /**
     * Erzeugt eine neue Brauerei
     * @param budget in €
     * @param size Unternehmensgröße
     * @param qualityLevel Qualität
     * @param targetPercentage Zielfüllmenge des Lagers
     * @param profit_margin Angestrebte Gewinnspanne
     */
    public Brewery(BigDecimal budget,
                   CompanySize size,
                   QualityLevel qualityLevel,
                   BigDecimal targetPercentage,
                   BigDecimal profit_margin) {
        this.budget = budget;
        this.startBudget = budget;
        int max_storage_size = Params.getMaxiumumStorage(size);
        breaking_factor = Params.getBreakingFactor(qualityLevel);
        // große Unternehmen können doppelt so schnell produzieren
        if (size == CompanySize.BIG) breaking_factor = breaking_factor.multiply(Params.boost);
        this.max_amount_water = breaking_factor.multiply(new BigDecimal(default_waterConsumeRate).multiply(Params.inputStorageSizeModifier)).intValue();
        this.max_amount_hops = breaking_factor.multiply(new BigDecimal(default_hopConsumeRate).multiply(Params.inputStorageSizeModifier)).intValue();
        this.max_amount_malt = breaking_factor.multiply(new BigDecimal(default_waterConsumeRate).multiply(Params.inputStorageSizeModifier)).intValue();
        this.max_amount_beer = max_storage_size;
        target_amount_beer = targetPercentage.multiply(new BigDecimal(max_amount_beer)).intValue();
        this.size = size;
        this.qualityLevel = qualityLevel;


        cost_factor = Params.getCostFactor(qualityLevel);
        this.profit_margin = profit_margin;
        this.dailyFixCosts = Params.getDailyCost(qualityLevel);
        this.numericalId = count.incrementAndGet();
    }

    /**
     * Erzeugt eine Brauerei aus dem übergebenen DTO
     * @param breweryPostDTO Von anderem Microservice übergebenes BreweryDTO
     */
    public Brewery(BreweryPostDTO breweryPostDTO){
        this(
                new BigDecimal(breweryPostDTO.budget).movePointLeft(2),
                CompanySize.getCompanySize(breweryPostDTO.companySize),
                QualityLevel.getQualityLevel(breweryPostDTO.qualityType),
                new BigDecimal(breweryPostDTO.warehousePercentage).movePointLeft(2),
                new BigDecimal(breweryPostDTO.profitMargin).movePointLeft(2)
        );
    }

    /**
     * Bucht eine Bestellung
     * @param buyAmount Menge in kg
     * @param price Preis in €
     * @param resources
     */
    public void buyRessources(int buyAmount, BigDecimal price, Ressource resources){
        int oldAmount;
        BigDecimal oldUnitValue;
        switch (resources){
            case HOP:
                oldAmount = amount_hops;
                oldUnitValue = value_hop_unit;
                break;
            case MALT:
                oldAmount = amount_malt;
                oldUnitValue = value_malt_unit;
                break;
            case WATER:
                oldAmount = amount_water;
                oldUnitValue = value_water_unit;
                break;
            default: throw new IllegalArgumentException("Wrong Ressourcetype");
        }
        BigDecimal[] recalculatedValues = recalcValues(oldAmount, buyAmount, oldUnitValue, price);
        budget = budget.subtract(price.multiply(BigDecimal.valueOf(buyAmount)));
        switch (resources){
            case HOP:
                amount_hops = recalculatedValues[0].toBigInteger().intValue();
                value_hop_unit = recalculatedValues[1];
                break;
            case MALT:
                amount_malt = recalculatedValues[0].toBigInteger().intValue();
                value_malt_unit = recalculatedValues[1];
                break;
            case WATER:
                amount_water = recalculatedValues[0].toBigInteger().intValue();
                value_water_unit = recalculatedValues[1];
                break;
        }
    }

    /**
     * Methode dient zum Starten eines Produktionsdurchgangs.
     * Einmal täglich aufrufen.
     * Sämltiche relevante Prüfungen sind hierin enthalten.
     */
    public void produceBeer(){
//        Bestandsänderungen werden berechnet
        BigDecimal consumed_water = breaking_factor.multiply(new BigDecimal(default_waterConsumeRate));
        BigDecimal consumed_malt = breaking_factor.multiply(new BigDecimal(default_maltConsumeRate));
        BigDecimal consumed_hop = breaking_factor.multiply(new BigDecimal(default_hopConsumeRate));
        BigDecimal produced_beer = breaking_factor.multiply(new BigDecimal(default_beerProduceRate));

//        Bestände werden geprüft
        if (max_amount_beer < (amount_beer + produced_beer.intValue()))return; //Es kann nicht mehr produziert werden als ins Lager passt
        if (target_amount_beer < amount_beer) return; //Es wird nicht mehr wenn die Zielmenge erreicht wurde
        if (amount_water < consumed_water.intValue()) return;
        if (amount_malt < consumed_malt.intValue()) return;
        if (amount_hops < consumed_hop.intValue()) return;

//        Berechnung von Zwischensummen zur Bierpreisermittlung
//        Bierpreis ändert sich somit bei jeder Produktion
        BigDecimal value_consumedGoods =
                consumed_water.multiply(value_water_unit).add(
                consumed_malt.multiply(value_malt_unit)).add(
                consumed_hop.multiply(value_hop_unit));
        BigDecimal cost_production = value_consumedGoods.multiply(cost_factor);
        if(!enoughtBudgetAvailable(cost_production)) return;
        BigDecimal[] recalculatedValues = recalcValues(amount_beer, produced_beer.intValue(), value_beer_unit, cost_production.add(value_consumedGoods).divide(produced_beer, Params.roundingScale, RoundingMode.DOWN));

//        Lagerbestände werden angepasst
        amount_water = amount_water - consumed_water.intValue();
        amount_malt = amount_malt - consumed_malt.intValue();
        amount_hops = amount_hops - consumed_hop.intValue();
        amount_beer = recalculatedValues[0].intValue();
        value_beer_unit = recalculatedValues[1];
        budget = budget.subtract(cost_production);
    }

    /**
     * Preisberechung nach Bestandszuwachs
     * @param oldAmount Menge vor Bestandsänderung
     * @param increaseAmount Menge des Bestandzuwachses
     * @param oldPrice bisheriger Preis pro Einheit
     * @param buyPrice Stückpreis des Zuwachses
     * @return [neue Gesamtmenge, neuer Stückpreis]
     */
    private BigDecimal[] recalcValues(int oldAmount, int increaseAmount, BigDecimal oldPrice, BigDecimal buyPrice){
        int total_amount = oldAmount + increaseAmount;
        BigDecimal oldValue = new BigDecimal(oldAmount).multiply(oldPrice);
        BigDecimal increasingValue = new BigDecimal(increaseAmount).multiply(buyPrice);
        BigDecimal total_value = oldValue.add(increasingValue);
        BigDecimal unit_value =  total_value.divide(new BigDecimal(total_amount), Params.roundingScale, RoundingMode.DOWN);  // Durchschnittlicher Wert einer Einheit im Lager
        BigDecimal[] returnValue = new BigDecimal[2];
        returnValue[0] = new BigDecimal(total_amount);
        returnValue[1] = unit_value;
         return returnValue;
    }

    /**
     * Verkaufspreisberechnung
     * Ist notwendig, da die Gewinnspanne beim Warenwert des Biers berücksichtig wird
     * @return Verkaufspreis pro Liter Bier
     */
    public BigDecimal calcSellPrice(){
        return value_beer_unit.multiply(profit_margin.add(new BigDecimal(1)));
    }

    /**
     * Verbucht die Änderungen die durch einen Verkauf entstehen
     * @param sell_amount_beer Menge des Bieres die verkauft wird
     * @param price_singleUnit Verkaufspreis von einem Bier in € (inkl. Profitmarge)
     */
    public void sellBeer(int sell_amount_beer, BigDecimal price_singleUnit){
        if (sell_amount_beer > amount_beer) throw new IllegalArgumentException("Order Amount too high");
        amount_beer = amount_beer - sell_amount_beer;
        budget = budget.add(price_singleUnit.multiply(new BigDecimal(sell_amount_beer)));
    }

    /**
     * Setzt die Zielfüllmenge des Lagers anhand eines prozentualen Inputs fest
     * @param targetPercentage Prozentuale Zielfüllmenge des Lagers
     */
    public void updateTargetAmount(BigDecimal targetPercentage){
        if (targetPercentage.intValue() < 0) throw new IllegalArgumentException("Negative percentage");
        if (targetPercentage.intValue() > 100) throw new IllegalArgumentException("More then 100 percent");
        target_amount_beer = targetPercentage.multiply(new BigDecimal(max_amount_beer)).intValue();
    }

    public boolean enoughtBudgetAvailable(BigDecimal cost){
        return cost.compareTo(this.budget) <= 0;
    }

    /**
     * Zieht die täglichen Kosten (Fix- plus Lagerkosten) vom Budget ab
     */
    public void bookDailyCosts(){
        BigDecimal totalCosts = dailyFixCosts.add(Params.storageCostPerKg.multiply(new BigDecimal(amount_beer)));
        budget = budget.subtract(totalCosts);
//        es findet keine Prüfung statt. Es ist bewusst möglich hierbei ins Minus zu rutschen
    }

    /**
     * Setzt den Unternehmenscounter auf 0 zurück.
     * Soll nur nach dem Beenden einer Simulation durchgeführt werden
     */
    public static void resetCounter(){
        resetCounter(0);
    }

    /**
     * Setzt den Unternehmenscounter auf eine Zahl zurück.
     * @param highestId Höchste aktuell vergebene numerische ID in der Datenbank
     */
    public static void resetCounter(int highestId){
        count.set(highestId);
    }











//    Standardmethoden

    /*
    Kennzeichnung als Deprecated hat den Zweck, dass die Methode nicht unwissentlich verwendet wird
    Wird lediglich für die Field based access von Hibernate benötigt
     */

    public Brewery() {
    }

    public UUID getId() {
        return id;
    }

    @Deprecated
    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    @Deprecated
    public void setBudget(BigDecimal budget) {
        this.budget = budget;
    }

    public int getAmount_water() {
        return amount_water;
    }

    @Deprecated
    public void setAmount_water(int amount_water) {
        this.amount_water = amount_water;
    }

    public int getAmount_hops() {
        return amount_hops;
    }

    @Deprecated
    public void setAmount_hops(int amount_hops) {
        this.amount_hops = amount_hops;
    }

    public int getAmount_malt() {
        return amount_malt;
    }

    @Deprecated
    public void setAmount_malt(int amount_malt) {
        this.amount_malt = amount_malt;
    }

    public int getAmount_beer() {
        return amount_beer;
    }

    @Deprecated
    public void setAmount_beer(int amount_beer) {
        this.amount_beer = amount_beer;
    }

    public BigDecimal getValue_water_unit() {
        return value_water_unit;
    }

    @Deprecated
    public void setValue_water_unit(BigDecimal value_water_unit) {
        this.value_water_unit = value_water_unit;
    }

    public BigDecimal getValue_hop_unit() {
        return value_hop_unit;
    }

    @Deprecated
    public void setValue_hop_unit(BigDecimal value_hop_unit) {
        this.value_hop_unit = value_hop_unit;
    }

    public BigDecimal getValue_malt_unit() {
        return value_malt_unit;
    }

    @Deprecated
    public void setValue_malt_unit(BigDecimal value_malt_unit) {
        this.value_malt_unit = value_malt_unit;
    }

    @Deprecated
    public BigDecimal getValue_beer_unit() {
        return value_beer_unit;
    }

    @Deprecated
    public void setValue_beer_unit(BigDecimal value_beer_unit) {
        this.value_beer_unit = value_beer_unit;
    }

    public int getMax_amount_water() {
        return max_amount_water;
    }

    @Deprecated
    public void setMax_amount_water(int max_amount_water) {
        this.max_amount_water = max_amount_water;
    }

    public int getMax_amount_hops() {
        return max_amount_hops;
    }

    @Deprecated
    public void setMax_amount_hops(int max_amount_hops) {
        this.max_amount_hops = max_amount_hops;
    }

    public int getMax_amount_malt() {
        return max_amount_malt;
    }

    @Deprecated
    public void setMax_amount_malt(int max_amount_malt) {
        this.max_amount_malt = max_amount_malt;
    }

    public int getMax_amount_beer() {
        return max_amount_beer;
    }

    @Deprecated
    public void setMax_amount_beer(int max_amount_beer) {
        this.max_amount_beer = max_amount_beer;
    }

    public int getTarget_amount_beer() {
        return target_amount_beer;
    }

    @Deprecated
    public void setTarget_amount_beer(int target_amount_beer) {
        this.target_amount_beer = target_amount_beer;
    }

    public BigDecimal getBreaking_factor() {
        return breaking_factor;
    }

    @Deprecated
    public void setBreaking_factor(BigDecimal breaking_factor) {
        this.breaking_factor = breaking_factor;
    }

    public BigDecimal getCost_factor() {
        return cost_factor;
    }

    @Deprecated
    public void setCost_factor(BigDecimal cost_factor) {
        this.cost_factor = cost_factor;
    }

    public BigDecimal getProfit_margin() {
        return profit_margin;
    }

    public void setProfit_margin(BigDecimal profit_margin) {
        this.profit_margin = profit_margin;
    }

    public CompanySize getSize() {
        return size;
    }

    @Deprecated
    public void setSize(CompanySize size) {
        this.size = size;
    }

    public QualityLevel getQualityLevel() {
        return qualityLevel;
    }

    @Deprecated
    public void setQualityLevel(QualityLevel qualityLevel) {
        this.qualityLevel = qualityLevel;
    }

    public BigDecimal getStartBudget() {
        return startBudget;
    }

    @Deprecated
    public void setStartBudget(BigDecimal startBudget) {
        this.startBudget = startBudget;
    }

    public int getNumericalId() {
        return numericalId;
    }

    @Deprecated
    public void setNumericalId(int numericalId) {
        this.numericalId = numericalId;
    }

    public BigDecimal getDailyFixCosts() {
        return dailyFixCosts;
    }

    public void setDailyFixCosts(BigDecimal dailyFixCosts) {
        this.dailyFixCosts = dailyFixCosts;
    }

    public long getVersion() {
        return version;
    }
}
