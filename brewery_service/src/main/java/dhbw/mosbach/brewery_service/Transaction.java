package dhbw.mosbach.brewery_service;

import dhbw.mosbach.brewery_service.enums.QualityLevel;
import dhbw.mosbach.brewery_service.enums.Ressource;
import dhbw.mosbach.brewery_service.enums.TransactionType;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
public class Transaction {
    @Id
    @Type(type="uuid-char")
    private UUID transactionID = UUID.randomUUID();
    private TransactionType transactionType;
    private int day;
    private String sellerID;
    private String buyerID;
    private QualityLevel quality;
    private int amount;
    @Column(columnDefinition=Params.bigDecimalColumDefinition)
    private BigDecimal unitPrice; //in €
    private Ressource resource;

    public Transaction() {
    }

    public Transaction(TransactionType transactionType, int day, String sellerID, String buyerID, QualityLevel quality, int amount, BigDecimal unitPrice, Ressource resource) {
        this.transactionID = UUID.randomUUID();
        this.transactionType = transactionType;
        this.day = day;
        this.sellerID = sellerID;
        this.buyerID = buyerID;
        this.quality = quality;
        this.amount = amount;
        this.unitPrice = unitPrice;
        this.resource = resource;
    }

    public UUID getTransactionID() {
        return transactionID;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public int getDay() {
        return day;
    }

    public String getSellerID() {
        return sellerID;
    }

    public String getBuyerID() {
        return buyerID;
    }

    public QualityLevel getQuality() {
        return quality;
    }

    public int getAmount() {
        return amount;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }
}
