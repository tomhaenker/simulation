package dhbw.mosbach.brewery_service;

import dhbw.mosbach.brewery_service.enums.CompanySize;
import dhbw.mosbach.brewery_service.enums.QualityLevel;
import dhbw.mosbach.brewery_service.enums.Ressource;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Alle Tests müssen nacheinander ausgeführt werden
 * Ansonsten schlagen einzelne Tests fehl
 *
 * Beim Ausführen der Tests muss geprüft werden, dass auch die korrekten Parameter für das Starten der Simulation verwendet werden
 * Bei IntelliJ - Gradle muss unter Enviroment variables folgendes gesetzt werden: spring.profiles.active=dev
 * Am einfachsten ist das, wenn man es auf Klassenebene definiert ansonsten muss das für jede einzeln getestete Methode eingetragen werden
 *
 */
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class BreweryServiceApplicationTests {
    /*
    Beim Ausführen der Tests muss geprüft werden, dass auch die korrekten Parameter für das Starten der Simulation verwendet werden
    Bei IntelliJ - Gradle muss unter Enviroment variables folgendes gesetzt werden: spring.profiles.active=dev
    Am einfachsten ist das, wenn man es auf Klassenebene definiert ansonsten muss das für jede einzeln getestete Methode eingetragen werden
     */

    static Brewery brewery = new Brewery(new BigDecimal(100_000), CompanySize.SMALL, QualityLevel.LOW, new BigDecimal("0.5"), new BigDecimal("0.1"));

    @BeforeAll
    static void before(){
        brewery.buyRessources(brewery.getMax_amount_hops(), new BigDecimal("0.1"), Ressource.HOP);
        brewery.buyRessources(brewery.getMax_amount_malt(), new BigDecimal("0.2"), Ressource.MALT);
        brewery.buyRessources(brewery.getMax_amount_water(), new BigDecimal("0.05"), Ressource.WATER);
        brewery.produceBeer();
    }

    @Test @Order(10)
    void testWater() {
        assertEquals( 3_500, brewery.getAmount_water() );
    }

    @Test @Order(20)
    void testMalt() {
        assertEquals( 4_500, brewery.getAmount_malt() );
    }

    @Test @Order(30)
    void testHop() {
        assertEquals( 4_800, brewery.getAmount_hops() );
    }

    @Test @Order(40)
    void testBeer() {
        assertEquals( 1_000, brewery.getAmount_beer() );
    }

    @Test @Order(50)
    void testBudget() {
        assertEquals( 99_980.5, brewery.getBudget().doubleValue() );
    }

    @Test @Order(60)
    void testBeerValue() {
        assertEquals( 0.2145, brewery.getValue_beer_unit().doubleValue() );
    }

    @Test @Order(70)
    void buyWater(){
        brewery.buyRessources(1500, new BigDecimal(10), Ressource.WATER);
        assertEquals(5000, brewery.getAmount_water());
        assertEquals(3.035, brewery.getValue_water_unit().doubleValue());
    }

    @Test @Order(80)
    void testSecondProduce(){
        brewery.produceBeer();
        assertEquals( 3_500, brewery.getAmount_water() );
        assertEquals( 4_000, brewery.getAmount_malt() );
        assertEquals( 4_600, brewery.getAmount_hops() );
        assertEquals( 2_000, brewery.getAmount_beer() );
        assertEquals( 99_513.25, brewery.getBudget().doubleValue() );
        assertEquals( 2.677125, brewery.getValue_beer_unit().doubleValue() );
    }

}
